package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;


public class SchemaParticlesTest {

	private DSSDocument d;
	
	String[] expElemDataNames = {
		"A", "B",
		"C", "Cs", "AttNameClash2",
		"elements",
		"elements2",
		"Es", "F",
		"elements3",
		"G", "H", "H_ambiguous", "G_ambiguous",
		"elements4",
		"evenI_ambiguous",
		"elements5",
		"evenJ_ambiguous",
		"elements6",
		"elements7",
		"K_ambiguouses"
	};
	
	String[] expAttrDataNames = {
		// Alphabetical order, since Attribute order is not deterministic
		"AttNameClash", "attA", "attB", "attC"
	};
	
	
	@Before
	public void prepare() throws FileNotFoundException, SAXException {

	}
	
	
	@After
	public void cleanup() {
		d = null;
	}
	
	
	@Test
	public void dataNames() throws FileNotFoundException, SAXException {
		File xmlFile = new File("testdata/particles/particles_empty.xml");
		File xsdFile = new File("testdata/particles/particles.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		d = parser.parseDocument(xml, xsd);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		assertNotNull(d.getRoot().getType());
		assertNotNull(d.getRoot().getType().getTemplate());
		
		String[] attrDataNames = getAttrDataNames(d.getRoot().getType().getTemplate());
		assertArrayEquals(expAttrDataNames, attrDataNames);

		String[] elemDataNames = getElemDataNames(d.getRoot().getType().getTemplate());
		assertArrayEquals(expElemDataNames, elemDataNames);

	}
	
	
	private String[] getElemDataNames(DSSData dc) {
		DSSDataContainer c = (DSSDataContainer)dc;
		List<String> particleNames = new ArrayList<String>();
		for (DSSData d : c.getElements()) {
			particleNames.add(d.getName());
		}
		return particleNames.toArray(new String[particleNames.size()]);
	}
	
	private String[] getAttrDataNames(DSSData dc) {
		DSSDataContainer c = (DSSDataContainer)dc;
		List<String> particleNames = new ArrayList<String>();
		for (DSSData d : c.getAttributes()) {
			particleNames.add(d.getName());
		}
		java.util.Collections.sort(particleNames);		// make order deterministic
		return particleNames.toArray(new String[particleNames.size()]);
	}
	


}
