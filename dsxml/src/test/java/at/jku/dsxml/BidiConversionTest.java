package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.transform.stream.StreamSource;

import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlunit.validation.Languages;
import org.xmlunit.validation.ValidationResult;
import org.xmlunit.validation.Validator;

import at.jku.dsxml.helpers.XMLHelper;
import at.jku.dsxml.parsers.DS;
import at.jku.dsxml.parsers.DSSGenerator;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Owner;
import at.jku.sea.cloud.Tool;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.rest.client.RestCloud;

import nu.xom.Document;

public class BidiConversionTest {

	
	@Test
	public void xmlds_dsxml_simple() throws SAXException, IOException {
		File xmlFile = new File("testdata/simple.xml");
		File xsdFile = new File("testdata/simple.xsd");
		
		// 1. XML parsing (and optional: schema generation)
		Document xmlDoc = XMLHelper.LoadDocument(xmlFile);
		DSSParser parser = new DSSParser();
		//ByteArrayOutputStream schemaStream = parser.generateSchema(xmlDoc);
		//FileOutputStream xsd_out = new FileOutputStream(xsdFile);
		//xsd_out.write(schemaStream.toByteArray());
		//xsd_out.close();
		
		FileInputStream xsd = new FileInputStream(xsdFile);
		parser.addSchema(xsd);
		DSSDocument d = parser.parseDocument(xmlDoc);
		
		// 2. DS generation
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        at.jku.sea.cloud.Package p = ws.createPackage();
        p.createProperty(ws, "name").setValue(ws, "DSSXSD");
        DS ds = new DS(ws, p);
        Artifact docArtifact = ds.storeDocument(d);
        //ws.commitAll("test");
        
        // 3. DS parsing
        ds = new DS(ws, p);
        DSSDocument d_out = ds.parseDocument(docArtifact);
        
        // 4. XML generation
		File xmlOutFile = new File("testdata/simple.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        
        
        // output XML complies to schema
        Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        v.setSchemaSource(new StreamSource(xsdFile));
        ValidationResult r = v.validateInstance(new StreamSource(xmlOutFile));
        assertTrue(r.isValid());
        
        
        
        // compare XML documents
        /*
        Source xml_ref = Input.fromFile(xmlFile).build();
        Source xml_out = Input.fromFile(xmlOutFile).build();
        Diff diff = DiffBuilder.compare(xml_ref).withTest(xml_out)
        		.ignoreComments()
        		.ignoreWhitespace()
        		.withDifferenceListeners(new ComparisonListener() {
	                public void comparisonPerformed(Comparison comparison, ComparisonResult outcome) {
	                    fail("found a difference: " + comparison);
	                }
	            })
        		.build();
        
        assertTrue(!diff.hasDifferences());
        */
        
	}
	
	
	@Test
	public void xmlds_dsxml_shiporder() throws SAXException, FileNotFoundException {
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		// 1. XML parsing
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xml, xsd);
		
		// 2. DS generation
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        at.jku.sea.cloud.Package p = ws.createPackage();
        p.createProperty(ws, "name").setValue(ws, "DSSXSD");
        DS ds = new DS(ws, p);
        Artifact docArtifact = ds.storeDocument(d);
        //ws.commitAll("test");
        
        // 3. DS parsing
        ds = new DS(ws, p);
        DSSDocument d_out = ds.parseDocument(docArtifact);
        
        // 4. XML generation
		File xmlOutFile = new File("testdata/shiporder.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        
        
        // output XML complies to schema
        Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        v.setSchemaSource(new StreamSource(xsdFile));
        ValidationResult r = v.validateInstance(new StreamSource(xmlOutFile));
        assertTrue(r.isValid());
        
	}


	@Test
	public void xmlds_dsxml_namespace() throws SAXException, FileNotFoundException {
		File xmlFile = new File("testdata/namespace/Homogeneous/Company.xml");
		File xsdFile = new File("testdata/namespace/Homogeneous/Company.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		
		// 1. XML parsing
		DSSParser parser = new DSSParser();
		InputSource input = new InputSource();
		input.setSystemId("testdata/namespace/Homogeneous/Company.xsd");
		input.setPublicId("http://www.company.org");
		input.setByteStream(new FileInputStream(xsdFile));
		parser.addSchema(input);
		DSSDocument d = parser.parseDocument(xml);
		
		
		// 2. DS generation
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        at.jku.sea.cloud.Package p = ws.createPackage();
        p.createProperty(ws, "name").setValue(ws, "DSSXSD");
        DS ds = new DS(ws, p);
        Artifact docArtifact = ds.storeDocument(d);
        //ws.commitAll("test");
        
        // 3. DS parsing
        ds = new DS(ws, p);
        DSSDocument d_out = ds.parseDocument(docArtifact);
        
        // 4. XML generation
		File xmlOutFile = new File("testdata/namespace/Homogeneous/Company.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        
        
        // output XML complies to schema
        Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        v.setSchemaSource(new StreamSource(xsdFile));
        ValidationResult r = v.validateInstance(new StreamSource(xmlOutFile));
        assertTrue(r.isValid());
        
	}
	
	
	
	
	@Test
	public void xmlds_dsxml_namespace_multi() throws SAXException, FileNotFoundException {
		File xmlFile = new File("testdata/namespace/Heterogeneous/Company.xml");
		File xsdFile = new File("testdata/namespace/Heterogeneous/Company.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		
		// 1. XML parsing
		DSSParser parser = new DSSParser();
		InputSource input = new InputSource();
		input.setSystemId("testdata/namespace/Heterogeneous/Company.xsd");
		input.setPublicId("http://www.company.org");
		input.setByteStream(new FileInputStream(xsdFile));
		parser.addSchema(input);
		DSSDocument d = parser.parseDocument(xml);
		
		
		// 2. DS generation
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        at.jku.sea.cloud.Package p = ws.createPackage();
        p.createProperty(ws, "name").setValue(ws, "DSSXSD");
        DS ds = new DS(ws, p);
        Artifact docArtifact = ds.storeDocument(d);
        //ws.commitAll("test");
        
        // 3. DS parsing
        ds = new DS(ws, p);
        DSSDocument d_out = ds.parseDocument(docArtifact);
        
        // 4. XML generation
		File xmlOutFile = new File("testdata/namespace/Heterogeneous/Company.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        
        
        // output XML complies to schema
        Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        v.setSchemaSource(new StreamSource(xsdFile));
        ValidationResult r = v.validateInstance(new StreamSource(xmlOutFile));
        assertTrue(r.isValid());
        
		
	}
	

	
}
