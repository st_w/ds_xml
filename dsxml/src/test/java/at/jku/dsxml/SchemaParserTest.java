package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElementDecl;


public class SchemaParserTest {

	List<String> shiporderElemRefs = Arrays.asList(
			"{}shiporder",
			"{}orderperson",
			"{}shipto",
			"{}name",
			"{}address",
			"{}city",
			"{}country",
			"{}item",
			"{}title",
			"{}note",
			"{}quantity",
			"{}price"
		);
	
	
	
	@Before
	public void prepare() {
		
	}
	
	@After
	public void cleanup() {
		
	}
	
	

	
	@Test
	public void testSchemaNested() throws FileNotFoundException, SAXException {
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xml, xsd);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());

		assertElemDecls(d.getElemRegistry().values(), shiporderElemRefs);
	}
	
	
	@Test
	public void testSchemaTopLevelElements() throws FileNotFoundException, SAXException {
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder2.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xml, xsd);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());

		assertElemDecls(d.getElemRegistry().values(), shiporderElemRefs);
	}
	
	
	@Test
	public void testSchemaNamedTypes() throws FileNotFoundException, SAXException {
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder3.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xml, xsd);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());

		assertElemDecls(d.getElemRegistry().values(), shiporderElemRefs);
	}
	
	
	
	private void assertElemDecls(Collection<DSSElementDecl> elemDecls, Collection<String> elemRefs) {
		assertEquals(elemRefs.size(), elemDecls.size());
		
		for (DSSElementDecl e : elemDecls) {
			assertTrue(elemRefs.contains(e.getName().toString()));
		}
	}


}
