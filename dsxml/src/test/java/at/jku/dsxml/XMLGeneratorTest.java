package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import at.jku.dsxml.helpers.XMLHelper;
import at.jku.dsxml.parsers.DSSGenerator;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElement;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;

public class XMLGeneratorTest {

	private static Document d_before;
	private static DSSDocument d;
	private static Document d_after;
	
	
	
	@BeforeClass
	public static void prepareClass() throws Exception {
		
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder.xsd");
		
		FileInputStream xsd = new FileInputStream(xsdFile);
		
        d_before = XMLHelper.LoadDocument(xmlFile);

		DSSParser dsparser = new DSSParser();
		dsparser.addSchema(xsd);
		d = dsparser.parseDocument(d_before);
		
		DSSGenerator gen = new DSSGenerator();
		d_after = gen.generateXML(d.getRoot());
		
	}

	@AfterClass
	public static void cleanupClass() throws Exception {
		d_before = null;
		d = null;
		d_after = null;
	}

	
	@Test
	public void documentRoot() {
		assertNotNull(d_after);
		assertEquals(d.getRoot().getName(), XMLName.fromElement(d_after.getRootElement()));
	}
	
	@Test
	public void rootAttributes() {
		DSSElement er = d.getRoot();
		Element e = d_after.getRootElement();
		assertEquals(er.getAttributes().size(), e.getAttributeCount());
		assertNotNull(e.getAttribute("orderid"));
		assertEquals(er.getAttributes().get(0).getText(), e.getAttribute(0).getValue());
	}
	
	
	
	@Test
	public void simpleContentElement() {
		Element e = d_after.getRootElement();
		Element orderperson = e.getFirstChildElement("orderperson");
		assertNotNull(orderperson);
		assertEquals("John Smith", orderperson.getValue());
	}
	
	
	@Test
	public void complexContentElement() {
		Element e = d_after.getRootElement();
		Element shipto = e.getFirstChildElement("shipto");
		assertNotNull(shipto);
		assertEquals(4, shipto.getChildCount());
		
		Element child;
		child = shipto.getFirstChildElement("name");
		assertNotNull(child);
		assertEquals("Ola Nordmann", child.getValue());
		child = shipto.getFirstChildElement("address");
		assertNotNull(child);
		assertEquals("Langgt 23", child.getValue());
		child = shipto.getFirstChildElement("city");
		assertNotNull(child);
		assertEquals("4000 Stavanger", child.getValue());
		child = shipto.getFirstChildElement("country");
		assertNotNull(child);
		assertEquals("Norway", child.getValue());
	}
	
	
	@Test
	public void arrayContentElement() {
		Element e = d_after.getRootElement();
		Elements items = e.getChildElements("item");
		assertNotNull(items);

		assertEquals(2, items.size());
		Element item;
		Element child;
		
		item = items.get(0);
		child = item.getFirstChildElement("title");
		assertNotNull(child);
		assertEquals("Empire Burlesque", child.getValue());
		child = item.getFirstChildElement("note");
		assertNotNull(child);
		assertEquals("Special Edition", child.getValue());
		child = item.getFirstChildElement("quantity");
		assertNotNull(child);
		assertEquals("1", child.getValue());
		child = item.getFirstChildElement("price");
		assertNotNull(child);
		assertEquals("10.90", child.getValue());
		
		item = items.get(1);
		child = item.getFirstChildElement("title");
		assertNotNull(child);
		assertEquals("Hide your heart", child.getValue());
		child = item.getFirstChildElement("quantity");
		assertNotNull(child);
		assertEquals("1", child.getValue());
		child = item.getFirstChildElement("price");
		assertNotNull(child);
		assertEquals("9.90", child.getValue());
		
	}
	
	
	@Test
	public void optionalSimpleElement() {
		Element e = d_after.getRootElement();
		Element item = e.getChildElements("item").get(1);
		assertNotNull(item);
		assertNull(item.getFirstChildElement("note"));	
	}
	
	

	@Test
	public void twoWaySimpleContent() {
		Element e_after = d_after.getRootElement().getFirstChildElement("orderperson");
		Element e_before = d_before.getRootElement().getFirstChildElement("orderperson");
		assertNotNull(e_after);
		assertEquals(e_before.toXML(), e_after.toXML());
	}
	
	

}
