package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import at.jku.dsxml.helpers.XMLHelper;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import nu.xom.Document;

public class SchemaGeneration {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void schemaGenerationSimple() throws Exception {
		File xmlFile = new File("testdata/demo.xml");
		Document xmlDoc = XMLHelper.LoadDocument(xmlFile);
		
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xmlDoc);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		
		assertTrue(d.getElemRegistry().size() > 0);
		
	}
	
	@Test
	public void schemaGenerationTTX() throws Exception {
		File xmlFile = new File("testdata/ComicSansMSRegular.ttx");
		Document xmlDoc = XMLHelper.LoadDocument(xmlFile);
		
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xmlDoc);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		
		assertTrue(d.getElemRegistry().size() > 0);
		
	}
	
	
	@Test
	public void schemaGenerationManual() throws Exception {
		File xmlFile = new File("testdata/demo.xml");
		InputStream xml = new FileInputStream(xmlFile);
		Document xmlDoc = XMLHelper.LoadDocument(xmlFile);
		
		DSSParser parser = new DSSParser();
		ByteArrayOutputStream schemaStream = parser.generateSchema(xmlDoc);
		assertNotNull(schemaStream);
		byte[] schemaData = schemaStream.toByteArray();
		assertTrue(schemaData.length > 0);
		
		InputStream xsd = new ByteArrayInputStream(schemaData);
		
		XMLHelper.ValidateDocument(xml, xsd);
	}
	
	
}
