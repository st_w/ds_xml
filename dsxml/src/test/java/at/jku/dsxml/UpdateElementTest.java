package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DS;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Owner;
import at.jku.sea.cloud.Tool;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.rest.client.RestCloud;

public class UpdateElementTest {


	@Test
	public void backToOriginal() throws FileNotFoundException, SAXException {
		
		Workspace ws;
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        
        DSSDocument original;
        Artifact document;

		{
			
			DSSDocument d;
			
			File xmlFile = new File("testdata/shiporder.xml");
			File xsdFile = new File("testdata/shiporder.xsd");
			
			FileInputStream xml = new FileInputStream(xmlFile);
			FileInputStream xsd = new FileInputStream(xsdFile);
			
			DSSParser parser = new DSSParser();
			d = parser.parseDocument(xml, xsd);
			
	        DS ds = new DS(ws);
	        //ds.setOverwriteEnabled(false);
	        Artifact dsDoc = ds.storeDocument(d);
	        assertNotNull(dsDoc);
	        document = dsDoc;
	        
	        //ws.commitAll("add DSX");
	        
	        ds = new DS(ws);
	        DSSDocument d_out = ds.parseDocument(dsDoc);
	        original = d_out;
	        
	        assertTrue(d_out.getRoot().contentEquals(d.getRoot()));
	        
	        /*
			File xmlOutFile = new File("testdata/shiporder.gen.xml");
			FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
	        DSSGenerator gen = new DSSGenerator();
	        gen.writeXML(d_out, xmlOut);
	        */
        
		}
		{
		
			DSSDocument d;
			
			File xmlFile = new File("testdata/shiporder2.xml");
			File xsdFile = new File("testdata/shiporder.xsd");
			
			FileInputStream xml = new FileInputStream(xmlFile);
			FileInputStream xsd = new FileInputStream(xsdFile);
			
			DSSParser parser = new DSSParser();
			d = parser.parseDocument(xml, xsd);
			
	        DS ds = new DS(ws);
	        ds.setOverwriteEnabled(true);
	        Artifact dsDoc = ds.storeDocument(d);
	        assertNotNull(dsDoc);
	        assertEquals(document.getId(), dsDoc.getId());
	        
	        //ws.commitAll("add DSX");
	        
	        ds = new DS(ws);
	        DSSDocument d_out = ds.parseDocument(dsDoc);
	        
	        assertTrue(d_out.getRoot().contentEquals(d.getRoot()));
	        assertFalse(d_out.getRoot().contentEquals(original.getRoot()));
	        
	        /*
			File xmlOutFile = new File("testdata/shiporder2.gen.xml");
			FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
	        DSSGenerator gen = new DSSGenerator();
	        gen.writeXML(d_out, xmlOut);
	        */
	        
        
		}
		
		{
			DSSDocument d;
			
			File xmlFile = new File("testdata/shiporder.xml");
			File xsdFile = new File("testdata/shiporder.xsd");
			
			FileInputStream xml = new FileInputStream(xmlFile);
			FileInputStream xsd = new FileInputStream(xsdFile);
			
			DSSParser parser = new DSSParser();
			d = parser.parseDocument(xml, xsd);
			
	        DS ds = new DS(ws);
	        ds.setOverwriteEnabled(true);
	        Artifact dsDoc = ds.storeDocument(d);
	        assertNotNull(dsDoc);
	        
	        //ws.commitAll("add DSX");
	        
	        ds = new DS(ws);
	        DSSDocument d_out = ds.parseDocument(dsDoc);
	        
	        assertTrue(d_out.getRoot().contentEquals(d.getRoot()));
	        assertTrue(d_out.getRoot().contentEquals(original.getRoot()));
	        
	        /*
			File xmlOutFile = new File("testdata/shiporder.gen.out.xml");
			FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
	        DSSGenerator gen = new DSSGenerator();
	        gen.writeXML(d_out, xmlOut);
	        */
	        
        
		}
	}
	
	
	
	@Test
	public void updateNonExistant() throws SAXException, FileNotFoundException {
		Workspace ws;
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        
		DSSDocument d;
		
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		d = parser.parseDocument(xml, xsd);
		
        DS ds = new DS(ws);
        ds.setOverwriteEnabled(true);
        Artifact dsDoc = ds.storeDocument(d);
        assertNotNull(dsDoc);
                
        ds = new DS(ws);
        DSSDocument d_out = ds.parseDocument(dsDoc);
        
        assertTrue(d_out.getRoot().contentEquals(d.getRoot()));
        
        /*
		File xmlOutFile = new File("testdata/shiporder.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        */
		
	}
	
	
	
	@Test
	public void noChanges() throws FileNotFoundException, SAXException {
		
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");

		File xmlFile = new File("testdata/musicxml/singleNote.xml");
		File xsdFile = new File("testdata/musicxml/musicxml30/musicxml.xsd");
		
		Artifact original;
		
		{
			FileInputStream xml = new FileInputStream(xmlFile);
			FileInputStream xsd = new FileInputStream(xsdFile);
			
			DSSParser parser = new DSSParser();
			parser.setEntityResolver(new EntityResolver() {
				
				@Override
				public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
					if (systemId.lastIndexOf('/') > 0) {
						String filename = systemId.substring(systemId.lastIndexOf('/'));
						return new InputSource(new FileInputStream("testdata/musicxml/musicxml30" + filename));
					}
					System.out.println(publicId + ";   " + systemId);
					return null;
				}
			});
			DSSDocument d = parser.parseDocument(xml, xsd);
			
	        DS ds = new DS(ws);
	        ds.setOverwriteEnabled(true);
	        Artifact dsDoc = ds.storeDocument(d);
			assertNotNull(dsDoc);
			original = dsDoc;
	        
	        //ws.commitAll("add DSX");
	        
	        /*
	        // 3. DS parsing
	        ds = new DS(ws);
	        DSSDocument d_out = ds.parseDocument(dsDoc);
	        
	        // 4. XML generation
			File xmlOutFile = new File("testdata/musicxml/singleNote.gen.xml");
			FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
	        DSSGenerator gen = new DSSGenerator();
	        gen.writeXML(d_out, xmlOut);
	        */
		}
		{
			FileInputStream xml = new FileInputStream(xmlFile);
			FileInputStream xsd = new FileInputStream(xsdFile);
			
			DSSParser parser = new DSSParser();
			parser.setEntityResolver(new EntityResolver() {
				
				@Override
				public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
					if (systemId.lastIndexOf('/') > 0) {
						String filename = systemId.substring(systemId.lastIndexOf('/'));
						return new InputSource(new FileInputStream("testdata/musicxml/musicxml30" + filename));
					}
					System.out.println(publicId + ";   " + systemId);
					return null;
				}
			});
			DSSDocument d = parser.parseDocument(xml, xsd);
			
			
	        DS ds = new DS(ws);
	        ds.setOverwriteEnabled(true);
	        Artifact dsDoc = ds.storeDocument(d);
	        assertNotNull(dsDoc);
	        assertEquals(original.getId(), dsDoc.getId());
	        
	        //ws.commitAll("add DSX");
	        
	        /*
	        // 3. DS parsing
	        ds = new DS(ws);
	        DSSDocument d_out = ds.parseDocument(dsDoc);
	        
	        // 4. XML generation
			File xmlOutFile = new File("testdata/musicxml/singleNote.gen.xml");
			FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
	        DSSGenerator gen = new DSSGenerator();
	        gen.writeXML(d_out, xmlOut);
	        */
		}
        
    
		
	}
	

}
