package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DS;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Owner;
import at.jku.sea.cloud.Tool;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.rest.client.RestCloud;


public class DSGenerationTest {
	
	private static DSSDocument d;
	private static Workspace ws;

	@BeforeClass
	public static void prepare() throws FileNotFoundException, SAXException {
		
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		d = parser.parseDocument(xml, xsd);
		
		
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        DS ds = new DS(ws);
        Artifact dsDoc = ds.storeDocument(d);
        
        //ws.commitAll("add DSX");
        
        assertNotNull(dsDoc);
        
	}
	
	
	@AfterClass
	public static void cleanup() {
		d = null;
	}
	
	
	@Test
	public void test() {
		
	}
	
	
}
