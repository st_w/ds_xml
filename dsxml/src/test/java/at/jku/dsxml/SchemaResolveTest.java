package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElementDecl;


public class SchemaResolveTest {

	List<String> shiporderElemRefs = Arrays.asList(
			"{}shiporder",
			"{}orderperson",
			"{}shipto",
			"{}name",
			"{}address",
			"{}city",
			"{}country",
			"{}item",
			"{}title",
			"{}note",
			"{}quantity",
			"{}price"
		);
	
	List<String> companyElemHetRefs = Arrays.asList(
			"{http://www.product.org}Type",
			"{http://www.person.org}Name",
			"{http://www.person.org}SSN",
			"{http://www.company.org}Company",
			"{http://www.company.org}Person",
			"{http://www.company.org}Product"
		);
	
	List<String> companyElemRefs = Arrays.asList(
			"{http://www.company.org}Type",
			"{http://www.company.org}Name",
			"{http://www.company.org}SSN",
			"{http://www.company.org}Company",
			"{http://www.company.org}Person",
			"{http://www.company.org}Product"
		);
	
	List<String> companyElemUnqRefs = Arrays.asList(
			"{}Type",
			"{}Name",
			"{}SSN",
			"{http://www.company.org}Company",
			"{}Person",
			"{}Product"
		);
	
	@Test
	public void resolveNoNSSchema() throws SAXException, FileNotFoundException {
		
		File xmlFile = new File("testdata/shiporder.xml");
		FileInputStream xml = new FileInputStream(xmlFile);
		
		DSSParser parser = new DSSParser();
		parser.setEntityResolver(new EntityResolver() {
			
			@Override
			public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
				assertNull(publicId);
				assertEquals("shiporder.xsd", systemId);
				File xsdFile = new File("testdata/" + systemId);
				FileInputStream xsd = new FileInputStream(xsdFile);
				return new InputSource(xsd);
			}
		});
		DSSDocument d = parser.parseDocument(xml);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		
		assertElemDecls(d.getElemRegistry().values(), shiporderElemRefs);
		
	}
	
	
	@Test (expected=SAXException.class)
	public void resolveNoNSSchemaWithoutResolver() throws SAXException, FileNotFoundException {
		
		File xmlFile = new File("testdata/shiporder.xml");
		FileInputStream xml = new FileInputStream(xmlFile);
		
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xml);
		
		assertNotNull(d);
		
	}
	
	
	
	@Test
	public void resolveNSHeterogenous() throws SAXException, FileNotFoundException {
		
		File xmlFile = new File("testdata/namespace/Heterogeneous/Company.xml");
		FileInputStream xml = new FileInputStream(xmlFile);
		
		DSSParser parser = new DSSParser();
		parser.setEntityResolver(new EntityResolver() {
			
			@Override
			public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
				File f = null;
				switch (publicId) {
				case "http://www.company.org":
					f = new File("testdata/namespace/Heterogeneous/Company.xsd");
					break;
				case "http://www.product.org":
					f = new File("testdata/namespace/Heterogeneous/Product.xsd");
					break;
				case "http://www.person.org":
					f = new File("testdata/namespace/Heterogeneous/Person.xsd");
					break;
				default:
					fail("unexpected schema publicId: " + systemId);
				}
				FileInputStream xsd = new FileInputStream(f);
				InputSource is = new InputSource(xsd);
				is.setSystemId(f.getAbsolutePath());
				return is;
			}
		});
		DSSDocument d = parser.parseDocument(xml);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		
		assertElemDecls(d.getElemRegistry().values(), companyElemHetRefs);
		
	}
	
	
	
	@Test
	public void resolveNSChameleon() throws SAXException, FileNotFoundException {
		
		File xmlFile = new File("testdata/namespace/Chameleon/Company.xml");
		FileInputStream xml = new FileInputStream(xmlFile);
		
		DSSParser parser = new DSSParser();
		parser.setEntityResolver(new EntityResolver() {
			
			@Override
			public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
				File f = null;
				assertNotNull(systemId);
				if (systemId.endsWith("Company.xsd")) {
					f = new File("testdata/namespace/Homogeneous/Company.xsd");
				} else
				if (systemId.endsWith("/Product.xsd")) {
					f = new File("testdata/namespace/Homogeneous/Product.xsd");
				} else
				if (systemId.endsWith("/Person.xsd")) {
					f = new File("testdata/namespace/Homogeneous/Person.xsd");
				} else {
					fail("unexpected schema publicId: " + systemId);
				}
				FileInputStream xsd = new FileInputStream(f);
				InputSource is = new InputSource(xsd);
				is.setSystemId(f.getAbsolutePath());
				return is;
			}
		});
		DSSDocument d = parser.parseDocument(xml);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		
		assertElemDecls(d.getElemRegistry().values(), companyElemRefs);
		
	}
	
	
	@Test
	public void resolveNSHomogeneous() throws SAXException, FileNotFoundException {
		
		File xmlFile = new File("testdata/namespace/Homogeneous/Company.xml");
		FileInputStream xml = new FileInputStream(xmlFile);
		
		DSSParser parser = new DSSParser();
		parser.setEntityResolver(new EntityResolver() {
			
			@Override
			public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
				File f = null;
				assertNotNull(systemId);
				if (systemId.endsWith("Company.xsd")) {
					f = new File("testdata/namespace/Homogeneous/Company.xsd");
				} else
				if (systemId.endsWith("/Product.xsd")) {
					f = new File("testdata/namespace/Homogeneous/Product.xsd");
				} else
				if (systemId.endsWith("/Person.xsd")) {
					f = new File("testdata/namespace/Homogeneous/Person.xsd");
				} else {
					fail("unexpected schema publicId: " + systemId);
				}
				FileInputStream xsd = new FileInputStream(f);
				InputSource is = new InputSource(xsd);
				is.setSystemId(f.getAbsolutePath());
				return is;
			}
		});
		DSSDocument d = parser.parseDocument(xml);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		
		assertElemDecls(d.getElemRegistry().values(), companyElemRefs);
		
	}
	
	
	@Test
	public void resolveNSUnqualified() throws SAXException, FileNotFoundException {
		
		File xmlFile = new File("testdata/namespace/Unqualified/Company.xml");
		FileInputStream xml = new FileInputStream(xmlFile);
		
		DSSParser parser = new DSSParser();
		parser.setEntityResolver(new EntityResolver() {
			
			@Override
			public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
				File f = null;
				assertNotNull(systemId);
				if (systemId.endsWith("Company.xsd")) {
					f = new File("testdata/namespace/Unqualified/Company.xsd");
				} else
				if (systemId.endsWith("/Product.xsd")) {
					f = new File("testdata/namespace/Unqualified/Product.xsd");
				} else
				if (systemId.endsWith("/Person.xsd")) {
					f = new File("testdata/namespace/Unqualified/Person.xsd");
				} else {
					fail("unexpected schema publicId: " + systemId);
				}
				FileInputStream xsd = new FileInputStream(f);
				InputSource is = new InputSource(xsd);
				is.setSystemId(f.getAbsolutePath());
				return is;
			}
		});
		DSSDocument d = parser.parseDocument(xml);
		
		assertNotNull(d);
		assertNotNull(d.getRoot());
		
		assertElemDecls(d.getElemRegistry().values(), companyElemUnqRefs);
		
	}
	
	
	private void assertElemDecls(Collection<DSSElementDecl> elemDecls, Collection<String> elemRefs) {
		assertEquals(elemRefs.size(), elemDecls.size());
		
		for (DSSElementDecl e : elemDecls) {
			assertTrue("ElementDecl not found: " + e.getName(), elemRefs.contains(e.getName().toString()));
		}
	}
	
	
}
