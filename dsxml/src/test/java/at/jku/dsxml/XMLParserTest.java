package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javax.xml.XMLConstants;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataValue;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSAttribute;
import at.jku.dsxml.schema.DSSAttributeDecl;
import at.jku.dsxml.schema.DSSComplexType;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElement;
import at.jku.dsxml.schema.DSSElementRef;


public class XMLParserTest {

	private DSSDocument d;
	
	private final XMLName xmlStringType = new XMLName(XMLConstants.W3C_XML_SCHEMA_NS_URI, "string");
	
	
	@Before
	public void prepare() throws FileNotFoundException, SAXException {
		File xmlFile = new File("testdata/shiporder.xml");
		File xsdFile = new File("testdata/shiporder.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		d = parser.parseDocument(xml, xsd);
	}
	
	
	@After
	public void cleanup() {
		d = null;
	}
	
	
	@Test
	public void documentRootNotNull() {
		assertNotNull(d);
		assertNotNull(d.getRoot());
	}
	
	@Test
	public void rootElementName() {
		DSSElement e = d.getRoot();
		assertEquals("{}shiporder", e.getName().toString());
	}
	
	@Test
	public void rootElementAttributes() {
		DSSElement e = d.getRoot();
		List<DSSAttribute> att = e.getAttributes();
		assertEquals(1, att.size());
		DSSAttribute v = att.get(0);
		assertEquals("orderid", v.getName());		
	}
	
	@Test
	public void rootElementAttributeByName() {
		DSSElement e = d.getRoot();
		DSSDataValue v = e.getAttribute(XMLName.fromString("{}orderid"));
		assertNotNull(v);
	}
	
	
	
	@Test
	public void rootElementAttributeSubType() {
		DSSDataValue v = d.getRoot().getAttributes().get(0);
		
		assertNotNull(v.getSubType());
		assertTrue(v.getSubType() instanceof DSSAttributeDecl);
		DSSAttributeDecl ad = (DSSAttributeDecl)v.getSubType();
		assertEquals("{}orderid", ad.getName().toString());
	}
	
	
	@Test
	public void rootElementAttributeType() {
		DSSDataValue v = d.getRoot().getAttributes().get(0);
		
		assertNotNull(v.getType());
	}
	
	@Test
	public void rootElementAttributeValue() {
		DSSDataValue v = d.getRoot().getAttributes().get(0);

		assertEquals("889923", v.getText());
	}
	
	
	@Test
	public void rootElementContents() {
		DSSElement e = d.getRoot();
		List<DSSData> children = e.getChildren();
		String[] childrenDataNames = {
				"orderperson", "shipto", "items", "orderid"
		};
		assertEquals("epected number of data nodes", childrenDataNames.length, children.size());
		for (String name : childrenDataNames) {
			boolean containsIt = false;
			for (DSSData data : children) {
				if (data.getName().equals(name)) {
					containsIt = true;
					break;
				}
			}
			assertTrue("Element contains data node '" + name + "'", containsIt);			
		}
	}
	
	
	@Test
	public void simpleContentElement() {
		DSSElement e = d.getRoot();
		DSSDataValue orderperson = (DSSDataValue)e.getChild("orderperson");
		assertEquals("orderperson", orderperson.getName());
		assertEquals(xmlStringType, orderperson.getType().getName());
		assertEquals(XMLName.fromString("{}orderperson"), ((DSSElementRef)orderperson.getSubType()).getRef().getName());
		assertEquals("John Smith", orderperson.getText());
	}
	
	
	@Test
	public void complexContentElement() {
		DSSElement e = d.getRoot();
		DSSDataValue shipto = (DSSDataValue)e.getChild("shipto");
		assertEquals("shipto", shipto.getName());
		assertTrue(shipto.getType() instanceof DSSComplexType);
		assertEquals(XMLName.fromString("{}shipto"), ((DSSElementRef)shipto.getSubType()).getRef().getName());
		assertTrue(shipto.getValue() instanceof DSSElement);
		
		DSSElement shipToElem = (DSSElement)shipto.getValue();
		assertEquals(XMLName.fromString("{}shipto"), shipToElem.getName());
		assertEquals(4, shipToElem.getChildren().size());
		
		DSSDataValue child;
		child = (DSSDataValue) shipToElem.getChild("name1");
		assertNotNull(child);
		assertEquals("Ola Nordmann", child.getText());
		child = (DSSDataValue) shipToElem.getChild("address");
		assertNotNull(child);
		assertEquals("Langgt 23", child.getText());
		child = (DSSDataValue) shipToElem.getChild("city");
		assertNotNull(child);
		assertEquals("4000 Stavanger", child.getText());
		child = (DSSDataValue) shipToElem.getChild("country");
		assertNotNull(child);
		assertEquals("Norway", child.getText());
	}
	
	
	@Test
	public void arrayContentElement() {
		DSSElement e = d.getRoot();
		DSSDataArray items = (DSSDataArray)e.getChild("items");
		assertEquals("items", items.getName());
		assertTrue(items.getType() instanceof DSSComplexType);
		assertEquals(XMLName.fromString("{}item"), ((DSSElementRef)items.getSubType()).getRef().getName());
		
		DSSElement item;
		DSSDataValue child;
		assertEquals(2, items.getValues().size());
		
		item = (DSSElement) items.getValues().get(0);
		child = (DSSDataValue) item.getChild("title");
		assertNotNull(child);
		assertEquals("Empire Burlesque", child.getText());
		child = (DSSDataValue) item.getChild("note");
		assertNotNull(child);
		assertEquals("Special Edition", child.getText());
		child = (DSSDataValue) item.getChild("quantity");
		assertNotNull(child);
		assertEquals("1", child.getText());
		child = (DSSDataValue) item.getChild("price");
		assertNotNull(child);
		assertEquals("10.90", child.getText());
		
		item = (DSSElement) items.getValues().get(1);
		child = (DSSDataValue) item.getChild("title");
		assertNotNull(child);
		assertEquals("Hide your heart", child.getText());
		child = (DSSDataValue) item.getChild("quantity");
		assertNotNull(child);
		assertEquals("1", child.getText());
		child = (DSSDataValue) item.getChild("price");
		assertNotNull(child);
		assertEquals("9.90", child.getText());
		
	}
	
	
	@Test
	public void optionalSimpleElement() {
		DSSElement e = d.getRoot();
		DSSDataArray items = (DSSDataArray)e.getChild("items");
		DSSElement item = (DSSElement) items.getValues().get(1);
		DSSDataValue child = (DSSDataValue) item.getChild("note");
		assertNotNull(child);
		assertNull(child.getValue());
		assertNull(child.getText());		
	}
	
	


}
