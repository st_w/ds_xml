package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElement;
import at.jku.dsxml.schema.DSSElement.ElementTraverser;
import at.jku.dsxml.schema.DSSElementDecl;


public class ElementTraverserTest {

	/*
	String[] expElemDataNames = {
			"A", "B",
			"C", "Cs", "AttNameClash2",
			"elements",
			"elements2",
			"Es", "F",
			"elements3",
			"G", "H", "H_ambiguous", "G_ambiguous",
			"evenI_ambiguous",
			"elements4",
			"evenJ_ambiguous",
			"elements5",
			"elements6",
			"K_ambiguouses"
		};
	*/
	
	private DSSDocument d;
	
	@Before
	public void prepare() throws FileNotFoundException, SAXException {
		
		DSSParser parser = new DSSParser();
		File xsdFile = new File("testdata/particles/particles.xsd");
		FileInputStream xsd = new FileInputStream(xsdFile);
		parser.addSchema(xsd);
		d = parser.getSchemaDocument();
		
	}
	
	
	@After
	public void cleanup() {
		d = null;
	}
	
	
	@Test
	public void elementTraverserValid() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSElement ei = e.instantiate();
		ElementTraverser t = ei.createTraverser();
		
		assertTraverse(t, "{}A", "A");
		assertTraverse(t, "{}B", "B");
		assertTraverse(t, "{}C", "C");
		assertTraverse(t, "{}C", "Cs");
		assertTraverse(t, "{}C", "Cs");
		assertTraverse(t, "{}C", "Cs");
		assertTraverse(t, "{}C", "Cs");
		assertTraverse(t, "{}AttNameClash", "AttNameClash2");
		assertTraverse(t, "{}C", "elements");
		assertTraverse(t, "{}D", "elements2");
		assertTraverse(t, "{}D", "elements2");
		assertTraverse(t, "{}D", "elements2");
		assertTraverse(t, "{}D", "elements2");
		assertTraverse(t, "{}D", "elements2");
		assertTraverse(t, "{}E", "Es");
		assertTraverse(t, "{}E", "Es");
		assertTraverse(t, "{}F", "F");
		assertTraverse(t, "{}H_ambiguous", "H_ambiguous");
		assertTraverse(t, "{}G_ambiguous", "G_ambiguous");
		assertTraverse(t, "{}evenI", "elements5");
		assertTraverse(t, "{}evenI", "elements5");
		assertTraverse(t, "{}evenJ", "elements6");
		assertTraverse(t, "{}evenJ", "elements6");
		assertTraverse(t, "{}K_ambiguous", "K_ambiguouses");
		assertTraverse(t, "{}K_ambiguous", "K_ambiguouses");
		
	}
	
	
	
	
	
	@Test
	public void elementTraverserValidMinimum() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSElement ei = e.instantiate();
		ElementTraverser t = ei.createTraverser();
		
		assertTraverse(t, "{}A", "A");
		assertTraverse(t, "{}B", "B");
		assertTraverse(t, "{}C", "C");
		assertTraverse(t, "{}C", "Cs");
		assertTraverse(t, "{}AttNameClash", "AttNameClash2");
		assertTraverse(t, "{}K_ambiguous", "K_ambiguouses");
		assertTraverse(t, "{}K_ambiguous", "K_ambiguouses");
		
	}
	
	
	
	
	private static void assertTraverse(ElementTraverser t, String name, String expected) {
		assertEquals(expected, t.getNextElement(XMLName.fromString(name)).getName());
	}
	
	
	

}
