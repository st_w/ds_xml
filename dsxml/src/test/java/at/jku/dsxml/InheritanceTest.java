package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonListener;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.Diff;
import org.xmlunit.validation.Languages;
import org.xmlunit.validation.ValidationResult;
import org.xmlunit.validation.Validator;

import at.jku.dsxml.helpers.XMLHelper;
import at.jku.dsxml.parsers.DS;
import at.jku.dsxml.parsers.DSSGenerator;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Owner;
import at.jku.sea.cloud.Tool;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.rest.client.RestCloud;

import nu.xom.Document;

public class InheritanceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void simpleTypeInheritance() throws FileNotFoundException, SAXException {
		File xmlFile = new File("testdata/inheritance/simple.xml");
		File xsdFile = new File("testdata/inheritance/simple.xsd");
		
		// 1. XML parsing (and optional: schema generation)
		Document xmlDoc = XMLHelper.LoadDocument(xmlFile);
		DSSParser parser = new DSSParser();
		
		FileInputStream xsd = new FileInputStream(xsdFile);
		parser.addSchema(xsd);
		DSSDocument d = parser.parseDocument(xmlDoc);
		
		// 2. DS generation
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        at.jku.sea.cloud.Package p = ws.createPackage();
        p.createProperty(ws, "name").setValue(ws, "DSSXSD");
        DS ds = new DS(ws, p);
        Artifact docArtifact = ds.storeDocument(d);
        //ws.commitAll("test");
        
        // 3. DS parsing
        ds = new DS(ws, p);
        DSSDocument d_out = ds.parseDocument(docArtifact);
        
        // 4. XML generation
		File xmlOutFile = new File("testdata/inheritance/simple.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        
        
        // output XML complies to schema
        Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        v.setSchemaSource(new StreamSource(xsdFile));
        ValidationResult r = v.validateInstance(new StreamSource(xmlOutFile));
        assertTrue(r.isValid());
        
        
        // compare XML documents
        Source xml_ref = Input.fromFile(xmlFile).build();
        Source xml_out = Input.fromFile(xmlOutFile).build();
        Diff diff = DiffBuilder.compare(xml_ref).withTest(xml_out)
        		.ignoreWhitespace()
        		.withDifferenceListeners(new ComparisonListener() {
	                public void comparisonPerformed(Comparison comparison, ComparisonResult outcome) {
	                    fail("found a difference: " + comparison);
	                }
	            })
        		.build();
        
        assertTrue(!diff.hasDifferences());
        
	}
	
	
	
	@Test
	public void complexTypeInheritance() throws FileNotFoundException, SAXException {
		File xmlFile = new File("testdata/inheritance/employee.xml");
		File xsdFile = new File("testdata/inheritance/employee.xsd");
		
		// 1. XML parsing (and optional: schema generation)
		Document xmlDoc = XMLHelper.LoadDocument(xmlFile);
		DSSParser parser = new DSSParser();
		
		FileInputStream xsd = new FileInputStream(xsdFile);
		parser.addSchema(xsd);
		DSSDocument d = parser.parseDocument(xmlDoc);
		
		// 2. DS generation
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        at.jku.sea.cloud.Package p = ws.createPackage();
        p.createProperty(ws, "name").setValue(ws, "DSSXSD");
        DS ds = new DS(ws, p);
        Artifact docArtifact = ds.storeDocument(d);
        //ws.commitAll("test");
        
        // 3. DS parsing
        ds = new DS(ws, p);
        DSSDocument d_out = ds.parseDocument(docArtifact);
        
        // 4. XML generation
		File xmlOutFile = new File("testdata/inheritance/employee.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        
        
        // output XML complies to schema
        Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        v.setSchemaSource(new StreamSource(xsdFile));
        ValidationResult r = v.validateInstance(new StreamSource(xmlOutFile));
        assertTrue(r.isValid());
        
        // compare XML documents
        Source xml_ref = Input.fromFile(xmlFile).build();
        Source xml_out = Input.fromFile(xmlOutFile).build();
        Diff diff = DiffBuilder.compare(xml_ref).withTest(xml_out)
        		.ignoreWhitespace()
        		.withDifferenceListeners(new ComparisonListener() {
	                public void comparisonPerformed(Comparison comparison, ComparisonResult outcome) {
	                    fail("found a difference: " + comparison);
	                }
	            })
        		.build();
        
        assertTrue(!diff.hasDifferences());
        
	}

}
