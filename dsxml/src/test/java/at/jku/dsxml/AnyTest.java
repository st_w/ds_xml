package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.transform.stream.StreamSource;

import org.junit.Test;
import org.xml.sax.SAXException;
import org.xmlunit.validation.Languages;
import org.xmlunit.validation.ValidationResult;
import org.xmlunit.validation.Validator;

import at.jku.dsxml.parsers.DS;
import at.jku.dsxml.parsers.DSSGenerator;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Owner;
import at.jku.sea.cloud.Tool;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.rest.client.RestCloud;


public class AnyTest {

	@Test
	public void anyTest() throws SAXException, FileNotFoundException {
		File xmlFile = new File("testdata/any/shiporder.xml");
		File xsdFile = new File("testdata/any/shiporder.any.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		// 1. XML parsing
		DSSParser parser = new DSSParser();
		DSSDocument d = parser.parseDocument(xml, xsd);
		
		// 2. DS generation
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        Workspace ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        at.jku.sea.cloud.Package p = ws.createPackage();
        p.createProperty(ws, "name").setValue(ws, "DSSXSD");
        DS ds = new DS(ws, p);
        Artifact docArtifact = ds.storeDocument(d);
        //ws.commitAll("test");
        
        // 3. DS parsing
        ds = new DS(ws, p);
        DSSDocument d_out = ds.parseDocument(docArtifact);
        
        // 4. XML generation
		File xmlOutFile = new File("testdata/any/shiporder.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
        
        
        // output XML complies to schema
        Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        v.setSchemaSource(new StreamSource(xsdFile));
        ValidationResult r = v.validateInstance(new StreamSource(xmlOutFile));
        assertTrue(r.isValid());
        
	}

}
