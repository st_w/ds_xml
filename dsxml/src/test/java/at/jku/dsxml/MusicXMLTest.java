package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DS;
import at.jku.dsxml.parsers.DSSGenerator;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Owner;
import at.jku.sea.cloud.Tool;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.rest.client.RestCloud;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MusicXMLTest {

	private static DSSDocument d;
	private static DSSDocument d_out;
	private static Artifact dssDoc;
	private static Workspace ws;

	@Test
	public void a_XMLParsing() throws Exception {
		//File xmlFile = new File("musicxml/xmlsamples/ActorPreludeSample.xml");
		File xmlFile = new File("testdata/musicxml/xmlsamples/Chant.xml");
		File xsdFile = new File("testdata/musicxml/musicxml30/musicxml.xsd");
		
		FileInputStream xml = new FileInputStream(xmlFile);
		FileInputStream xsd = new FileInputStream(xsdFile);
		
		DSSParser parser = new DSSParser();
		parser.setEntityResolver(new EntityResolver() {
			
			@Override
			public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
				
				if (systemId.equals("http://www.musicxml.org/xsd/xml.xsd")) {
					return new InputSource(new FileInputStream("testdata/musicxml/musicxml30/xml.xsd"));
				}
				if (systemId.equals("http://www.musicxml.org/xsd/xlink.xsd")) {
					return new InputSource(new FileInputStream("testdata/musicxml/musicxml30/xlink.xsd"));
				}

				
				if (systemId.lastIndexOf('/') > 0) {
					String filename = systemId.substring(systemId.lastIndexOf('/'));
					return new InputSource(new FileInputStream("testdata/musicxml/musicxml30" + filename));
				}
				
				System.out.println(publicId + ";   " + systemId);
				return null;
			}
		});
		d = parser.parseDocument(xml, xsd);
		
		assertNotNull(d.getRoot());
		assertTrue(d.getElemRegistry().size() > 0);
	}
	
	@Test
	public void b_DSGeneration() {
        RestCloud cloud = RestCloud.getInstance();
        Owner owner = cloud.createOwner();
        Tool tool = cloud.createTool();
        ws = cloud.createWorkspace(owner, tool, "myWorkspace");
        DS ds = new DS(ws);
        dssDoc = ds.storeDocument(d);
        
        //ws.commitAll("dssxml test");
        
	}
	
	
	@Test
	public void c_DSParsing() {
        DS ds = new DS(ws);
        d_out = ds.parseDocument(dssDoc);
	}
	
	
	@Test
	public void d_XMLGeneration() throws FileNotFoundException {
		File xmlOutFile = new File("testdata/musicxml/Chant.gen.xml");
		FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
        DSSGenerator gen = new DSSGenerator();
        gen.writeXML(d_out, xmlOut);
	}
	
	

}
