package at.jku.dsxml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSComplexType;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElementDecl;
import at.jku.dsxml.schema.DSSParticle;
import at.jku.dsxml.schema.DSSParticle.ParticleTraverser;


public class ParticleTraverserTest {

	private DSSDocument d;
	
	@Before
	public void prepare() throws FileNotFoundException, SAXException {
		
		DSSParser parser = new DSSParser();
		File xsdFile = new File("testdata/particles/particles.xsd");
		FileInputStream xsd = new FileInputStream(xsdFile);
		parser.addSchema(xsd);
		d = parser.getSchemaDocument();
		
	}
	
	
	@After
	public void cleanup() {
		d = null;
	}

	
	@Test
	public void particleTraverserValid() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}C",
				"{}D",
				"{}D",
				"{}D",
				"{}D",
				"{}D",
				"{}E",
				"{}E",
				"{}F",
				"{}H_ambiguous",
				"{}G_ambiguous",
				"{}evenI",
				"{}evenI",
				"{}evenJ",
				"{}evenJ",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	@Test
	public void particleTraverserValidMinimal() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test
	public void particleTraverserValidMaximum() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		List<String> nameSeq = new ArrayList<>();
		
		nameSeq.add("{}A");
		nameSeq.add("{}B");
		nameSeq.add("{}C");
		for (int q = 0; q < 100; q++)		// unbounded
			nameSeq.add("{}C");
		nameSeq.add("{}AttNameClash");
		for (int q = 0; q < 10; q++)		// (0-10)*(0-1)
			nameSeq.add("{}C");
		for (int q = 0; q < 80; q++)		// (0-8)*(1-10)
			nameSeq.add("{}D");
		for (int q = 0; q < 5; q++)
			nameSeq.add("{}E");
		nameSeq.add("{}F");
		for (int q = 0; q < 80; q++)		// (0-8)*(1-10)
			nameSeq.add("{}D_ambiguous");
		nameSeq.add("{}G");
		nameSeq.add("{}H");
		nameSeq.add("{}evenI_ambiguous");
		for (int q = 0; q < 2*100; q++)		// (1+1)*unbounded
			nameSeq.add("{}evenI");
		nameSeq.add("{}evenJ_ambiguous");
		for (int q = 0; q < 2*100; q++)		// 2*unbounded
			nameSeq.add("{}evenJ");
		for (int q = 0; q < 100; q++)		// unbounded
			nameSeq.add("{}K");
		nameSeq.add("{}K_ambiguous");
		nameSeq.add("{}K_ambiguous");
		
		traverseParticles(ct, nameSeq.toArray(new String[nameSeq.size()]));		
	}
	
	
	
	@Test(expected=IllegalStateException.class)
	public void incompleteSeqSingleElement() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}evenJ",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test(expected=IllegalStateException.class)
	public void incompleteSeqMultiElement() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}evenI",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	
	@Test(expected=IllegalStateException.class)
	public void missingElement() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}AttNameClash",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test(expected=IllegalStateException.class)
	public void disallowedElement() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}disallowed",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test(expected=IllegalStateException.class)
	public void disallowedGroup() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}disallowedGroup",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	@Test(expected=IllegalStateException.class)
	public void prematureEnd() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	
	@Test(expected=IllegalStateException.class)
	public void firstMissing() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test
	public void choiceFirstNullableSeq() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}H",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test
	public void choiceSecondNullableSeq() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}G_ambiguous",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	
	@Test
	public void choiceNullableRequireSingle() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}choiceB",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test
	public void choiceNullableRequireRepeated() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}choiceB",
				"{}choiceB",
				"{}choiceB",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test
	public void choiceNullableRequireMulti() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}choiceA",
				"{}choiceB",
				"{}choiceC",
				"{}choiceD",
				"{}choiceD",
				"{}choiceC",
				"{}choiceB",
				"{}choiceA",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	@Test(expected=IllegalStateException.class)
	public void choiceBoth() {
		DSSElementDecl e = d.getElemRegistry().get(XMLName.fromString("{}main"));
		DSSComplexType ct = (DSSComplexType) e.getType();
		
		String[] nameSeq = {
				"{}A",
				"{}B",
				"{}C",
				"{}C",
				"{}AttNameClash",
				"{}H",
				"{}G_ambiguous",
				"{}K_ambiguous",
				"{}K_ambiguous"
		};
		
		traverseParticles(ct, nameSeq);		
	}
	
	
	
	private void traverseParticles(DSSComplexType ct, String[] nameSeq) {
		assertNotNull(ct.getParticle());
		
		ParticleTraverser t = ct.getParticle().createTraverser();
		assertNotNull(t);
		
		for (String name : nameSeq) {
			DSSParticle p = t.next(XMLName.fromString(name));
			assertNotNull(p);
		}
		assertNull(t.next(null));
	}
	
	
	
	
	
	
	
}
