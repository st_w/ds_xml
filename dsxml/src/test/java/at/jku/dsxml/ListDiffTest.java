package at.jku.dsxml;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import at.jku.dsxml.helpers.ListDiff;
import at.jku.dsxml.helpers.ListDiff.Subsequence;
import at.jku.dsxml.helpers.ListDiff.TransformOperation;

public class ListDiffTest {

	@Test
	public void LCSEmptyList() {
		
		List<Integer> la = new ArrayList<>();
		List<Integer> lb = new ArrayList<>();
		
		Subsequence ss = ListDiff.longestCommonSubsequence(la, lb);
		assertEquals(0, ss.length);
		
	}
	
	
	@Test
	public void LCSMultipleMatches() {
		
		List<Integer> la = Arrays.asList(99, 1, 2, 3);
		List<Integer> lb = Arrays.asList(88, 1, 2, 1, 2, 3);
		
		Subsequence ss = ListDiff.longestCommonSubsequence(la, lb);
		assertEquals(3, ss.length);
		assertEquals(1, ss.offsetA);
		assertEquals(3, ss.offsetB);		
	}
	
	
	
	@Test
	public void LCSSecondaryMatch() {
		
		List<Integer> la = Arrays.asList(1, 2,3,2,3, 4);
		List<Integer> lb = Arrays.asList(99, 1, 2, 2,3,2,3);
		
		Subsequence ss = ListDiff.longestCommonSubsequence(la, lb);
		assertEquals(4, ss.length);
		assertEquals(1, ss.offsetA);
		assertEquals(3, ss.offsetB);
		
	}
	
	
	
	@Test
	public void CompareEmptyList() {
		
		List<Integer> la = new ArrayList<>();
		List<Integer> lb = new ArrayList<>();
		
		List<TransformOperation<Integer>> ops = ListDiff.compareLists(la, lb);
		printOps(ops);
		assertEquals(0, ops.size());
		
	}
	
	
	@Test
	public void CompareMultipleMatches() {
		
		List<Integer> la = new ArrayList<>(Arrays.asList(99, 1, 2, 3));
		List<Integer> lb = Arrays.asList(88, 1, 2, 1, 2, 3);
		
		List<TransformOperation<Integer>> ops = ListDiff.compareLists(la, lb);
		printOps(ops);
		List<Integer> lc = ListDiff.applyTransformations(la, ops);
		assertEquals(lb, lc);	
	}
	
	
	
	@Test
	public void CompareSecondaryMatch() {
		
		List<Integer> la = new ArrayList<>(Arrays.asList(1, 2,3,2,3, 4));
		List<Integer> lb = Arrays.asList(99, 1, 2, 2,3,2,3);
		
		List<TransformOperation<Integer>> ops = ListDiff.compareLists(la, lb);
		printOps(ops);
		assertEquals(lb, ListDiff.applyTransformations(la, ops));	
		
	}
	
	
	@Test
	public void CompareManyMatches() {
		
		List<Integer> la = new ArrayList<>(Arrays.asList(99, 1, 2, 3, 4, 2, 3, 1, 2, 3, 3, 4, 88, 99, 3, 2, 1, 2, 3, 4, 5, 6, 44, 2, 4, 1));
		List<Integer> lb = Arrays.asList(88, 1, 2, 1, 2, 3, 55, 3, 4, 5, 1, 44, 4, 3, 2, 3, 4, 5, 6, 99, 2, 3, 4);
		
		List<TransformOperation<Integer>> ops = ListDiff.compareLists(la, lb);
		printOps(ops);
		List<Integer> lc = ListDiff.applyTransformations(la, ops);
		assertEquals(lb, lc);	
	}
	
	
	
	
	private static <T> void printOps(List<TransformOperation<T>> ops) {
		
		System.out.println("Transformations:");
		for (TransformOperation<T> op : ops) {
			switch (op.op) {
			case Delete:
				System.out.format("Delete at index %d\n", op.index);
				break;
			case Insert:
				System.out.format("Insert at index %d value '%s' from index %d\n", op.index, op.value.toString(), op.valueIndex);
				break;
			case Update:
				System.out.format("Update at index %d with value '%s' from index %d\n", op.index, op.value.toString(), op.valueIndex);
				
			}
		}
		
	}
	
	
	
}
