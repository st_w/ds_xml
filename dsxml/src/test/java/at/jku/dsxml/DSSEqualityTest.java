package at.jku.dsxml;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import at.jku.dsxml.schema.DSSAttributeDecl;
import at.jku.dsxml.schema.DSSAttributeDecl.Use;
import at.jku.dsxml.schema.DSSComplexType;
import at.jku.dsxml.schema.DSSComplexType.ContentType;
import at.jku.dsxml.schema.DSSComplexType.Method;
import at.jku.dsxml.schema.DSSElementDecl;
import at.jku.dsxml.schema.DSSElementRef;
import at.jku.dsxml.schema.DSSGroup;
import at.jku.dsxml.schema.DSSGroup.Composition;
import at.jku.dsxml.schema.DSSNode;
import at.jku.dsxml.schema.DSSParticle;
import at.jku.dsxml.schema.DSSSimpleType;
import at.jku.dsxml.schema.DSSType;
import at.jku.dsxml.schema.DSSWildcard;
import at.jku.dsxml.schema.compare.IComparable;


public class DSSEqualityTest {

	// instance types
	static int refAttDecl, refComplexType, refElemDecl, refElemRef, refGroup, refSimpleType, refWildcard;
	// abstract types
	static int refNode, refParticle, refType;
	
	
	static final int crefAttDecl = 404284337, crefComplexType = -829902704, 
			crefElemDecl = -713296762, crefElemRef = -1317059032, crefGroup = -1091568908, 
			crefSimpleType = -1433696687, crefWildcard = 1056,
			crefNode = crefElemDecl, crefParticle = crefGroup, crefType = crefComplexType;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		refAttDecl = getSampleAttDecl().contentHash();
		refComplexType = getSampleComplexType().contentHash();
		refElemDecl = getSampleElemDecl().contentHash();
		refElemRef = getSampleElemRef().contentHash();
		refGroup = getSampleGroup().contentHash();
		refSimpleType = getSampleSimpleType(null).contentHash();
		refWildcard = getSampleWildcard().contentHash();
		
		refNode = ((DSSNode)getSampleElemDecl()).contentHash();
		refParticle = ((DSSParticle)getSampleGroup()).contentHash();
		refType = ((DSSType)getSampleComplexType()).contentHash();
		
		//System.out.println(refAttDecl + " " + refComplexType + " " + refElemDecl + " " + refElemRef + " " + refGroup + " " + refSimpleType + " " + refWildcard);
	}

	
	private static DSSAttributeDecl getSampleAttDecl() {
		DSSAttributeDecl a = new DSSAttributeDecl(XMLName.fromString("{a}B"));
		a.setType(getSampleComplexType());
		a.setUse(Use.OPTIONAL);
		return a;
	}
	
	private static DSSComplexType getSampleComplexType() {
		DSSComplexType t = new DSSComplexType();
		t.setAbstract(false);
		t.setAttributeWildcard(getSampleWildcard());
		t.setBase(null);
		t.setContentType(ContentType.COMPLEX);
		t.setMethod(Method.EXTENSION);
		t.setMixed(false);
		t.setName(XMLName.fromString("{a}B"));
		t.setParticle(getSampleGroup());
		return t;
	}
	
	private static DSSElementDecl getSampleElemDecl() {
		DSSElementDecl e = new DSSElementDecl(XMLName.fromString("{a}B"));
		e.setType(getSampleComplexType());
		return e;
	}
	
	private static DSSElementDecl getSampleElemDeclSimple() {
		DSSElementDecl e = new DSSElementDecl(XMLName.fromString("{a}B"));
		e.setType(getSampleSimpleType(null));
		return e;
	}
	
	private static DSSElementRef getSampleElemRef() {
		DSSElementRef r = new DSSElementRef();
		r.setMaxOccurs(2);
		r.setMinOccurs(0);
		r.setRef(getSampleElemDeclSimple());
		return r;
	}
	
	private static DSSGroup getSampleGroup() {
		DSSGroup g = new DSSGroup();
		g.setComposition(Composition.SEQUENCE);
		g.setMinOccurs(1);
		g.setMaxOccurs(3);
		g.addChild(getSampleElemRef());
		g.addChild(getSampleElemRef());
		g.addChild(getSampleWildcard());
		return g;
	}
	
	private static DSSSimpleType getSampleSimpleType(DSSType base) {
		DSSSimpleType t = new DSSSimpleType();
		t.setBase(base);
		t.setMethod(DSSSimpleType.Method.EXTENSION);
		t.setName(XMLName.fromString("{a}B"));
		return t;
	}
	
	private static DSSWildcard getSampleWildcard() {
		DSSWildcard w = new DSSWildcard();
		w.setMaxOccurs(3);
		w.setMinOccurs(2);
		return w;
	}

	
	
	
	
	@Test
	public void contentHashEquals() {
		assertEquals(refAttDecl, getSampleAttDecl().contentHash());
		assertEquals(refComplexType, getSampleComplexType().contentHash());
		assertEquals(refElemDecl, getSampleElemDecl().contentHash());
		assertEquals(refElemRef, getSampleElemRef().contentHash());
		assertEquals(refGroup, getSampleGroup().contentHash());
		assertEquals(refSimpleType, getSampleSimpleType(null).contentHash());
		assertEquals(refWildcard, getSampleWildcard().contentHash());
		
		assertEquals(refNode, ((DSSNode)getSampleElemDecl()).contentHash());
		assertEquals(refParticle, ((DSSParticle)getSampleGroup()).contentHash());
		assertEquals(refType, ((DSSType)getSampleComplexType()).contentHash());
	}
	
	
	@Test
	public void constantHash() {
		assertEquals(crefAttDecl, getSampleAttDecl().contentHash());
		assertEquals(crefComplexType, getSampleComplexType().contentHash());
		assertEquals(crefElemDecl, getSampleElemDecl().contentHash());
		assertEquals(crefElemRef, getSampleElemRef().contentHash());
		assertEquals(crefGroup, getSampleGroup().contentHash());
		assertEquals(crefSimpleType, getSampleSimpleType(null).contentHash());
		assertEquals(crefWildcard, getSampleWildcard().contentHash());
		
		assertEquals(crefNode, ((DSSNode)getSampleElemDecl()).contentHash());
		assertEquals(crefParticle, ((DSSParticle)getSampleGroup()).contentHash());
		assertEquals(crefType, ((DSSType)getSampleComplexType()).contentHash());
	}
	
	
	
	@Test
	public void attributeDeclEquality() {
		
		DSSAttributeDecl a1 = new DSSAttributeDecl(XMLName.fromString("{a}B"));
		DSSAttributeDecl a2 = new DSSAttributeDecl(XMLName.fromString("{a}B"));
		assertContentEquals(a1, a2);
		
		a1.setUse(Use.PROHIBITED);
		assertContentDiffers(a1, a2);
		a2.setUse(Use.PROHIBITED);
		assertContentEquals(a1, a2);
		
		a1.setType(new DSSComplexType());
		assertContentDiffers(a1, a2);
		a2.setType(new DSSComplexType());
		assertContentEquals(a1, a2);
		
	}
	
	
	
	@Test
	public void complexTypeEquality() {
		
		DSSComplexType t1 = new DSSComplexType();
		DSSComplexType t2 = new DSSComplexType();
		assertContentEquals(t1, t2);
		
		t1.setAbstract(true);
		assertContentDiffers(t1, t2);
		t2.setAbstract(true);
		assertContentEquals(t1, t2);
		
		t1.setAttributeWildcard(new DSSWildcard());
		assertContentDiffers(t1, t2);
		t2.setAttributeWildcard(new DSSWildcard());
		assertContentEquals(t1, t2);

		t1.setBase(new DSSSimpleType());
		assertContentDiffers(t1, t2);
		t2.setBase(new DSSSimpleType());
		assertContentEquals(t1, t2);
		
		t1.setContentType(ContentType.COMPLEX);
		assertContentDiffers(t1, t2);
		t2.setContentType(ContentType.COMPLEX);		
		assertContentEquals(t1, t2);
		
		t1.setMethod(Method.RESTRICTION);
		assertContentDiffers(t1, t2);
		t2.setMethod(Method.RESTRICTION);
		assertContentEquals(t1, t2);
		
		t1.setMixed(true);
		assertContentDiffers(t1, t2);
		t2.setMixed(true);
		assertContentEquals(t1, t2);
		
		t1.setParticle(new DSSGroup());
		assertContentDiffers(t1, t2);
		t2.setParticle(new DSSGroup());
		assertContentEquals(t1, t2);
		
	}
	
	
	
	@Test
	public void elementDeclEquality() {
		
		DSSElementDecl e1 = new DSSElementDecl(XMLName.fromString("{a}B"));
		DSSElementDecl e2 = new DSSElementDecl(XMLName.fromString("{a}B"));
		assertContentEquals(e1, e2);
		
		e1.setType(new DSSComplexType());
		assertContentDiffers(e1, e2);
		e2.setType(new DSSComplexType());
		assertContentEquals(e1, e2);
		
	}
	
	
	@Test
	public void elementRefEquality() {
		
		DSSElementRef e1 = new DSSElementRef();
		DSSElementRef e2 = new DSSElementRef();
		assertContentEquals(e1, e2);
		
		e1.setMaxOccurs(10);
		assertContentDiffers(e1, e2);
		e2.setMaxOccurs(10);
		assertContentEquals(e1, e2);
		
		e1.setMinOccurs(2);
		assertContentDiffers(e1, e2);
		e2.setMinOccurs(2);
		assertContentEquals(e1, e2);
		
		
		e1.setRef(getSampleElemDeclSimple());
		assertContentDiffers(e1, e2);
		e2.setRef(getSampleElemDeclSimple());
		assertContentEquals(e1, e2);
		
	}
	
	
	@Test
	public void groupEquality() {
		
		DSSGroup g1 = new DSSGroup();
		DSSGroup g2 = new DSSGroup();
		assertContentEquals(g1, g2);
		
		g1.setComposition(Composition.ALL);
		assertContentDiffers(g1, g2);
		g2.setComposition(Composition.ALL);
		assertContentEquals(g1, g2);
		
		g1.setMaxOccurs(10);
		assertContentDiffers(g1, g2);
		g2.setMaxOccurs(10);
		assertContentEquals(g1, g2);
		
		g1.setMinOccurs(2);
		assertContentDiffers(g1, g2);
		g2.setMinOccurs(2);
		assertContentEquals(g1, g2);
		
	}
	
	
	
	@Test
	public void simpleTypeEquality() {
		
		DSSSimpleType t1 = new DSSSimpleType();
		DSSSimpleType t2 = new DSSSimpleType();
		assertContentEquals(t1, t2);
		
		t1.setName(XMLName.fromString("{a}B"));
		assertContentDiffers(t1, t2);
		t2.setName(XMLName.fromString("{a}B"));
		assertContentEquals(t1, t2);

		t1.setMethod(DSSSimpleType.Method.UNION);
		assertContentDiffers(t1, t2);
		t2.setMethod(DSSSimpleType.Method.UNION);
		assertContentEquals(t1, t2);
		
		t1.setBase(new DSSSimpleType());
		assertContentDiffers(t1, t2);
		t2.setBase(new DSSSimpleType());
		assertContentEquals(t1, t2);
		
	}
	
	
	
	@Test
	public void wildcardEquality() {
		
		DSSWildcard w1 = new DSSWildcard();
		DSSWildcard w2 = new DSSWildcard();
		assertContentEquals(w1, w2);
		
		w1.setMaxOccurs(10);
		assertContentDiffers(w1, w2);
		w2.setMaxOccurs(10);
		assertContentEquals(w1, w2);
		
		w1.setMinOccurs(2);
		assertContentDiffers(w1, w2);
		w2.setMinOccurs(2);
		assertContentEquals(w1, w2);
		
	}
	
	
	
	
	private void assertContentEquals(IComparable a, IComparable b) {
		assertTrue(a.contentEquals(b));
		assertTrue(b.contentEquals(a));
		assertTrue(a.contentHash() == b.contentHash());
	}
	
	
	private void assertContentDiffers(IComparable a, IComparable b) {
		assertFalse(a.contentEquals(b));
		assertFalse(b.contentEquals(a));
	}
	

}
