package at.jku.dsxml.helpers;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * SAX error handler which throws a SAXException on any error or warning
 * @author stw
 */
public class SimpleSAXErrorHandler implements ErrorHandler  {

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		throw exception;
		//System.out.println("WARNING: " + exception.toString());
	}
	
	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		throw exception;
		//System.out.println("FATAL: " + exception.toString());	
	}
	
	@Override
	public void error(SAXParseException exception) throws SAXException {
		throw exception;
		//System.out.println("ERROR: " + exception.toString());
	}

}
