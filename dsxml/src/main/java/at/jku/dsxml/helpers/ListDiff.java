package at.jku.dsxml.helpers;

import java.util.ArrayList;
import java.util.List;

import at.jku.dsxml.helpers.ListDiff.TransformOperation.OperationType;

/**
 * Helps comparing generic lists
 * @author stw
 */
public class ListDiff {
	
	
	/**
	 * Finds a minimal sequence of transformations to convert source into target
	 * @param la source list
	 * @param lb target list 
	 * @return transform operations to convert source to target
	 */
	public static <T> List<TransformOperation<T>> compareLists(List<T> la, List<T> lb) {
		
		// see also:
		// http://www.drdobbs.com/database/pattern-matching-the-gestalt-approach/184407970?pgno=5

		List<TransformOperation<T>> transformations = new ArrayList<>();
		
		Subsequence ss = longestCommonSubsequence(la, lb);
		if (ss.length > 0) {
			
			List<TransformOperation<T>> ops;
			
			List<T> beforeA = la.subList(0, ss.offsetA);
			List<T> beforeB = lb.subList(0, ss.offsetB);
			ops = compareLists(beforeA, beforeB);
			transformations.addAll(ops);
			
			List<T> afterA = la.subList(ss.offsetA+ss.length, la.size());
			List<T> afterB = lb.subList(ss.offsetB+ss.length, lb.size());
			ops = compareLists(afterA, afterB);
			for (TransformOperation<T> op : ops) {
				if (op.index >= 0) op.index += ss.offsetB+ss.length;
				if (op.valueIndex >= 0) op.valueIndex += ss.offsetB+ss.length;
			}
			transformations.addAll(ops);
			
		} else {
			
			//TODO: implement more intelligent updating
			for (int q = 0; q < la.size() && q < lb.size(); q++) {
				transformations.add(new TransformOperation<T>(OperationType.Update, q, lb.get(q), q));
			}
			for (int q = la.size()-1; q >= lb.size(); q--) {
				transformations.add(new TransformOperation<T>(OperationType.Delete, q, null, -1));
			}
			for (int q = la.size(); q < lb.size(); q++) {
				transformations.add(new TransformOperation<T>(OperationType.Insert, q, lb.get(q), q));
			}
			
		}
		
		return transformations;
	}
	
	
	/**
	 * Applies a sequence of transformations to convert the given source list to the target list
	 * @param l source list
	 * @param ops transformation operations
	 * @return target list
	 */
	public static <T> List<T> applyTransformations(List<T> l, List<TransformOperation<T>> ops) {
		for (TransformOperation<T> op : ops) {
			
			switch (op.op) {
			case Delete:
				l.remove(op.index);
				break;
			case Insert:
				l.add(op.index, op.value);
				break;
			case Update:
				l.set(op.index, op.value);
				break;
			}
		}
		return l;
	}

	
	/**
	 * Describes a single transformation operation on a list
	 * @author stw
	 * @param <T> type of the list elements
	 */
	public static class TransformOperation<T> {
		
		public enum OperationType {
			Insert,
			Update,
			Delete
		}
		
		private TransformOperation(OperationType op, int index, T value, int valueIndex) {
			this.op = op;
			this.index = index;
			this.value = value;
			this.valueIndex = valueIndex;
		}
		
		public final OperationType op;
		//TODO: make inidices read-only from outside
		public int index;
		public final T value;
		public int valueIndex;
		
	}
	
	
	
	/**
	 * Finds the longest common subsequence of two lists
	 * @param la list A
	 * @param lb list B
	 * @return longest common subsequence (which may describe a sequence of length zero)
	 */
	public static <T> Subsequence longestCommonSubsequence(List<T> la, List<T> lb) {
		
		// TODO: implement in a more efficient way
		// see also:
		// https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
		// https://en.wikipedia.org/wiki/Longest_increasing_subsequence
		
		Subsequence ss = new ListDiff.Subsequence();
		
		for (int q = 0; q < la.size()-ss.length; q++) {
			int p = lb.indexOf(la.get(q));
			if (p < 0) continue;
			
			List<Integer> seq = new ArrayList<>();
			seq.add(p);
			
			for (int w = p+1; w < lb.size(); w++) {
				for (int sn = seq.size()-1; sn >= 0; sn--) {
					int ssLen = w-seq.get(sn);
					if (q+ssLen >= la.size() || lb.get(w) != la.get(q+ssLen)) {
						//System.out.format("Found sequence index(A/B) %d/%d length %d\n", q, seq.get(sn),ssLen);
						if (ss.length < ssLen) {
							ss.length = ssLen;
							ss.offsetA = q;
							ss.offsetB = seq.get(sn);
						}
						seq.remove(sn);
					}
				}
				if (lb.get(w) == la.get(q)) seq.add(w);
			}
			for (int sn = seq.size()-1; sn >= 0; sn--) {
				int ssLen = lb.size()-seq.get(sn);
				//System.out.format("FFound sequence index(A/B) %d/%d length %d\n", q, seq.get(sn),ssLen);
				if (ss.length < ssLen) {
					ss.length = ssLen;
					ss.offsetA = q;
					ss.offsetB = seq.get(sn);
				}
			}
			
		}
		
		return ss;
		
	}
	
	/**
	 * Subsequence in two lists A and B
	 * @author stw
	 */
	public static class Subsequence {
		
		public int offsetA = 0;
		public int offsetB = 0;
		public int length = 0;
		
	}
	
	
	
	

}
