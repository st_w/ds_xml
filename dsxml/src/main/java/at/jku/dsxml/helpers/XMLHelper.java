package at.jku.dsxml.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import nu.xom.Builder;
import nu.xom.Document;


/**
 * Helper class for typical XML operations
 * @author stw
 */
public class XMLHelper {

	/**
	 * Loads a XML document from a file
	 * @param f XML file
	 * @return XML Document
	 * @throws SAXException on parsing errors
	 * @throws FileNotFoundException if the XML file is not accessible
	 */
	public static Document LoadDocument(File f) throws SAXException, FileNotFoundException {
		FileInputStream xml = new FileInputStream(f);
		return LoadDocument(xml);
	}
	
	/**
	 * Loads a XML document from a stream
	 * @param xml XML input streaml
	 * @return XML document
	 * @throws SAXException on parsing errors
	 */
	public static Document LoadDocument(InputStream xml) throws SAXException {
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
	        factory.setValidating(false);
	        factory.setNamespaceAware(true);
	
	        SAXParser parser = factory.newSAXParser();
	        XMLReader reader = parser.getXMLReader();
	        reader.setErrorHandler(new SimpleSAXErrorHandler());
	
	        Builder builder = new Builder(reader, false);
	        Document d = builder.build(xml);
			return d;
			
		} catch (Exception e) {
			throw new SAXException(e);
		}
	}
	
	
	/**
	 * Validates a XML document using a XML schema definition (XSD)
	 * @param xml XML stream to validate
	 * @param xsd XML schema definition
	 * @throws SAXException on parsing errors
	 * @throws IOException on XML/XSD I/O errors
	 */
	public static void ValidateDocument(InputStream xml, InputStream xsd) throws SAXException, IOException
	{
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new StreamSource(xsd));
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(xml));	    
	}
	
	
	
	
}
