package at.jku.dsxml.schema;

import java.util.ArrayList;
import java.util.List;

import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataValue;
import at.jku.dsxml.schema.compare.EnumCompareHelper;
import at.jku.dsxml.schema.compare.ListCompareHelper;

/**
 * Represents a DSS simple type
 * A simple type only describes a single value
 * @author stw
 */
public class DSSSimpleType extends DSSType {

	public enum Method {
		EXTENSION, RESTRICTION, LIST, UNION
	}
	
	private Method method;
	private List<DSSSimpleType> children;
	
	public DSSSimpleType() {
		children = new ArrayList<>();
	}
	
	
	public List<DSSSimpleType> getChildren() {
		return new ArrayList<>(children);
	}
	
	public void addChild(DSSSimpleType child) {
		children.add(child);
	}
	
	public void removeChild(DSSSimpleType child) {
		children.remove(child);
	}


	public Method getMethod() {
		return method;
	}


	public void setMethod(Method method) {
		this.method = method;
	}


	@Override
	public DSSData getTemplate() {
		DSSDataValue v= new DSSDataValue("", this, null);
		return v;
	}


	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((children == null) ? 0 : ListCompareHelper.contentHash(children));
		result = prime * result + ((method == null) ? 0 : EnumCompareHelper.contentHash(method));
		return result;
	}


	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSSimpleType other = (DSSSimpleType) obj;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!ListCompareHelper.contentEqual(children, other.children))
			return false;
		if (method != other.method)
			return false;
		assert(contentHash() == other.contentHash());
		return true;
	}
	
	
}
