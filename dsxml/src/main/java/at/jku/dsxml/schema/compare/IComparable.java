package at.jku.dsxml.schema.compare;

/**
 * Defines an interface to compare the content of elements as an alternative to the default
 * comparison methods, which usually compare references for Objects. 
 * This allows to keep the objects default behaviour by using 
 * their default hashcode() and equals() implementation while being able to compare its contents only.
 * @author stw
 */
public interface IComparable {

	boolean contentEquals(Object o);
	int contentHash();
	
}
