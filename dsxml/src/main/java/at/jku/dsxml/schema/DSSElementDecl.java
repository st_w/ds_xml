package at.jku.dsxml.schema;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;

/**
 * Represents a DSS element declaration
 * @author stw
 */
public class DSSElementDecl extends DSSNode {
	
	
	public DSSElementDecl() {
		
	}
	public DSSElementDecl(XMLName name) {
		this.setName(name);
	}
	
	
	
	public DSSElement instantiate() {
		DSSElement e = new DSSElement();
		e.setName(this.getName());
		e.setType(this.getType());
		DSSData d = this.getType().getTemplate();
		if (d != null) {
			e.setData(d.instantiate());
		}
		return e;
	}

}
