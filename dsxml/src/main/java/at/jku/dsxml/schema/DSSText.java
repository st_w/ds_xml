package at.jku.dsxml.schema;

/**
 * Represents a DSS text node
 * Stores/describes text content in a DSS element node.
 * @author stw
 */
public class DSSText extends DSSNode {
	
	private String text;
	
	public DSSText() {
		this(null);
	}
	public DSSText(String text) {
		this.setText(text);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}
	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSText other = (DSSText) obj;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	

}
