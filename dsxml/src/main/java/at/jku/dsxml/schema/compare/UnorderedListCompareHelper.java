package at.jku.dsxml.schema.compare;

import java.util.ArrayList;
import java.util.List;

/**
 * Helps comparing unordered lists using the IComparable interface.
 * @author stw
 */
public class UnorderedListCompareHelper {

	
    @SuppressWarnings("rawtypes")
	public static boolean contentEqual(List<? extends IComparable> list, Object o) {
        if (o == list)
            return true;
        if (!(o instanceof List))
            return false;

        List list2 = (List)o;
        
        if (list.size() != list2.size())
        	return false;
        
        List<CompareEntry> cl = new ArrayList<>();
        for (IComparable ic : list) {
        	cl.add(new CompareEntry(ic.contentHash(), ic));
        }
        
        for (Object oc : list2) {
        	if (!(oc instanceof IComparable)) return false;
        	IComparable ic = (IComparable) oc;
        	boolean result = cl.remove(new CompareEntry(ic.contentHash(), ic));
        	if (!result) return false;
        }
        
        return cl.isEmpty();
    }


    public static int contentHash(List<? extends IComparable> list) {
    	//final int prime = 31;
        int hashCode = 1;
        for (IComparable e : list)
            hashCode += (e==null ? 0 : e.contentHash());
        return hashCode;
    }

    
    private static class CompareEntry {
    	
    	final int hash;
    	final IComparable value;
    	
    	public CompareEntry(int hash, IComparable value) {
    		this.hash = hash;
    		this.value = value;
    	}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + hash;
			result = prime * result + ((value == null) ? 0 : value.contentHash());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CompareEntry other = (CompareEntry) obj;
			if (hash != other.hash)
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.contentEquals(other.value))
				return false;
			return true;
		}
    	
    }
	
	
}
