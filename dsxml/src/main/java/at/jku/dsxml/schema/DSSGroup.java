package at.jku.dsxml.schema;

import java.util.ArrayList;
import java.util.List;

import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.schema.compare.EnumCompareHelper;
import at.jku.dsxml.schema.compare.ListCompareHelper;

/**
 * Represents a DSS composition group.
 * Composition groups are used in complex types for describing 
 * repeated or alternative allowed child elements.
 * @author stw
 */
public class DSSGroup extends DSSParticle {

	public enum Composition {
		SEQUENCE, CHOICE, ALL
	}
	
	private Composition composition;
	private List<DSSParticle> children;
	
	
	public DSSGroup() {
		this.children = new ArrayList<DSSParticle>();
	}
	

	public Composition getComposition() {
		return composition;
	}

	public void setComposition(Composition composition) {
		this.composition = composition;
	}


	public List<DSSParticle> getChildren() {
		return new ArrayList<DSSParticle>(children);
	}


	public void addChild(DSSParticle child) {
		this.children.add(child);
	}
	
	public void addChild(int index, DSSParticle child) {
		this.children.add(index, child);
	}
	
	public void removeChild(DSSParticle child) {
		this.children.remove(child);
	}
	
	@Override
	public DSSData getTemplate(DSSDataContainer d, String name) {
		if (d == null) throw new IllegalArgumentException("DSSDataContainer must not be null");

		if (this.getMaxOccurs() == 0) {
			// element must not occur			
			
		} else
		if (this.getMaxOccurs() == 1) {
			
			for (DSSParticle child : children) {
				DSSData data = child.getTemplate(d);
				
				if (child instanceof DSSElementRef) {
					if (data != null) {
						d.addChild(data);
					}
				}
			}
			
		} else {
			
			/*
			// if multiple instances of this structure are allowed ..
			if (this.children.size() == 1 && this.children.get(0) instanceof DSSElementRef) {
				// .. convert value into array if only a single element child, ..
				DSSData data = children.get(0).getTemplate(d);
				if (data != null) {
					DSSDataArray dataArray = new DSSDataArray(data.getName(), data.getType(), data.getSubType());
					d.addChild(dataArray);
				}
				
			} else {
			
				// .. otherwise add generic elements container
				d.addChild(new DSSDataArray(d.findName("elements"), null, this));
			}
			*/
			
			if (!getAllElements().isEmpty()) {
				d.addChild(new DSSDataArray(d.findName("elements"), null, this));
			}
			
		}
		return d;
	}


	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((children == null) ? 0 : ListCompareHelper.contentHash(children));
		result = prime * result + ((composition == null) ? 0 : EnumCompareHelper.contentHash(composition));
		return result;
	}


	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSGroup other = (DSSGroup) obj;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!ListCompareHelper.contentEqual(children, other.children))
			return false;
		if (composition != other.composition)
			return false;
		assert(contentHash() == other.contentHash());
		return true;
	}
	
}
