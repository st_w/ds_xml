package at.jku.dsxml.schema;

import org.atteo.evo.inflector.English;

import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.data.DSSDataValue;

/**
 * Represents a DSS element reference.
 * Used in complex types to describe the allowed child elements.
 * @author stw
 */
public class DSSElementRef extends DSSParticle {
	

	private DSSElementDecl ref;
	
	
	public DSSElementRef() {
		
	}
	public DSSElementRef(DSSElementDecl ref) {
		if (ref == null) {
			throw new IllegalArgumentException("ref must not be null");
		}
		this.ref = ref;		
	}

	
	public DSSElementDecl getRef() {
		return ref;
	}
	
	public void setRef(DSSElementDecl ref) {
		if (ref == null) {
			throw new IllegalArgumentException("ref must not be null");
		}
		this.ref = ref;
	}
	
	@Override
	public DSSData getTemplate(DSSDataContainer d, String name) {
		if (this.getMaxOccurs() == 0) {
			// Element must not occur
			return null;
		}
		String defaultName = this.getRef().getName().Name;
		if (this.getMaxOccurs() != 1) {
			defaultName = English.plural(defaultName);
		}
		if (name == null) {
			if (d == null) name = defaultName;
			else name = d.findName(defaultName);
		} else {
			if (d != null) name = d.findName(name);
		}
		DSSData v;
		if (this.getMaxOccurs() != 1) {
			v = new DSSDataArray(name, this.getRef().getType(), this);
		} else {
			v = new DSSDataValue(name, this.getRef().getType(), this);
		}
		//if (d != null) d.addChild(v);
		return v;
	}
	
	
	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((ref == null) ? 0 : ref.contentHash());
		return result;
	}
	
	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSElementRef other = (DSSElementRef) obj;
		if (ref == null) {
			if (other.ref != null)
				return false;
		} else if (!ref.contentEquals(other.ref))
			return false;
		assert(contentHash() == other.contentHash());
		return true;
	}

	
}
