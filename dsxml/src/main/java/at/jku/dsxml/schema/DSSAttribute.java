package at.jku.dsxml.schema;

import at.jku.dsxml.data.DSSDataValue;
import at.jku.dsxml.data.IDataType;

/**
 * Represents a DSS attribute instance with name, type and value
 * @author stw
 */
public class DSSAttribute extends DSSDataValue {
	
	/**
	 * Creates a DSS attribute
	 * @param name attribute name
	 * @param type type of attribute value
	 * @param subtype defining element node
	 */
	public DSSAttribute(String name, DSSType type, IDataType subtype) {
		super(name, type, subtype);
	}
	
	
	@Override
	public DSSAttribute instantiate() {
		DSSAttribute i = new DSSAttribute(this.getName(), this.getType(), this.getSubType());
		assert(this.getSubType() instanceof DSSAttributeDecl);
		return i;
	}

}
