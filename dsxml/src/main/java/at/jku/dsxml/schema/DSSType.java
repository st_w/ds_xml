package at.jku.dsxml.schema;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.schema.compare.IComparable;

/**
 * Represents a abstract base for DSS types
 * Types describe the content of element and attribute nodes.
 * @author stw
 */
public abstract class DSSType implements IComparable {

	private DSSType base;
	private XMLName name;		// optional, allowed only as child of <schema>
	

	public DSSType getBase() {
		return base;
	}

	public void setBase(DSSType base) {
		this.base = base;
	}

	public XMLName getName() {
		return name;
	}

	public void setName(XMLName name) {
		this.name = name;
	}
	
	
	
	abstract public DSSData getTemplate();

	
	@Override
	public int contentHash() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.contentHash());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSType other = (DSSType) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.contentEquals(other.base))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
