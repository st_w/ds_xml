package at.jku.dsxml.schema.compare;

/**
 * Helps comparing enums independently from the execution environment. This is needed
 * because the default implementation of Java enum hashcode() may differ when the program is
 * executed several times.
 * @author stw
 */
public class EnumCompareHelper {

	// not needed
	/*
	public static <T extends Enum<T>> boolean contentEqual(T e, Object o) {
		return e.equals(o);
    }
    */

	// enum's hashcode is not stable between runs
    public static <T extends Enum<T>> int contentHash(T e) {
    	if (e == null) return 59;
    	int hash = e.name().hashCode() * (1+e.ordinal());
    	hash += e.getDeclaringClass().getName().hashCode();
    	return hash;
    }
	
	
	
}
