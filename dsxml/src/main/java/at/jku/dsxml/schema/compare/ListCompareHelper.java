package at.jku.dsxml.schema.compare;

import java.util.List;
import java.util.ListIterator;


/**
 * Helps comparing ordered lists using the IComparable interface.
 * @author stw
 */
public class ListCompareHelper {

	
    @SuppressWarnings("rawtypes")
	public static boolean contentEqual(List<? extends IComparable> list, Object o) {
        if (o == list)
            return true;
        if (!(o instanceof List))
            return false;

        ListIterator<? extends IComparable> e1 = list.listIterator();
        ListIterator e2 = ((List) o).listIterator();
        while(e1.hasNext() && e2.hasNext()) {
        	IComparable o1 = e1.next();
            Object o2 = e2.next();
            if (!(o1==null ? o2==null : o1.contentEquals(o2)))
                return false;
        }
        return !(e1.hasNext() || e2.hasNext());
    }


    public static int contentHash(List<? extends IComparable> list) {
    	final int prime = 31;
        int hashCode = 1;
        for (IComparable e : list)
            hashCode = prime * hashCode + (e==null ? 0 : e.contentHash());
        return hashCode;
    }
	
	
}
