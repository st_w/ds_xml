package at.jku.dsxml.schema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.data.IDataType;
import at.jku.dsxml.schema.DSSGroup.Composition;
import at.jku.dsxml.schema.compare.IComparable;

/**
 * Represents DSS particles
 * Particles are an abstract base for parts of a complex type description, which may
 * occur multiple times and may contain child particles. Particles describes minimum and 
 * maximum occurrence.
 * @author stw
 */
public abstract class DSSParticle implements IDataType, IComparable {

	private int minOccurs = 1, maxOccurs = 1;
	
	public int getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(int minOccurs) {
		this.minOccurs = minOccurs;
	}

	public int getMaxOccurs() {
		return maxOccurs;
	}

	public void setMaxOccurs(int maxOccurs) {
		this.maxOccurs = maxOccurs;
	}
	
	public DSSData getTemplate() {
		return getTemplate(null);
	}
	public DSSData getTemplate(DSSDataContainer d) {
		return getTemplate(d, null);
	}
	abstract public DSSData getTemplate(DSSDataContainer d, String name);
	
	
	/**
	 * Creates a depth-first iterator to iterate over all the child particles starting with this particle
	 * @return a depth-first iterator
	 */
	public Iterator<DSSParticle> getDFIterator() {
		final DSSParticle root = this;
				
		return new Iterator<DSSParticle>() {
			
	        Stack<DSSParticle> stack = new Stack<>();
	        {{
	        	if (root.maxOccurs != 0) {
	        		stack.push(root);
	        	}
	        }}
	        
			@Override
			public DSSParticle next() {
				DSSParticle node = stack.pop();
				if (node.maxOccurs != 0 && node instanceof DSSGroup) {
					List<DSSParticle> children = ((DSSGroup) node).getChildren();
					for (int q = children.size()-1; q >= 0; q--) {
						DSSParticle child = children.get(q);
						if (child.maxOccurs != 0) {
							stack.add(children.get(q));
						}
					}
				}
				return node;
			}
			
			@Override
			public boolean hasNext() {
				return !stack.isEmpty();
			}
		};
	}
	
	/**
	 * Retrieve all child elements of this particle and its children
	 * @return a list of all element references of this particle and its children
	 */
	public List<DSSElementRef> getAllElements() {
		List<DSSElementRef> elems = new ArrayList<>();
		Iterator<DSSParticle> it = getDFIterator();
		while (it.hasNext()) {
			DSSParticle p = it.next();
			if (p instanceof DSSElementRef) {
				elems.add(((DSSElementRef) p));
			}
		}
		return elems;
	}
	
	/**
	 * Validates the given count to the limits of the particle
	 * @param count count number to validate
	 * @return true if valid
	 */
	public boolean isCountValid(int count) {
		if (count < 0) return false;
		if (this.getMaxOccurs() == -1) return (this.getMinOccurs() <= count);
		return (this.getMinOccurs() <= count && count <= this.getMaxOccurs());
	}
	
	
	/**
	 * Creates a particle traverser, which helps traversing particles for a given
	 * sequence of element node instances, starting with this particle
	 * @return particle traverser
	 */
	public ParticleTraverser createTraverser() {
		return new ParticleTraverser(this);
	}
	
	
	
	
	/**
	 * A particle traverser helps iterating over the particles for a given sequence of element
	 * node instances while following the occurrence rules defined by the particles. Depending on
	 * these rules optional particles may be skipped, alternative particles may be chosen
	 * and particles may be repeated.
	 * @author stw
	 */
	public class ParticleTraverser {
		
		private DSSParticle root;
		private int count = 0;
		private int groupIndex = -1;		//DSSGroup only
		private ParticleTraverser subTraverser = null;		//DSSGroup only
		
		private ParticleTraverser(DSSParticle root) {
			this.root = root;
		}
		
		
		private boolean isCountLEMax(DSSParticle p, int count) {
			return (p.getMaxOccurs() == -1 || count <= p.getMaxOccurs());
		}
		
		/**
		 * Finds the next particle for the given element name and validates the particle state
		 * @param elementName name of the next element
		 * @return Particle for given element name or null if no matching particle was found, but particle is in a valid state
		 * @throws IllegalStateException if no matching particle was found and particle is in an invalid state
		 */
		public DSSParticle next(XMLName elementName) {
			int cntNullValues = 0;		// DSSGroup only
			
			if (root instanceof DSSGroup) {
				DSSGroup g = (DSSGroup) root;
				
				List<DSSParticle> children = g.getChildren();
				DSSParticle result = null;
				
				if (isCountLEMax(g, count+1)) {
					
					if (subTraverser != null) {
						try{
							result = subTraverser.next(elementName);
						} catch (IllegalStateException e) {
							// invalid? look at others only if choice
							if (g.getComposition() != Composition.CHOICE) {
								throw e;
							}
						}
						if (result != null) {
							return result;
						}
					}
					subTraverser = null;
					
					try {
						
						
						while (isCountLEMax(g, count+1)) {
							
							groupIndex++;
							if (groupIndex >= children.size()) {
								if (cntNullValues == children.size() && root.isCountValid(count)) {
									break;
								}
								groupIndex = -1;
								count++;
								cntNullValues = 0;
								continue;
							}
							
							DSSParticle p = children.get(groupIndex);
							
							subTraverser = p.createTraverser();
							try{
								result = subTraverser.next(elementName);
							} catch (IllegalStateException e) {
								// invalid? look at others only if choice
								if (g.getComposition() != Composition.CHOICE) {
									throw e;
								}
							}
							if (result != null) {
								if (g.getComposition() == Composition.CHOICE) {
									// do not look at other children
									groupIndex = children.size();
								}
								return result;							
							}
							cntNullValues++;
						}
					}
					catch (IllegalStateException e) {
						
					}
					
				}
								
				
			} else
			if (root instanceof DSSElementRef) {
				DSSElementRef r = (DSSElementRef) root;
				
				if (elementName != null) {
					if (elementName.equals(r.getRef().getName())) {
						
						if (isCountLEMax(r, count+1)) {
							count++;
							return r;
						}
					}
				}
				
			} else
			if (root instanceof DSSWildcard) {
				DSSWildcard w = (DSSWildcard) root;
				
				if (w.acceptsNode(elementName)) {
					if (isCountLEMax(w, count+1)) {
						count++;
						return w;
					}
				}
				
			} else {
				assert(false);
			}
			
			if (!root.isCountValid(count) || groupIndex > cntNullValues) {
				throw new IllegalStateException("Unexpected element: " + (elementName==null?"<END>":elementName));
			}
			return null;
		}
		
		
	}




	@Override
	public int contentHash() {
		final int prime = 31;
		int result = 1;
		result = prime * result + maxOccurs;
		result = prime * result + minOccurs;
		return result;
	}

	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSParticle other = (DSSParticle) obj;
		if (maxOccurs != other.maxOccurs)
			return false;
		if (minOccurs != other.minOccurs)
			return false;
		return true;
	}
	
	
	
	
	
	
	
}
