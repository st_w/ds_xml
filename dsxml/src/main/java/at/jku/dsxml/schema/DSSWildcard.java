package at.jku.dsxml.schema;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataContainer;

/**
 * Represents a DSS wildcard particle
 * [NOT IMPLEMENTED]
 * A wildcard allows to omit a detailed list of allowed child nodes for an element by 
 * describing only requirements for the child nodes.
 * @author stw
 */
public class DSSWildcard extends DSSParticle {

	
	//FIXME: implement wildcard; currently acting as any
	
	public DSSWildcard() {
		
	}
	
	
	
	public boolean acceptsNode(XMLName name) {
		return true;
	}
	
	
	@Override
	public DSSData getTemplate(DSSDataContainer d, String name) {
		DSSDataArray data = new DSSDataArray(d.findName("any"), null, this);
		d.addChild(data);
		return data;
	}

}
