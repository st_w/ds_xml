package at.jku.dsxml.schema;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.data.DSSDataValue;
import at.jku.dsxml.schema.compare.EnumCompareHelper;
import at.jku.dsxml.schema.compare.UnorderedListCompareHelper;

/**
 * Represents a DSS complex type
 * A complex type describes an element which may contain sub-elements and attributes
 * @author stw
 */
public class DSSComplexType extends DSSType {
	
	public enum Method {
		EXTENSION, RESTRICTION, OPENCONTENT
	}
	public enum ContentType {
		SIMPLE, COMPLEX
	}
	
	private Method method;
	private ContentType contentType;
	private DSSParticle particle;
	private List<DSSAttributeDecl> attributes;
	private DSSWildcard attWildcard = null;
	private boolean mixed = false;
	private boolean isabstract = false;
	private DSSData template = null;
	
	public DSSComplexType() {
		attributes = new ArrayList<>();
	}
	


	public DSSParticle getParticle() {
		return particle;
	}

	public void setParticle(DSSParticle particle) {
		this.particle = particle;
	}
	
	
	/*
	@SuppressWarnings("unchecked")
	public List<DSSGroup> getGroups() {
		return new ArrayList<DSSGroup>((List<DSSGroup>)((Object)groups));
	}
	
	public void addChild(DSSGroup group) {
		groups.add(group);
	}
	
	public void removeChild(DSSGroup group) {
		groups.remove(group);
	}
	
	public DSSElementRef getElement() {
		if (groups.isEmpty()) return null;
		return (DSSElementRef)groups.get(0);
	}
	
	public void setElement(DSSElementRef elem) {
		groups.clear();
		groups.add(elem);
	}
	*/

	public List<DSSAttributeDecl> getAttributes() {
		return new ArrayList<>(attributes);
	}
	
	public void addAttribute(DSSAttributeDecl attr) {
		attributes.add(attr);
	}
	
	public void removeAttribute(DSSAttributeDecl attr) {
		attributes.remove(attr);
	}
	
	public DSSAttributeDecl getAttribute(XMLName name) {
		for (DSSAttributeDecl a : attributes) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}


	public Method getMethod() {
		return method;
	}


	public void setMethod(Method method) {
		this.method = method;
	}
	
	
	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}


	public boolean isMixed() {
		return mixed;
	}


	public void setMixed(boolean mixed) {
		this.mixed = mixed;
	}



	public boolean isAbstract() {
		return isabstract;
	}


	public void setAbstract(boolean isabstract) {
		this.isabstract = isabstract;
	}

	
	public DSSWildcard getAttributeWildcard() {
		return attWildcard;
	}

	public void setAttributeWildcard(DSSWildcard attWildcard) {
		this.attWildcard = attWildcard;
	}

	
	@Override
	public DSSData getTemplate() {
		if (template != null) {
			return template;
		}
		List<DSSComplexType> superTypes = new ArrayList<DSSComplexType>();
		superTypes.add(this);
		while (superTypes.get(0).getMethod() != Method.RESTRICTION) {
			DSSType t = superTypes.get(0).getBase();
			if (t != null && t instanceof DSSComplexType) {
				DSSComplexType tc = (DSSComplexType)t;
				if (tc.method == Method.OPENCONTENT) {
					// FIXME: OpenContent type is not supported (yet) => ignore
				} else {
					superTypes.add(0, tc);
				}
			} else {
				break;
			}
		}
		DSSDataContainer data = new DSSDataContainer(this, this.getParticle());

		// Attributes
		Set<XMLName> dataNames = new HashSet<>();
		for (int i = superTypes.size()-1; i >= 0; i--) {
			DSSComplexType t = superTypes.get(i);
			for (DSSAttributeDecl att : t.getAttributes()) {
				if (!dataNames.contains(att.getName())) {
					data.addChild(att.getTemplate(data));
					dataNames.add(att.getName());
				}
			}
		}
		
		assert(this.contentType != null);
		if (this.contentType == ContentType.COMPLEX) {
			// Elements
			for (DSSComplexType t : superTypes) {
				DSSGroup g = (DSSGroup)t.particle;
				DSSData dat = null;
				if (g != null) dat = g.getTemplate(data, null);
				if (dat == null) {
					// if the transformation fails add generic "elements" container
					data.addChild(new DSSDataArray(data.findName("elements"), null, g));
				}
			}
		} else {
			// Simple Content (Text)
			DSSDataValue dvContent = new DSSDataValue(data.findName("text"), this, null);
			dvContent.setValue(new DSSText());
			data.addChild(dvContent);
		}
		
		template = data;
		return data;
	}



	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((attWildcard == null) ? 0 : attWildcard.contentHash());
		result = prime * result + ((attributes == null) ? 0 : UnorderedListCompareHelper.contentHash(attributes));
		result = prime * result + ((contentType == null) ? 0 : EnumCompareHelper.contentHash(contentType));
		result = prime * result + (isabstract ? 1231 : 1237);
		result = prime * result + ((method == null) ? 0 : EnumCompareHelper.contentHash(method));
		result = prime * result + (mixed ? 1231 : 1237);
		result = prime * result + ((particle == null) ? 0 : particle.contentHash());
		return result;
	}



	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSComplexType other = (DSSComplexType) obj;
		if (attWildcard == null) {
			if (other.attWildcard != null)
				return false;
		} else if (!attWildcard.contentEquals(other.attWildcard))
			return false;
		if (attributes == null) {
			if (other.attributes != null)
				return false;
		} else if (!UnorderedListCompareHelper.contentEqual(attributes, other.attributes))
			return false;
		if (contentType != other.contentType)
			return false;
		if (isabstract != other.isabstract)
			return false;
		if (method != other.method)
			return false;
		if (mixed != other.mixed)
			return false;
		if (particle == null) {
			if (other.particle != null)
				return false;
		} else if (!particle.contentEquals(other.particle))
			return false;
		assert(contentHash() == other.contentHash());
		return true;
	}
	
	
}
