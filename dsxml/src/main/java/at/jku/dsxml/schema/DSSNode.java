package at.jku.dsxml.schema;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.schema.compare.IComparable;

/**
 * Represents a DSS (element) node
 * These nodes are mostly descriptive and describe e.g. name and type of a node.
 * DSS nodes may contain DSS data nodes to store values.
 * @author stw
 */
public abstract class DSSNode implements IComparable {
	private XMLName name;
	private DSSType type;


	public XMLName getName() {
		return name;
	}

	public void setName(XMLName name) {
		this.name = name;
	}


	
	public DSSType getType() {
		return type;
	}

	public void setType(DSSType type) {
		this.type = type;
	}

	
	public int contentHash() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.contentHash());
		return result;
	}

	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSNode other = (DSSNode) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.contentEquals(other.type))
			return false;
		return true;
	}
	
}
