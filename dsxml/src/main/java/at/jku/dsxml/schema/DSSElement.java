package at.jku.dsxml.schema;

import java.util.ArrayList;
import java.util.List;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.data.DSSDataValue;
import at.jku.dsxml.schema.DSSParticle.ParticleTraverser;

/**
 * Represents a DSS element
 * A element may contain sub-elements and attributes.
 * @author stw
 */
public class DSSElement extends DSSElementDecl {

	private DSSData data;
	
	public DSSElement() {
		
	}
	
	public DSSElement(XMLName name) {
		this();
		this.setName(name);
	}

	/**
	 * Retrieves text content of this element
	 * @return text content
	 * @throws UnsupportedOperationException if this element has not text content
	 */
	public String getText() {
		if (data instanceof DSSDataValue) {
			return ((DSSDataValue) data).getText();
		} else
		if (data instanceof DSSDataContainer) {
			return ((DSSDataContainer) data).getText();
		}
		throw new UnsupportedOperationException();
	}

	/**
	 * Sets text content of this element
	 * @param text text content
	 * @throws UnsupportedOperationException if this element does not support text content
	 */
	public void setText(String text) {
		if (data instanceof DSSDataValue) {
			((DSSDataValue) data).setText(text);
			return;
		} else
		if (data instanceof DSSDataContainer) {
			((DSSDataContainer) data).setText(text);
			return;
		}
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Retrieves child nodes of this element
	 * @return child nodes
	 * @throws UnsupportedOperationException if this element does not support child nodes
	 */
	public List<DSSData> getChildren() {
		if (data instanceof DSSDataContainer) {
			return ((DSSDataContainer) data).getChildren();
		}
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Adds a child node to this element
	 * @param child child node
	 * @throws UnsupportedOperationException if this element does not support child nodes
	 */
	public void addChild(DSSData child) {
		if (data instanceof DSSDataContainer) {
			((DSSDataContainer) data).addChild(child);
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
	/**
	 * Gets a child node of this element by name
	 * @param name name of the child node
	 * @return first child node with specified name or null
	 * @throws UnsupportedOperationException if this element does not support child nodes
	 */
	public DSSData getChild(String name) {
		if (data instanceof DSSDataContainer) {
			return ((DSSDataContainer) data).getChild(name);
		}
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Gets an attribute of this element by name
	 * @param name attribute name
	 * @return attribute with specified name or null
	 * @throws UnsupportedOperationException if this element does not support child attributes
	 */
	public DSSAttribute getAttribute(XMLName name) {
		if (data instanceof DSSDataContainer) {
			return ((DSSDataContainer) data).getAttribute(name);
		}
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Adds an attribute to this element
	 * @param name attribute name
	 * @return created attribute
	 * @throws UnsupportedOperationException if this element does not support child attributes
	 */
	public DSSAttribute createAttribute(XMLName name) {
		if (data instanceof DSSDataContainer) {
			return ((DSSDataContainer) data).createAttribute(name);
		}
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Gets a child element node of this element by name
	 * @param name name of the child element
	 * @return first child element node with specified name or null
	 * @throws UnsupportedOperationException if this element does not support child elements
	 */
	public DSSData getElement(String name) {
		if (data instanceof DSSDataContainer) {
			return ((DSSDataContainer) data).getElement(name);
		}
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Gets all attributes of this element
	 * @return a list of all attributes of this element
	 * @throws UnsupportedOperationException if this element does not support attributes
	 */
	public List<DSSAttribute> getAttributes() {
		if (data instanceof DSSDataContainer) {
			return ((DSSDataContainer) data).getAttributes();
		}
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Gets all child elements of this element
	 * @return a list of all child elements of this element
	 * @throws UnsupportedOperationException if this element does not support child elements
	 */
	public List<DSSElement> getElements() {
		if (!(data instanceof DSSDataContainer)) {
			throw new UnsupportedOperationException();
		}
		List<DSSElement> elem = new ArrayList<>();
		for (DSSData d : ((DSSDataContainer)data).getElements()) {
			if (d instanceof DSSDataValue) {
				DSSNode n = ((DSSDataValue) d).getValue();
				if (n != null) {
					elem.add((DSSElement) n);
				}
			} else
			if (d instanceof DSSDataArray) {
				for (DSSNode n : ((DSSDataArray) d).getValues()) {
					if (n != null) {
						elem.add((DSSElement) n);
					}
				}
			} else {
				assert(false);
			}
		}
		return elem;
	}
	
	
	/**
	 * Gets element content
	 * @return element content
	 */
	public DSSData getData() {
		return data;
	}
	
	/**
	 * Sets element content
	 * @param data element content
	 */
	void setData(DSSData data) {
		this.data = data;
	}
	
	
	/**
	 * Creates a ElementTraverser, which traverses all the child (data) nodes of this element
	 * @return Element traverser
	 */
	public ElementTraverser createTraverser() {
		if (!(this.data instanceof DSSDataContainer)) {
			throw new UnsupportedOperationException("Only a DSSDataCollection can be traversed");
		}
		return new ElementTraverser((DSSDataContainer) this.data);
	}
	
	
	
	
	
	/**
	 * Element Traverser helps iterating over the child nodes of the an element by
	 * mapping element node names to data nodes of an element. While element names may occur
	 * multiple times in an element data nodes are have unique names inside an element.
	 * @author stw
	 */
	public class ElementTraverser {
		
		private DSSDataContainer root;
		private ParticleTraverser t;
		
		private DSSData previousElement = null;
		
		
		private ElementTraverser(DSSDataContainer root) {
			this.root = root;
			this.t = ((DSSParticle)root.getSubType()).createTraverser();
		}
		
		public DSSDataContainer getRoot() {
			return root;
		}

		/**
		 * Finds next data node which corresponds to the given element name
		 * @param name element name
		 * @return data node
		 */
		public DSSData getNextElement(XMLName name) {
			
			List<DSSData> elem = root.getElements();
			int i = elem.indexOf(previousElement);
			if (i < 0) i = 0;
			
			DSSParticle nextPart = t.next(name);
			assert(nextPart != null);
			
			for (; i < elem.size(); i++) {
				DSSData curElem = elem.get(i);
				if (curElem.getSubType() instanceof DSSElementRef) {
					if (curElem.getSubType().equals(nextPart)) {
						previousElement = curElem;
						return curElem;
					}
				} else
				if (curElem.getSubType() instanceof DSSGroup) {
					DSSGroup group = (DSSGroup)curElem.getSubType();
					if (group.getAllElements().contains(nextPart)) {
						previousElement = curElem;
						return curElem;
					}
				} else
				if (curElem.getSubType() instanceof DSSWildcard) {
					if (curElem.getSubType().equals(nextPart)) {
						previousElement = curElem;
						return curElem;
					}
				} else {
					assert(false);
				}
			}
			return null;

		}
	}






	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((data == null) ? 0 : data.contentHash());
		return result;
	}

	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSElement other = (DSSElement) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.contentEquals(other.data))
			return false;
		return true;
	}
	



}
