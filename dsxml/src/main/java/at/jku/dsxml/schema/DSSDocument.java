package at.jku.dsxml.schema;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import at.jku.dsxml.XMLName;

/**
 * Represents a DSS document
 * A document is a container for all important information of a DSS model,
 * which is the root node and the schema definitions. Additionally it contains
 * some metadata describing itself for easier user recognizability.
 * @author stw
 *
 */
public class DSSDocument {
	private DSSElement root;
	private Map<XMLName, DSSElementDecl> elemRegistry;
	private String name;
	private Date date;
	private int edition;
	
	public DSSDocument() {
		elemRegistry = new HashMap<>();
	}
	public DSSDocument(DSSElement root) {
		this();
		this.setRoot(root);
	}

	public DSSElement getRoot() {
		return root;
	}

	public void setRoot(DSSElement root) {
		this.root = root;
	}
	
	public Map<XMLName, DSSElementDecl> getElemRegistry() {
		return elemRegistry;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public int getEdition() {
		return edition;
	}
	
	public void setEdition(int edition) {
		this.edition = edition;
	}

}
