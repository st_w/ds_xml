package at.jku.dsxml.schema;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.data.IDataType;
import at.jku.dsxml.schema.compare.EnumCompareHelper;

/**
 * Represents a DSS attribute declaration
 * @author stw
 */
public class DSSAttributeDecl extends DSSNode implements IDataType {
	
	public enum Use {
		REQUIRED, OPTIONAL, PROHIBITED
	}
	

	private Use use = Use.OPTIONAL;

	
	public DSSAttributeDecl() {
		
	}
	public DSSAttributeDecl(XMLName name) {
		this.setName(name);
	}

	
	
	public Use getUse() {
		return use;
	}
	
	public void setUse(Use use) {
		this.use = use;
	}
	
	
	
	@Override
	public DSSData getTemplate() {
		return getTemplate(null);
	}
	@Override
	public DSSData getTemplate(DSSDataContainer d) {
		return getTemplate(d, null);
	}
	@Override
	public DSSData getTemplate(DSSDataContainer d, String name) {
		if (name == null) {
			if (d == null) name = this.getName().Name;
			else name = d.findName(this.getName().Name);
		} else {
			if (d != null) name = d.findName(name);
		}
		DSSAttribute v = new DSSAttribute(name, this.getType(), this);
		//if (d != null) d.addChild(v);
		return v;
	}
	
	
	
	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((use == null) ? 0 : EnumCompareHelper.contentHash(use));
		return result;
	}
	
	
	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSAttributeDecl other = (DSSAttributeDecl) obj;
		if (use != other.use)
			return false;
		assert(contentHash() == other.contentHash());
		return true;
	}
	
	
}
