package at.jku.dsxml.parsers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.wiztools.xsdgen.ParseException;
import org.wiztools.xsdgen.XsdGen;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSContentType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSTerm;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.XSWildcard;
import com.sun.xml.xsom.parser.XSOMParser;
import com.thaiopensource.relaxng.edit.SchemaCollection;
import com.thaiopensource.relaxng.input.InputFailedException;
import com.thaiopensource.relaxng.input.InputFormat;
import com.thaiopensource.relaxng.output.LocalOutputDirectory;
import com.thaiopensource.relaxng.output.OutputDirectory;
import com.thaiopensource.relaxng.output.OutputFailedException;
import com.thaiopensource.relaxng.output.OutputFormat;
import com.thaiopensource.relaxng.translate.Formats;
import com.thaiopensource.relaxng.translate.util.InvalidParamsException;
import com.thaiopensource.util.UriOrFile;
import com.thaiopensource.xml.out.CharRepertoire;
import com.thaiopensource.xml.util.EncodingMap;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataValue;
import at.jku.dsxml.helpers.SimpleSAXErrorHandler;
import at.jku.dsxml.schema.DSSAttributeDecl;
import at.jku.dsxml.schema.DSSAttributeDecl.Use;
import at.jku.dsxml.schema.DSSComplexType;
import at.jku.dsxml.schema.DSSComplexType.ContentType;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElement;
import at.jku.dsxml.schema.DSSElement.ElementTraverser;
import at.jku.dsxml.schema.DSSElementDecl;
import at.jku.dsxml.schema.DSSElementRef;
import at.jku.dsxml.schema.DSSGroup;
import at.jku.dsxml.schema.DSSGroup.Composition;
import at.jku.dsxml.schema.DSSParticle;
import at.jku.dsxml.schema.DSSSimpleType;
import at.jku.dsxml.schema.DSSType;
import at.jku.dsxml.schema.DSSWildcard;
import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.ParsingException;

/**
 * Parses XML documents to generate DSS documents
 * @author stw
 */
public class DSSParser {
	/**
	 * Generator to use for the automated schema generation from models
	 * "trang" is default because it works most reliably
	 * "xsdgen" is another supported schema generator
	 */
	private final String SCHEMA_GENERATOR = "trang";
	
	private EntityResolver entityResolver;
	private List<InputSource> schemas = new ArrayList<>();
	private XSSchemaSet schemaSet = null;
	
	
	/**
	 * Create a new XML document parser for DSS models
	 */
	public DSSParser() {
		
	}
	
	/**
	 * Parse a XML document and generate a DSS document
	 * A schema is created if not specified before.
	 * @param xml XML document
	 * @return DSS document
	 * @throws SAXException on XML parsing errors
	 */
	public DSSDocument parseDocument(InputStream xml) throws SAXException {
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);
        
        try {
        	
        	//FIXME: cannot use InputStream twice
        	/*
        	factory.setValidating(true);
        	List<Source> sources = new ArrayList<>();
	        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        for (InputSource is : schemas) {
	        	if (is.getByteStream() != null) {
	        		is.getByteStream().reset();
	        		sources.add(new StreamSource(is.getByteStream()));
	        	} else {
	        		System.out.println("bs is null!" + is);
	        	}
	        }
	        Schema schema = schemaFactory.newSchema(sources.toArray(new Source[0]));
	        factory.setSchema(schema);
	        */
	        
	        SAXParser parser = factory.newSAXParser();
	        XMLReader reader = parser.getXMLReader();
	        reader.setErrorHandler(new SimpleSAXErrorHandler());
	        reader.setEntityResolver(this.getEntityResolver());
	        reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
	
	        Builder builder = new Builder(reader, false);
	        Document doc = builder.build(xml);
	        
	        return parseDocument(doc);
        
			
        } catch (ParsingException | IOException | ParserConfigurationException e) {
        	throw new SAXException(e);
        }
        
	}
	
	/**
	 * Parse a XML document and generate a DSS document
	 * A schema is created if not specified before.
	 * @param doc XML document
	 * @return DSS document
	 * @throws SAXException on XML parsing errors
	 */
	public DSSDocument parseDocument(Document doc) throws SAXException {
		
        Element root = doc.getRootElement();
        
        
		if (schemaSet == null) {
			if (schemas.isEmpty()) {
				this.getXMLSchemaReferences(root);
			}
			if (schemas.isEmpty()) {
				this.generateSchemaInput(doc);
			}
			assert(schemas.size() > 0);
			this.parseSchemas();
		}
		assert(schemaSet != null);
		

		DSSDocument d = new DSSDocument();
		parseSchemaSet(schemaSet, d);
		
		parseXML(root, d);
		
		d.setDate(new Date());
		d.setName(root.getLocalName());
		
		return d;
        
	}
	
	
	/**
	 * Parse XML schema documents (XSD) and generate a DSS document (schema only)
	 * @return empty DSS document with schema only
	 * @throws SAXException on XSD parsing errors
	 */
	public DSSDocument getSchemaDocument() throws SAXException {
		if (schemaSet == null && schemas.size() > 0) {
			this.parseSchemas();
		}
		DSSDocument d = new DSSDocument();
		if (schemaSet != null) {
			parseSchemaSet(schemaSet, d);
		}
		return d;
	}
	
	
	/**
	 * Parse XML schema documents (XSD) and store schemaSet
	 * @throws SAXException on XSD parsing error
	 */
	private void parseSchemas() throws SAXException {
		
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(false);
		XSOMParser parser = new XSOMParser(factory);
		parser.setErrorHandler(new SimpleSAXErrorHandler());
		parser.setEntityResolver(this.getEntityResolver());
		
		for (InputSource is : schemas) {
			parser.parse(is);
		}
		
		schemaSet = parser.getResult();
		
	}
	
	
	/**
	 * Parse XML schema set and generate DSS schema
	 * @param schemaSet XML schema set
	 * @param d DSS document to store the DSS schema elements
	 */
	private void parseSchemaSet(XSSchemaSet schemaSet, DSSDocument d) {
		
		Iterator<XSElementDecl> it = schemaSet.iterateElementDecls();
		while (it.hasNext()) {
			XSElementDecl e = it.next();
			
			parseXSD(e, d);
			
		}
		
	}
	
	/**
	 * Generate/Derive schema from existing model
	 * @param doc XML document (model)
	 * @return XML schema document
	 * @throws SAXException on XML model parsing errors
	 */
	public ByteArrayOutputStream generateSchema(Document doc) throws SAXException {
		ByteArrayOutputStream tmpOut = new ByteArrayOutputStream();
		
		File f = null;
		try {
			f = File.createTempFile("tmpschema", ".xml");
			{
				PrintWriter w = new PrintWriter(f);
				w.print(doc.toXML());
				w.close();
			}
			
			if (SCHEMA_GENERATOR.equals("trang")) {
				
				InputFormat inputFormat = Formats.createInputFormat("xml");
				OutputFormat outputFormat = Formats.createOutputFormat("xsd");

				ErrorHandler eh = new SimpleSAXErrorHandler();
				SchemaCollection sc = inputFormat.load(f.toURI().toString(), new String[0], "xsd", eh, null);
				OutputDirectory od = new OutputStreamOD(tmpOut);
				outputFormat.output(sc, od, new String[0], "xml", eh);
				
				//File fo = new File("genschema.xsd");
				//LocalOutputDirectory lod = new LocalOutputDirectory(sc.getMainUri(), fo, "xsd", "UTF-8", 120, 2);
				//outputFormat.output(sc, lod, new String[0], "xml", eh);
				
				
			} else
			if (SCHEMA_GENERATOR.equals("xsdgen")) {
				XsdGen gen = new XsdGen();
				gen.parse(f);
				
				//FileOutputStream fo = new FileOutputStream("genschema.xsd");
				//gen.write(fo);
				
				gen.write(tmpOut);
				
				
			} else {
				throw new SAXException("Unknown schema generator");
			}
			
		} catch (IOException | ParseException | InputFailedException | InvalidParamsException | OutputFailedException e) {
			throw new SAXException(e);
			
		} finally {
			if (f != null) f.delete();
		}
		
		return tmpOut;
	}
	
	/**
	 * Generate Input stream from generated schema
	 * @param doc XML document (model)
	 * @throws SAXException on parsing errors
	 * @see generateSchema
	 */
	private void generateSchemaInput(Document doc) throws SAXException {
		ByteArrayOutputStream tmpOut = generateSchema(doc);
		
		//TODO: implement a less memory-consuming way to do this e.g. using pipes
		InputStream is = new ByteArrayInputStream(tmpOut.toByteArray());
		
		this.addSchema(is);
	}


	/**
	 * Parse XML document and generate DSS document
	 * @param xml XML document
	 * @param xsd XML schema definition (XSD)
	 * @return DSS document
	 * @throws SAXException on XML/XSD parsing errors
	 */
	public DSSDocument parseDocument(InputStream xml, InputStream xsd) throws SAXException {
		
		this.addSchema(xsd);
		this.parseSchemas();
		return this.parseDocument(xml);
		
	}
	
	
	/**
	 * Resolves a XSD entity
	 * @param publicId XSD public ID
	 * @param systemId XSD system ID
	 * @return XSD entity
	 * @throws SAXException when the entity cannot be resolved
	 */
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException {
    	try {
	        InputSource is=null;
	        
	        // ask the client-specified entity resolver first
	        if( this.getEntityResolver()!=null)  
	            is = this.getEntityResolver().resolveEntity(publicId,systemId);
	        if( is!=null )  return is;  // if that succeeds, fine.
	        
	        // rather than returning null, resolve it now
	        // so that we can detect errors.
	        is = new InputSource( new URL(systemId).openStream() );
	        is.setSystemId(systemId);
	        is.setPublicId(publicId);
	        return is;
    	} catch (IOException e) {
    		throw new SAXException(e);
    	}
    }
    
    
    /** 
     * Adds a XML schema used during DSS document generation
     * @param schema XML schema
     */
    public void addSchema(Reader schema) {
    	schemas.add(new InputSource(schema));
    }
    
    /** 
     * Adds a XML schema used during DSS document generation
     * @param schema XML schema
     */
    public void addSchema(InputStream schema) {
    	schemas.add(new InputSource(schema));
    }
    
    /** 
     * Adds a XML schema used during DSS document generation
     * @param schema XML schema
     */
    public void addSchema(InputSource schema) {
    	schemas.add(schema);
    }
    
    /** 
     * Adds a XML schema used during DSS document generation
     * The schema is resolved from the given public ID and system ID
     * @param publicId XSD public ID
     * @param systemId XSD system ID
     */
    public void addSchema(String publicId, String systemId) throws SAXException {
    	schemas.add(resolveEntity(publicId, systemId));
    }
    
	
	
	/**
	 * Parses a XML document and generates DSS element nodes
	 * @param root XML document root node
	 * @param d DSS document to store DSS element nodes
	 */
	private static void parseXML(Element root, DSSDocument d) {
		Map<XMLName, DSSElementDecl> elemRegistry = d.getElemRegistry();
		
		Stack<Iterator<Element>> stack = new Stack<>();
		stack.push(Arrays.asList(root).iterator());
		
		Stack<ElementTraverser> parentStack = new Stack<>();
		parentStack.push(null);
		
		while(!stack.isEmpty()) {
			Iterator<Element> it = stack.pop();
			ElementTraverser parent = parentStack.pop();
			
			while (it.hasNext()) {
				Element e = it.next();
				XMLName name = XMLName.fromElement(e);
				
				//System.out.println("parsing elem: " + name);
				
				DSSElementDecl type;
				if (elemRegistry.containsKey(name)) {
					type = elemRegistry.get(name);
				} else {
					
					//FIXME: allow this only for wildcards
					type = new DSSElementDecl(name);
					DSSType derivedType = deriveType(e);
					type.setType(derivedType);
					//TODO: check whether there are better ways than this
					elemRegistry.put(name, type);
					
					//throw new UnsupportedOperationException("encountered unknown element: " + name);
				}
				
				DSSElement inst = type.instantiate();
				if (stack.empty()) d.setRoot(inst);
				
				for (int q = 0; q < e.getAttributeCount(); q++) {
					Attribute a = e.getAttribute(q);
					XMLName a_name = XMLName.fromAttribute(a);
					if (a_name.Namespace.equals(XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI)) {
						//FIXME: skip xsi: attributes for now (e.g. xsi:noNamespaceSchemaLocation)
						continue;
					}
					DSSDataValue attD = inst.getAttribute(a_name);
					if (attD == null) {
						attD = inst.createAttribute(a_name);
						//FIXME: what about e.g. xsi: attributes
						if (((DSSComplexType)type.getType()).getAttributeWildcard() == null) {
							System.out.println("Warning: encountered unexpected Attribute");
							for (DSSDataValue tmpAtt : inst.getAttributes()) {
								System.out.println("Available Attribute: " + tmpAtt.getName());
							}
							System.out.println("Requested Attribute: " + a.getQualifiedName());
						}
						assert(attD != null);
					}
					attD.setText(a.getValue());
				}
				
				if (parent != null) {
					DSSData elemEntry = null;
					try {
						elemEntry = parent.getNextElement(inst.getName());
						if (elemEntry == null) throw new IllegalStateException();
					} catch (IllegalStateException e2) {
						elemEntry = parent.getRoot().getGenericElementsContainer();
					}
					assert(elemEntry != null);
					if (elemEntry instanceof DSSDataArray) {
						((DSSDataArray) elemEntry).addValue(inst);
					} else
					if (elemEntry instanceof DSSDataValue) {
						((DSSDataValue) elemEntry).setValue(inst);
					} else {
						throw new IllegalStateException();
					}
				}
				
				Elements elems = e.getChildElements();
				if (type.getType() instanceof DSSSimpleType ||
					((DSSComplexType)type.getType()).getContentType() == ContentType.SIMPLE) {
					
					if (e.getChildCount() == 1) {
						inst.setText(e.getChild(0).getValue());
					}
					
				} else {
				
					if (elems.size() > 0) {
						stack.push(it);
						stack.push(getElementsIterator(elems));
						parentStack.push(parent);
						parentStack.push(inst.createTraverser());
						break;
					}
					
				}
				
			}
			
		}
	}
	
	
	/**
	 * Derive XML type from XMl element content
	 * Only needed when type of element is unknown, which should not happen if proper schema is available
	 * @param e XML element
	 * @return Derived XML type
	 */
	private static DSSType deriveType(Element e) {
		DSSComplexType t = null;
		if (e.getChildElements().size() > 0) {
			t = new DSSComplexType();
			t.setContentType(ContentType.COMPLEX);
		} else
		if (e.getAttributeCount() > 0) {
			t = new DSSComplexType();
			t.setContentType(ContentType.SIMPLE);
		} else {
			return new DSSSimpleType();
		}
		/**
		t.setBase(DSSConstants.anyType());
		t.setMethod(Method.EXTENSION);
		if (e.getAttributeCount() > 0) {
			t.setAttributeWildcard(new DSSWildcard());
		}
		*/
		t.setAttributeWildcard(new DSSWildcard());
		DSSGroup g = new DSSGroup();
		g.setComposition(Composition.SEQUENCE);
		DSSWildcard w = new DSSWildcard();
		w.setMinOccurs(0);
		w.setMaxOccurs(-1);
		g.addChild(w);
		t.setParticle(g);
		return t;
	}

	
	/**
	 * Finds all referenced XML schema documents (XSD) in a XML document
	 * @param root XML document root node
	 * @throws SAXException on parsing errors
	 */
	private void getXMLSchemaReferences(Element root) throws SAXException {
		Attribute a = root.getAttribute("noNamespaceSchemaLocation", XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
		if (a != null) {
			this.addSchema(resolveEntity(null, a.getValue()));
		}
		a = root.getAttribute("schemaLocation", XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
		if (a != null) {
			String[] parts = a.getValue().split("\\s+");
			assert(parts.length % 2 == 0);
			for (int q = 0; q < parts.length; q+=2) {
				this.addSchema(resolveEntity(parts[q], parts[q+1]));
			}
		}
	}
	
	
	

	private static Iterator<Element> getElementsIterator(final Elements elem) {
		Iterator<Element> it = new Iterator<Element>() {
			int i = 0;
			@Override
			public boolean hasNext() {
				return elem.size() > i;
			}

			@Override
			public Element next() {
				return elem.get(i++);
			}
		};
		return it;
	}
	
	
	
	static void parseXSD(XSElementDecl root, DSSDocument d) {
		assert(root != null);
		
		Map<XMLName,DSSElementDecl> elemRegistry = d.getElemRegistry();
		Map<XSType,DSSType> typeMap = new HashMap<>();
		Map<DSSElementRef, XMLName> attFixup = new HashMap<>();
		Stack<Iterator<XSElementDecl>> stack = new Stack<Iterator<XSElementDecl>>();
		List<XSElementDecl> rootEl = Arrays.asList(root);
		stack.add(rootEl.iterator());
		
		while (!stack.isEmpty()) {
			Iterator<XSElementDecl> it = stack.pop();
			while (it.hasNext()) {
				XSElementDecl elem = it.next();
				XMLName elem_name = XMLName.fromDeclaration(elem);
				if (elemRegistry.containsKey(elem_name)) continue;
				
				//System.out.println("Parsing schema element: " + elem_name);
				
				DSSElementDecl d_elem = new DSSElementDecl(elem_name);
				elemRegistry.put(elem_name, d_elem);
				
				XSType t = elem.getType();
				if (typeMap.containsKey(t)) {
					d_elem.setType(typeMap.get(t));
					
				} else {
					
					if (elem.getType().isComplexType()) {
						XSComplexType ctype;
						ctype = elem.getType().asComplexType();
						DSSComplexType d_type = new DSSComplexType();
						typeMap.put(ctype, d_type);
						d_elem.setType(d_type);
						
						Collection<? extends XSAttributeUse> att = ctype.getAttributeUses();
						for(XSAttributeUse au : att) {
							DSSAttributeDecl d_attr = new DSSAttributeDecl(XMLName.fromDeclaration(au.getDecl()));
							d_attr.setUse(au.isRequired() ? Use.REQUIRED : Use.OPTIONAL);
							XSSimpleType attType = au.getDecl().getType();
							if (typeMap.containsKey(attType)) {
								d_attr.setType(typeMap.get(attType));
							} else {
								DSSSimpleType att_type = new DSSSimpleType();
								typeMap.put(attType, att_type);
								att_type.setName(XMLName.fromDeclaration(attType));
								d_attr.setType(att_type);
							}
							d_type.addAttribute(d_attr);
						}
						
						XSWildcard attWC = ctype.getAttributeWildcard();
						if (attWC != null) {
							DSSWildcard wc = new DSSWildcard();
							//FIXME: implement attribute wildcard parsing
							d_type.setAttributeWildcard(wc);
						}
						
						d_type.setAbstract(ctype.isAbstract());
						d_type.setName(XMLName.fromDeclaration(ctype));
						
						XSContentType contType = ctype.getContentType();
						if (contType.asSimpleType() != null) {
							d_type.setContentType(ContentType.SIMPLE);
							
						} else {
							d_type.setContentType(ContentType.COMPLEX);

							XSParticle part = contType.asParticle();
							if (part != null) {
		
								List<XSElementDecl> newElems = new ArrayList<XSElementDecl>();
								d_type.setParticle(parseXSD_particle(part, elemRegistry, attFixup, newElems));
		
								stack.push(it);
								stack.push(newElems.iterator());
								
								break;
								
							} else {
								// empty complex type
								
							}
							
						}
						
					} else
					if (elem.getType().isSimpleType()) {
						
						XSSimpleType stype;
						stype = elem.getType().asSimpleType();
						DSSSimpleType d_type = new DSSSimpleType();
						typeMap.put(stype, d_type);
						d_elem.setType(d_type);
						
						d_type.setName(XMLName.fromDeclaration(stype));
						
					} else {
						
						throw new IllegalArgumentException("Unsupported type");
						
					}
				
				}
			}
		}
		
		for (Entry<DSSElementRef, XMLName> kv : attFixup.entrySet()) {
			assert(elemRegistry.containsKey(kv.getValue()));
			kv.getKey().setRef(elemRegistry.get(kv.getValue()));
		}
		
		
	}
	
	
	static DSSGroup parseXSD_group(XSParticle part, Map<XMLName,DSSElementDecl> elemRegistry, Map<DSSElementRef, XMLName> attFixup, List<XSElementDecl> newElem) {
		DSSGroup g = new DSSGroup();
		XSTerm term = part.getTerm();
		XSModelGroup mgroup = term.asModelGroup();
		if (term.asModelGroupDecl() != null) mgroup = term.asModelGroupDecl().getModelGroup();
		assert(mgroup != null);
		g.setMinOccurs(part.getMinOccurs().intValue());
		g.setMaxOccurs(part.getMaxOccurs().intValue());
		switch (mgroup.getCompositor()) {
			case ALL:		g.setComposition(Composition.ALL); break;
			case CHOICE: 	g.setComposition(Composition.CHOICE); break;
			case SEQUENCE: 	g.setComposition(Composition.SEQUENCE); break;
		}
		for (XSParticle p : mgroup.getChildren()) {
			g.addChild(parseXSD_particle(p, elemRegistry, attFixup, newElem));
		}
		return g;
	}
	
	
	
	static DSSElementRef parseXSD_elemRef(XSParticle part, Map<XMLName,DSSElementDecl> elemRegistry, Map<DSSElementRef, XMLName> attFixup, List<XSElementDecl> newElem) {
		XSTerm term = part.getTerm();
		XSElementDecl e = term.asElementDecl();
		DSSElementRef newRef = new DSSElementRef();
		newRef.setMinOccurs(part.getMinOccurs().intValue());
		newRef.setMaxOccurs(part.getMaxOccurs().intValue());
		XMLName e_name = XMLName.fromDeclaration(e);
		if (elemRegistry.containsKey(e_name)) {
			newRef.setRef(elemRegistry.get(e_name));
		} else {
			attFixup.put(newRef, e_name);
		}
		newElem.add(e);
		return newRef;
	}
	
	
	static DSSWildcard parseXSD_wildcard(XSParticle part, Map<XMLName,DSSElementDecl> elemRegistry, Map<DSSElementRef, XMLName> attFixup, List<XSElementDecl> newElem) {
		DSSWildcard w = new DSSWildcard();
		XSTerm term = part.getTerm();
		XSWildcard wc = term.asWildcard();
		// FIXME: implement element wildcard parsing
		w.setMinOccurs(part.getMinOccurs().intValue());
		w.setMaxOccurs(part.getMaxOccurs().intValue());
		return w;
	}
	
	
	static DSSParticle parseXSD_particle(XSParticle p, Map<XMLName,DSSElementDecl> elemRegistry, Map<DSSElementRef, XMLName> attFixup, List<XSElementDecl> newElem) {
		XSTerm t = p.getTerm();
		if (t.isElementDecl()) {
			return parseXSD_elemRef(p, elemRegistry, attFixup, newElem);
		} else 
		if (t.isModelGroup() || t.isModelGroupDecl()) {
			return parseXSD_group(p, elemRegistry, attFixup, newElem);
		} else 
		if (t.isWildcard()) {
			return parseXSD_wildcard(p, elemRegistry, attFixup, newElem);
		} else {
			throw new java.lang.UnsupportedOperationException("the encountered particle type is not supported");
		}
	}


	public EntityResolver getEntityResolver() {
		return entityResolver;
	}


	/**
	 * Allows to specify a custom XSD entity resolver, 
	 * which returns a XML schema document from given public ID and system ID.
	 * @param entityResolver Entity resolver method
	 */
	public void setEntityResolver(EntityResolver entityResolver) {
		this.entityResolver = entityResolver;
	}
	
	
	
	/**
	 * Virtual OutputDirectory for trang, which writes any Output to a stream
	 * Only a single output file is supported
	 * @author stw
	 */
	class OutputStreamOD implements OutputDirectory {

		private OutputStream os;
		private OutputStreamWriter ow = null;
		private int indent = 2;
		private String defaultEncoding = "UTF-8";
		
		OutputStreamOD(OutputStream os) {
			this.os = os;
		}
		
		@Override
		public Stream open(String sourceUri, String encoding) throws IOException {
			if (ow != null) throw new IOException("only single output supported");
			if (encoding == null) encoding = defaultEncoding;
			String javaEncoding = EncodingMap.getJavaName(encoding);
			ow = new OutputStreamWriter(this.os, javaEncoding);
			Stream stream = new Stream(ow, javaEncoding, CharRepertoire.getInstance(javaEncoding));
			return stream;
		}

		@Override
		public String reference(String fromSourceUri, String toSourceUri) {
			return "";
		}

		@Override
		public String getLineSeparator() {
			return "\n";
		}

		@Override
		public int getLineLength() {
			return 1000;
		}

		@Override
		public int getIndent() {
			return this.indent;
		}

		@Override
		public void setIndent(int indent) {
			this.indent = indent;
			
		}

		@Override
		public void setEncoding(String encoding) {
			this.defaultEncoding = encoding;			
		}
		
	}
	
	
	
	
	
}
