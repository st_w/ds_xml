package at.jku.dsxml.parsers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.data.DSSData;
import at.jku.dsxml.data.DSSDataArray;
import at.jku.dsxml.data.DSSDataContainer;
import at.jku.dsxml.data.DSSDataValue;
import at.jku.dsxml.data.IDataType;
import at.jku.dsxml.helpers.ListDiff;
import at.jku.dsxml.helpers.ListDiff.TransformOperation;
import at.jku.dsxml.schema.DSSAttribute;
import at.jku.dsxml.schema.DSSAttributeDecl;
import at.jku.dsxml.schema.DSSAttributeDecl.Use;
import at.jku.dsxml.schema.DSSComplexType;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElement;
import at.jku.dsxml.schema.DSSElementDecl;
import at.jku.dsxml.schema.DSSElementRef;
import at.jku.dsxml.schema.DSSGroup;
import at.jku.dsxml.schema.DSSGroup.Composition;
import at.jku.dsxml.schema.DSSNode;
import at.jku.dsxml.schema.DSSParticle;
import at.jku.dsxml.schema.DSSSimpleType;
import at.jku.dsxml.schema.DSSText;
import at.jku.dsxml.schema.DSSType;
import at.jku.dsxml.schema.DSSWildcard;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.CollectionArtifact;
import at.jku.sea.cloud.Container.Filter;
import at.jku.sea.cloud.MetaModel;
import at.jku.sea.cloud.Package;
import at.jku.sea.cloud.Property;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.exceptions.MetaModelDoesNotExistException;


/**
 * DesignSpace interface of the DSS data model
 * Implements methods for both generating DSS models in DesignSpace and
 * parsing existing DSS models in DesignSpace.
 * @author stw
 */
public class DS {
	
	final static Logger log = Logger.getLogger(DS.class);

	private Workspace ws;
	private at.jku.sea.cloud.Package pModel;
	private at.jku.sea.cloud.Package pMetaModel;
	private at.jku.sea.cloud.Package pMetaMetaModel;
	
	MetaModel dssRoot;
	//Artifact dssSchemaType;
	Artifact dssDocumentType;
	Artifact dssElementType;
	Artifact dssElementDeclType;
	Artifact dssComplexTypeType;
	Artifact dssSimpleTypeType;
	Artifact dssGroupType;
	Artifact dssElementRefType;
	Artifact dssWildcardType;
	Artifact dssAttributeDeclType;
	
	DSSDocument dssDoc;
	Map<XMLName, Artifact> dsElements = new HashMap<>();
	Map<DSSType, Artifact> dsTypes = new HashMap<>();
	Map<Artifact, DSSType> dsTypesR = new HashMap<>();
	Map<IDataType, Artifact> dsSubTypes = new HashMap<>();
	
	private boolean overwrite = false;
	
	/**
	 * Creates a DesignSpace interface
	 * @param ws Workspace to use
	 */
	public DS(Workspace ws) {
		this(ws, null);
	}
	
	/**
	 * Creates a DesignSpace interface
	 * @param ws Workspace to use
	 * @param pModel Existing package to store the DSS model
	 */
	public DS(Workspace ws, at.jku.sea.cloud.Package pModel) {
		this.ws = ws;
		for (at.jku.sea.cloud.Package pkg : ws.getPackages()) {
			if ("DSS".equals(pkg.getPropertyValueOrNull("name"))) {
				pMetaMetaModel = pkg;
			}
		}

		if (pMetaMetaModel == null) {
			pMetaMetaModel = ws.createPackage();
			pMetaMetaModel.createProperty(ws, "name").setValue(ws, "DSS");
		}
		
		this.pModel = pModel;
		this.pMetaModel = null;
		
		
		dssRoot	= getOrCreateMetaMetaModelRoot();
		//dssSchemaType = getOrCreateMetaMetaModelArtifact("dssSchema", "schema");
		dssDocumentType = getOrCreateMetaMetaModelArtifact("dssDocument", "document");
		dssElementType = getOrCreateMetaMetaModelArtifact("dssElement", "element");
		dssElementDeclType = getOrCreateMetaMetaModelArtifact("dssElementDecl", "elementDecl");
		dssComplexTypeType = getOrCreateMetaMetaModelArtifact("dssComplexType", "complexType");
		dssSimpleTypeType = getOrCreateMetaMetaModelArtifact("dssSimpleType", "simpleType");
		dssGroupType = getOrCreateMetaMetaModelArtifact("dssGroup", "group");
		dssElementRefType = getOrCreateMetaMetaModelArtifact("dssElementRef", "elementRef");
		dssWildcardType = getOrCreateMetaMetaModelArtifact("dssWildcard", "wildcard");
		dssAttributeDeclType = getOrCreateMetaMetaModelArtifact("dssAttributeDecl", "attributeDecl");
		
	}
	
	/**
	 * Returns whether existing documents are overwritten
	 * @return true if existing documents are overwritten/updated
	 */
	public boolean isOverwriteEnabled() {
		return overwrite;
	}
	
	/**
	 * Defines whether a new document should be created or existing documents should be overwritten/updated
	 * @param overwrite true to overwrite existing (if any)
	 */
	public void setOverwriteEnabled(boolean overwrite) {
		this.overwrite = overwrite;
	}
	
	
	/**
	 * Finds or Creates the root element in the MetaMetaModel package
	 * @return root node of the MetaMetaModel
	 */
	private MetaModel getOrCreateMetaMetaModelRoot() {
		if (pMetaMetaModel.hasProperty("root")) {
			return ws.getMetaModel((long)pMetaMetaModel.getPropertyValue("root"));
		}
		MetaModel mm = ws.createMetaModel(pMetaMetaModel);
		pMetaMetaModel.createProperty(ws, "root").setValue(ws, mm.getId());
		return mm;
	}
	
	/**
	 * @param name Element name
	 * @param propertyName Property name in the MetaMetaModel root node
	 * @return MetaMetaModel node
	 */
	private Artifact getOrCreateMetaMetaModelArtifact(String name, String propertyName) {
		if (dssRoot.hasProperty(propertyName)) {
			return ws.getArtifact((long)dssRoot.getPropertyValue(propertyName));
		}
		
		Collection<Artifact> ca = pMetaMetaModel.getArtifactsWithProperty("name", name, true, new Filter());
		if (!ca.isEmpty()) {
			assert(ca.size() == 1);
			Artifact a = ca.iterator().next();
			dssRoot.createProperty(ws, propertyName).setValue(ws, a.getId());
			return a;
		}
		
		Artifact a = ws.createArtifact(pMetaMetaModel);
		a.createProperty(ws,"name").setValue(ws, name);
		dssRoot.createProperty(ws, propertyName).setValue(ws, a.getId());
		return a;
	}
	

	/**
	 * Stores a DSS model in DesignSpace
	 * @param d DSS document to store
	 * @return root (document) node of the stored DSS model
	 */
	public Artifact storeDocument(DSSDocument d) {
		
		Artifact doc = null;
		MetaModel schema = null;
		
		if (overwrite) {
			
			int maxEdition = 0;
			for (Artifact a : getDocuments()) {
				if (d.getName().equals(a.getPropertyValueOrNull("name"))) {
					if ((int)a.getPropertyValue("edition") >= maxEdition) {
						maxEdition = (int)a.getPropertyValue("edition");
						doc = a;
					}
				}
			}
			
			if (doc != null) {
				
				log.info("Overwriting existing document");
				
				pModel = doc.getPackage();
				
				doc.setPropertyValue(ws, "date", d.getDate().getTime());
				
				schema = ws.getMetaModel((long)doc.getPropertyValue("schema"));
				schema = findSchemaArtifact(d.getElemRegistry().values(), schema);
				
				
			}
			
		}
		if (doc == null) {
			
			if (pModel == null) {
				pModel = ws.createPackage();
				pModel.createProperty(ws, "name").setValue(ws, "DSSXML");
			}
			
			doc = ws.createArtifact(dssDocumentType, pModel);
			if (pModel.hasProperty("root")) {
				pModel.getProperty("root").setValue(ws, doc.getId());
			} else {
				pModel.createProperty(ws, "root").setValue(ws, doc.getId());
			}		
			
			if (d.getDate() != null) {
				doc.createProperty(ws, "date").setValue(ws, d.getDate().getTime());
			}
			int maxEdition = 0;
			if (d.getName() != null) {
				doc.createProperty(ws, "name").setValue(ws, d.getName());
				for (Artifact a : getDocuments()) {
					if (a.getId() == doc.getId()) continue;
					if (d.getName().equals(a.getPropertyValueOrNull("name"))) {
						if (a.hasProperty("edition")) {
							maxEdition = Math.max(maxEdition, (int)a.getPropertyValue("edition"));
						} else {
							assert(false);
							maxEdition = Math.max(maxEdition, 1);
						}
					}
				}
			}
			maxEdition++;
			d.setEdition(maxEdition);
			doc.createProperty(ws, "edition").setValue(ws, d.getEdition());
		}

		if (schema == null) {
			boolean foundAllElemDecls = true;
			
			// 1. iteration: find existing (matching)
			for (Map.Entry<XMLName, DSSElementDecl> e : d.getElemRegistry().entrySet()) {
				Artifact a = findElementDecl(e.getValue());
				if (a != null) {
					dsElements.put(e.getKey(), a);
					log.info("Using existing elementDeclaration for " + e.getKey());
				} else {
					foundAllElemDecls = false;
				}
			}
			
			// use existing schema, if possible
			if (foundAllElemDecls) {
				schema = findSchemaArtifact(dsElements.values());
			}
		} else {
			
			// use existing schema's elemDecls
			log.info("Parsing (existing) schema artifacts");
			CollectionArtifact ca = ws.getCollectionArtifact((long)schema.getPropertyValue("elements"));
			for (Object o : ca.getElements()) {
				Artifact a = (Artifact)o;
				// we need to parse it to fill type/subtype maps with data
				// which are needed for node generation
				DSSNode n = parseNode(a);
				dsElements.put(n.getName(), a);
			}
			
		}
		if (schema == null) {
			log.info("Generating new dssSchema");
			pMetaModel = ws.createPackage();
			pMetaModel.createProperty(ws, "name").setValue(ws, "DSSXSD");
			schema = ws.createMetaModel(pMetaModel);
			pMetaModel.createProperty(ws, "root").setValue(ws, schema.getId());
			
			
			// 2. iteration: generate missing (using existing ones)
			int cnt = 0;
			for (Map.Entry<XMLName, DSSElementDecl> e : d.getElemRegistry().entrySet()) {
				cnt++;
				if (!dsElements.containsKey(e.getKey())) {
					log.info("Generating elementDeclaration " + cnt + " of " + d.getElemRegistry().size() + " (" + e.getKey() + ")");
					generateNode(e.getValue());
				}
			}
			
			if (!schema.hasProperty("elements")) {
				CollectionArtifact elemCA = ws.createCollection(true, pMetaModel);
				for (Artifact a : dsElements.values()) {
					elemCA.addElement(ws, a);
				}
				schema.createProperty(ws, "elements").setValue(ws, elemCA.getId());
			}
			
		} else {
			log.info("Using existing dssSchema");
			pMetaModel = schema.getPackage();
		}
		
		log.info("Generating elementNodes");
		
		if (doc.hasProperty("root")) {
			Artifact root = generateNode(d.getRoot(), ws.getArtifact((long)doc.getPropertyValue("root")));
			doc.setPropertyValue(ws, "root", root.getId());
			doc.setPropertyValue(ws, "schema", schema.getId());
		} else {
			Artifact root = generateNode(d.getRoot());
			doc.createProperty(ws, "root").setValue(ws, root.getId());
			doc.createProperty(ws, "schema").setValue(ws, schema.getId());
		}
		
		return doc;
	}
	
	
	/**
	 * Parses an existing DSS model from DesignSpace
	 * @param doc DSS document/root node
	 * @return parsed DSS document
	 */
	public DSSDocument parseDocument(Artifact doc) {
		
		DSSDocument d = new DSSDocument();
		dssDoc = d;
		
		d.setName((String)doc.getPropertyValueOrNull("name"));
		if (doc.hasProperty("date")) {
			d.setDate(new Date((long)doc.getPropertyValue("date")));
		}
		d.setEdition((int)doc.getPropertyValue("edition"));
		
		Artifact schema = ws.getArtifact((long)doc.getPropertyValue("schema"));
		
		CollectionArtifact elemCA = ws.getCollectionArtifact((long)schema.getPropertyValue("elements"));
		for (Object eObj : elemCA.getElements()) {
			Artifact elem = (Artifact) eObj;
			
			DSSElementDecl ed = (DSSElementDecl) parseNode(elem);
			d.getElemRegistry().put(ed.getName(), ed);
			
		}
		
		Artifact rootA = ws.getArtifact((long)doc.getPropertyValue("root"));
		DSSElement root = (DSSElement) parseNode(rootA);
		d.setRoot(root);
		
		return d;
		
	}
	
	
	/**
	 * Retrieves a list of all DSS models in DesignSpace
	 * @return list of DSS documents in DS
	 */
	public List<Artifact> getDocuments() {
		
		List<Artifact> docs = new ArrayList<>();
		for (at.jku.sea.cloud.Package p : ws.getPackages()) {
			if (p.hasProperty("root")) {
				Artifact a = ws.getArtifact((long)p.getProperty("root").getValue());
				if (a.getType().getId() == dssDocumentType.getId()) {
					docs.add(a);
				}
			}
		}
		return docs;
	}
	
	
	/**
	 * Finds a DSS element in DesignSpace
	 * @param e DSS element to look for
	 * @return DSS element artifact or null
	 */
	private Artifact findElement(DSSElement e) {
		if (dsElements.containsKey(e.getName())) {
			return dsElements.get(e.getName());
		}
		int refHash = e.contentHash();
		for (Artifact a : ws.getArtifactsWithProperty("name", e.getName().toString())) {
			if (a.getType().getId() != dssElementDeclType.getId()) continue;
			if (a.hasProperty("@@hash")) {
				Property hashProp = a.getProperty("@@hash");
				if (!hashProp.isAlive()) continue;
				int hash = (int)hashProp.getValue();
				//System.out.println("Comparing " + e.getName().toString() + ": " + hash + " <> " + refHash);
				if (hash != refHash) {
					continue;
				}
			}
			DSSElementDecl dsElemDecl = (DSSElementDecl)parseNode(a);
			if (dsElemDecl.contentEquals(e)) {
				return a;
			}
		}
		return null;
	}
	
	
	/**
	 * Finds a DSS element declaration in DesignSpace
	 * @param e DSS element declaration to look for
	 * @return DSS element declaration artifact or null
	 */
	private Artifact findElementDecl(DSSElementDecl e) {
		if (dsElements.containsKey(e.getName())) {
			return dsElements.get(e.getName());
		}
		int refHash = e.contentHash();
		for (Artifact a : ws.getArtifactsWithProperty("name", e.getName().toString())) {
			if (a.getType().getId() != dssElementDeclType.getId()) continue;
			if (a.hasProperty("@@hash")) {
				Property hashProp = a.getProperty("@@hash");
				if (!hashProp.isAlive()) continue;
				int hash = (int)hashProp.getValue();
				//System.out.println("Comparing " + e.getName().toString() + ": " + hash + " <> " + refHash);
				if (hash != refHash) {
					continue;
				}
			}
			DSSElementDecl dsElemDecl = (DSSElementDecl)parseNode(a);
			if (dsElemDecl.contentEquals(e)) {
				return a;
			}
		}
		return null;
	}
	
	
	/**
	 * Finds and Validates a DSS schema in DesignSpace
	 * @param elems list of element declarations the schema must define
	 * @param preferredSchema a DSS schema where to start searching; may be null
	 * @return DSS schema artifact or null
	 */
	private MetaModel findSchemaArtifact(Collection<DSSElementDecl> elems, Artifact preferredSchema) {
		
		Iterator<Package> it = ws.getPackages().iterator();
		Package p = null;
		if (preferredSchema != null) p = preferredSchema.getPackage();
		if (p == null) p = it.next();
		
		schemaSearch:
		for (; p != null; p = it.hasNext() ? it.next() : null) {
			if (!p.hasProperty("root")) continue;
			if (!"DSSXSD".equals(p.getPropertyValueOrNull("name"))) continue;
			Artifact a = null;
			a = ws.getArtifact((long)p.getProperty("root").getValue());
			if (!(a instanceof MetaModel)) continue;

			if (!a.hasProperty("elements")) continue;	
			CollectionArtifact ca = ws.getCollectionArtifact((long)a.getPropertyValue("elements"));
			if (ca.size() != elems.size()) continue;
			
			Set<String> expectedElems = new HashSet<String>();
			for (DSSElementDecl e : elems) {
				expectedElems.add(e.getName().toString() + "|" + e.contentHash());
			}
			
			//TODO: should we accept schemas containing additional elemDecls? (currently not accepted)
			for (Object o : ca.getElements()) {
				Artifact ea = (Artifact)o;
				if (!ea.hasProperty("name")) continue schemaSearch;
				if (!ea.hasProperty("@@hash")) continue schemaSearch;
				String actualElem = ea.getPropertyValue("name") + "|" + ea.getPropertyValue("@@hash");
				expectedElems.remove(actualElem);
				if (expectedElems.isEmpty()) return (MetaModel)a;
			}
		}
		return null;
	}
	
	
	/**
	 * Finds a DSS schema in DesignSpace
	 * @param elems list of DSS element declaration artifacts
	 * @return DSS schema artifact or null
	 */
	private MetaModel findSchemaArtifact(Collection<Artifact> elems) {
		schemaSearch:
		for (at.jku.sea.cloud.Package p : ws.getPackages()) {
			if (!p.hasProperty("root")) continue;
			if (!"DSSXSD".equals(p.getPropertyValueOrNull("name"))) continue;
			MetaModel a = null;
			try {
				a = ws.getMetaModel((long)p.getProperty("root").getValue());
			} catch (MetaModelDoesNotExistException e) {
				continue;
			}
			//if (a.getType().getId() != dssSchemaType.getId()) continue;
			if (!a.hasProperty("elements")) continue;	
			CollectionArtifact ca = ws.getCollectionArtifact((long)a.getPropertyValue("elements"));
			if (ca.size() != elems.size()) continue;
			for (Object o : ca.getElements()) {
				if (!elems.contains(o)) continue schemaSearch;
			}
			return a;
		}
		return null;
	}
	
	
	
	/*
	Artifact findType(DSSType type) {
		if (type.getName() == null) {
			// cannot search for anonymous types
			return null;
		}
		for (Artifact a : ws.getArtifactsWithProperty("name", type.getName())) {
			DSSType referenceType = parseType(a);
		}
	}
	*/
	
	
	
	private DSSType parseType(Artifact a) {
		if (dsTypesR.containsKey(a)) {
			return dsTypesR.get(a);
		}
		
		DSSType t;
		if (a.getType().getId() == dssComplexTypeType.getId()) {
			t = parseComplexType(a);
		} else
		if (a.getType().getId() == dssSimpleTypeType.getId()) {
			t = parseSimpleType(a);
		} else {
			throw new IllegalArgumentException();
		}
		
		if (a.hasProperty("name")) {
			t.setName(XMLName.fromString((String)a.getPropertyValue("name")));
		}
		
		//t.setBase(null);
		if (a.hasProperty("base")) {
			Artifact baseArt = ws.getArtifact((long)a.getPropertyValue("base"));
			assert(baseArt != null);
			DSSType base = parseType(baseArt);
			t.setBase(base);
		}
		
		return t;
	}
	
	private Artifact generateType(DSSType t) {
		if (dsTypes.containsKey(t)) {
			return dsTypes.get(t);
		}
		
		Artifact a;
		if (t instanceof DSSSimpleType) {
			a = generateSimpleType((DSSSimpleType)t);
		} else
		if (t instanceof DSSComplexType) {
			a = generateComplexType((DSSComplexType)t);
		} else {
			throw new IllegalArgumentException();
		}
		
		if (t.getName() != null) {
			a.createProperty(ws, "name").setValue(ws, t.getName().toString());
		}
		
		if (t.getBase() != null) {
			Artifact base = generateType(t.getBase());
			a.createProperty(ws, "base").setValue(ws, base.getId());			
		}
		
		return a;
	}
	
	




	private DSSSimpleType parseSimpleType(Artifact a) {
		DSSSimpleType t = new DSSSimpleType();
		dsTypes.put(t, a);
		dsTypesR.put(a, t);

		//TODO: parse SimpleType content
		
		return t;
	}
	
	private Artifact generateSimpleType(DSSSimpleType t) {
		Artifact a = ws.createArtifact(dssSimpleTypeType, pMetaModel);
		dsTypes.put(t, a);
		dsTypesR.put(a, t);
		
		return a;
	}
	
	
	private DSSComplexType parseComplexType(Artifact a) {
		DSSComplexType t = new DSSComplexType();
		dsTypes.put(t, a);
		dsTypesR.put(a, t);
		
		if (a.hasProperty("method")) {
			t.setMethod(DSSComplexType.Method.valueOf((String)a.getPropertyValue("method")));
		}
		
		t.setContentType(DSSComplexType.ContentType.valueOf((String)a.getPropertyValue("content")));
		
		if (a.hasProperty("mixed")) {
			t.setMixed((boolean)a.getPropertyValue("mixed"));
		} else {
			t.setMixed(false);
		}
		
		if (a.hasProperty("abstract")) {
			t.setAbstract((boolean)a.getPropertyValue("abstract"));
		} else {
			t.setAbstract(false);
		}
		
		if (a.hasProperty("attWildcard")) {
			Artifact awc = ws.getArtifact((long)a.getPropertyValue("attWildcard"));
			assert(awc != null);
			DSSParticle part = parseParticle(awc);
			assert(part instanceof DSSWildcard);
			t.setAttributeWildcard((DSSWildcard) part);
		}
		
		if (a.hasProperty("attributes")) {
			CollectionArtifact attributes = ws.getCollectionArtifact((long)a.getPropertyValue("attributes"));
			assert(attributes != null);
			for (Object attaID : attributes.getElements()) {
				Artifact atta = ws.getArtifact((long)attaID);
				DSSAttributeDecl att = (DSSAttributeDecl) parseNode(atta);
				t.addAttribute(att);
			}
		}
		
		if (a.hasProperty("particle")) {
			Artifact pArt = ws.getArtifact((long)a.getPropertyValue("particle"));
			assert(pArt != null);
			DSSParticle part = parseParticle(pArt);
			t.setParticle(part);
		}
		
		return t;
	}
	
	private Artifact generateComplexType(DSSComplexType t) {
		Artifact a = ws.createArtifact(dssComplexTypeType, pMetaModel);
		dsTypes.put(t, a);
		dsTypesR.put(a, t);

		a.createProperty(ws, "mixed").setValue(ws, t.isMixed());
		a.createProperty(ws, "abstract").setValue(ws, t.isAbstract());
		if (t.getBase() != null) {
			assert(t.getMethod() != null);
			a.createProperty(ws, "method").setValue(ws, t.getMethod().toString());
		}
		a.createProperty(ws, "content").setValue(ws, t.getContentType().toString());
		if (t.getName() != null) {
			a.createProperty(ws, "name").setValue(ws, t.getName().toString());
		}
		
		if (t.getAttributeWildcard() != null) {
			Artifact awc = generateParticle(t.getAttributeWildcard());
			a.createProperty(ws, "attWildcard").setValue(ws, awc.getId());
		}
		
		//attributes
		if (t.getAttributes().size() > 0) {
			CollectionArtifact attCA = ws.createCollection(false, pMetaModel);
			a.createProperty(ws, "attributes").setValue(ws, attCA.getId());
			for (DSSAttributeDecl att : t.getAttributes()) {
				Artifact attA = generateNode(att);
				attCA.addElement(ws, attA.getId());
			}
		}
		
		//complexType with non-SimpleContent
		if (t.getParticle() != null) {
			Artifact particle = generateParticle(t.getParticle());
			a.createProperty(ws, "particle").setValue(ws, particle.getId());
		}
		
		return a;
	}
	
	



	private DSSParticle parseParticle(Artifact a) {
		DSSParticle p;
		if (a.getType().getId() == dssElementRefType.getId()) {
			p = parseElementRef(a);
		} else
		if (a.getType().getId() == dssGroupType.getId()) {
			p = parseGroup(a);
		} else 
		if (a.getType().getId() == dssWildcardType.getId()) {
			p = parseWildcard(a);
		} else {
			throw new IllegalArgumentException();
		}
		
		if (a.hasProperty("minOccurs")) {
			p.setMinOccurs((int)a.getPropertyValue("minOccurs"));
		} else {
			p.setMinOccurs(1);
		}
		
		if (a.hasProperty("maxOccurs")) {
			p.setMaxOccurs((int)a.getPropertyValue("maxOccurs"));
		} else {
			p.setMaxOccurs(1);
		}
		
		return p;
	}
	
	
	private Artifact generateParticle(DSSParticle p) {
		Artifact a;
		if (dsSubTypes.containsKey(p)) {
			return dsSubTypes.get(p);
		}
		
		if (p instanceof DSSGroup) {
			a = generateGroup((DSSGroup)p);
		} else
		if (p instanceof DSSElementRef) {
			a = generateElementRef((DSSElementRef)p);
		} else
		if (p instanceof DSSWildcard) {
			a = generateWildcard((DSSWildcard)p);
		} else {
			throw new IllegalArgumentException();
		}
		
		a.createProperty(ws, "minOccurs").setValue(ws, p.getMinOccurs());
		a.createProperty(ws, "maxOccurs").setValue(ws, p.getMaxOccurs());
		
		return a;
	}



	private DSSGroup parseGroup(Artifact a) {
		DSSGroup g = new DSSGroup();
		dsSubTypes.put(g, a);
		
		if (a.hasProperty("composition")) {
			g.setComposition(Composition.valueOf((String)a.getPropertyValue("composition")));
		} else {
			g.setComposition(Composition.SEQUENCE);
		}
		
		if (a.hasProperty("children")) {
			CollectionArtifact children = ws.getCollectionArtifact((long)a.getPropertyValue("children"));
			assert(children != null);
			for (Object childID : children.getElements()) {
				Artifact parta = ws.getArtifact((long)childID);
				DSSParticle part = parseParticle(parta);
				g.addChild(part);
			}
		}
		
		return g;
	}

	private Artifact generateGroup(DSSGroup g) {
		Artifact a = ws.createArtifact(dssGroupType, pMetaModel);
		dsSubTypes.put(g, a);
		
		a.createProperty(ws, "composition").setValue(ws, g.getComposition().toString());
		
		CollectionArtifact children = ws.createCollection(false, pMetaModel);
		//children.setType(ws, dssParticleType);
		a.createProperty(ws, "children").setValue(ws, children.getId());
		for (DSSParticle p : g.getChildren()) {
			Artifact pa = generateParticle(p);
			children.addElement(ws, pa.getId());
		}
		
		return a;
	}
	

	private DSSElementRef parseElementRef(Artifact a) {
		DSSElementRef r = new DSSElementRef();
		dsSubTypes.put(r, a);
		
		Artifact refArt = ws.getArtifact((long)a.getPropertyValue("ref"));
		assert(refArt != null);
		DSSElementDecl e = (DSSElementDecl)parseNode(refArt);
		r.setRef(e);
		
		return r;		
	}


	private Artifact generateElementRef(DSSElementRef r) {
		Artifact a = ws.createArtifact(dssElementRefType, pMetaModel);
		dsSubTypes.put(r, a);
		
		Artifact ref = generateNode(r.getRef());
		a.createProperty(ws, "ref").setValue(ws, ref.getId());
		
		return a;
	}

	
	private DSSWildcard parseWildcard(Artifact a) {
		DSSWildcard w = new DSSWildcard();
		dsSubTypes.put(w, a);
		
		//FIXME: implement wildcard parsing
		
		return w;	
	}


	private Artifact generateWildcard(DSSWildcard w) {
		Artifact a = ws.createArtifact(dssWildcardType, pMetaModel);
		dsSubTypes.put(w, a);
		
		//FIXME: implement wildcard generation
		
		return a;
	}



	private DSSNode parseNode(Artifact a) {
		DSSNode n;
		if (a.getType().getId() == dssAttributeDeclType.getId()) {
			n = parseAttributeDecl(a);
		} else
		if (a.getType().getId() == dssElementDeclType.getId() ) {
			n = parseElementDecl(a);
		} else
		if (a.getType().getId() == dssElementType.getId()) {
			n = null;
			//TODO: add Attribute, (Text?)
		} else {
			throw new IllegalArgumentException();
		}
		
		if (n == null) {
			
			XMLName name = XMLName.fromString((String)a.getPropertyValue("name"));
			n = dssDoc.getElemRegistry().get(name);
			assert(n != null);
			
			n = parseElement(a, (DSSElementDecl) n);
			
		} else {
			
			n.setName(XMLName.fromString((String)a.getPropertyValue("name")));
			
			if (a.hasProperty("type")) {
				Artifact typeArt = ws.getArtifact((long)a.getPropertyValue("type"));
				if (typeArt != null) {
					DSSType type = parseType(typeArt);
					n.setType(type);
				}
			} else {
				//TODO: should a <anyType> be introduced? what would be its type?
			}
			
		}
		
		return n;
	}
	

	private Artifact generateNode(DSSNode n) {
		return generateNode(n, null);
	}
	
	private Artifact generateNode(DSSNode n, Artifact existing) {
		
		if (existing != null) {
			if ((int)existing.getPropertyValue("@@hash") == n.contentHash()) {
				//DSSNode nc = parseNode(existing);
				//if (nc.contentEquals(n)) {
					log.info("using existing element " + n.getName());
					return existing;
				//}
			}
		}
		
		Artifact tArt = null;
		if (n.getType() != null) {
			tArt = generateType(n.getType());
		}
		
		Artifact a;
		if (n instanceof DSSElement) {
			a = generateElement((DSSElement)n, existing);
		} else
		if (n instanceof DSSAttributeDecl) {
			a = generateAttributeDecl((DSSAttributeDecl)n);
		} else
		if (n instanceof DSSElementDecl) {
			a = findElementDecl((DSSElementDecl)n);
			if (a == null) a = generateElementDecl((DSSElementDecl)n);
		} else {
			throw new IllegalArgumentException();
		}
		
		a.createProperty(ws, "name").setValue(ws, n.getName().toString());
		if (tArt != null) {
			//TODO: should a <anyType> be introduced? what would be its type?
			a.createProperty(ws, "type").setValue(ws, tArt.getId());
		}
		
		a.createProperty(ws, "@@hash").setValue(ws, n.contentHash());
		//System.out.println("Saving node " + e.getName() + " with hash " + e.contentHash());

				
		return a;
	}
	

	private DSSAttributeDecl parseAttributeDecl(Artifact a) {
		DSSAttributeDecl d = new DSSAttributeDecl();
		dsSubTypes.put(d, a);
		
		if (a.hasProperty("use")) {
			d.setUse(Use.valueOf((String)a.getPropertyValue("use")));
		} else {
			d.setUse(Use.OPTIONAL);
		}
		
		return d;
	}
	

	private Artifact generateAttributeDecl(DSSAttributeDecl d) {
		Artifact a = ws.createArtifact(dssAttributeDeclType, pMetaModel);
		if (dsSubTypes.containsKey(d)) {
			return dsSubTypes.get(d);
		}
		dsSubTypes.put(d, a);
		
		a.createProperty(ws, "use").setValue(ws, d.getUse().toString());
		
		return a;
	}
	

	private DSSElementDecl parseElementDecl(Artifact refArt) {
		DSSElementDecl d = new DSSElementDecl();
		
		return d;
	}
	
	
	private DSSElement parseElement(Artifact a, DSSElementDecl ed) {
		DSSElement e = ed.instantiate();
		
		if (e.getType() instanceof DSSSimpleType) {
			// text content
			e.setText((String)a.getPropertyValueOrNull("text"));
			
		} else {
			DSSDataContainer dc = (DSSDataContainer) e.getData();
			// complex content
			
			for (Property dp : a.getAliveProperties()) {
				String pName = dp.getName();
				if (pName.equals("name") || pName.equals("type") || pName.startsWith("@")) {
					continue;
				}
				
				DSSData child = dc.getChild(pName);
				if (child instanceof DSSDataValue) {
					// single value
					
					Object dpVal = dp.getValue();
					if (dpVal instanceof String) {
						((DSSDataValue) child).setText((String) dpVal);
					} else
					if (dpVal != null) {
						long refId = (long)dpVal;
						DSSElement ref = (DSSElement) parseNode(ws.getArtifact(refId));
						((DSSDataValue) child).setValue(ref);
					} else {
						((DSSDataValue) child).setValue(null);
					}
					
				} else
				if (child instanceof DSSDataArray) {
					// list of values
					
					CollectionArtifact dca = (CollectionArtifact) ws.getArtifact((long)dp.getValue());
					for (Object dcao : dca.getElements()) {
						long refId = (long)dcao;
						DSSElement ref = (DSSElement)parseNode(ws.getArtifact(refId));
						((DSSDataArray) child).addValue(ref);
						
					}
					
				} else
				if (child == null) {
					// value not in template -> derive from type (should not occur normally)
					
					Property dtype = null;
					if (a.hasProperty("@" + pName)) {
						dtype = a.getProperty("@" + pName);
						Artifact dtypeA = ws.getArtifact((long)dtype.getValue());
						
						if (dtypeA.getType().getId() == dssAttributeDeclType.getId()) {
							DSSAttributeDecl attd = (DSSAttributeDecl) parseNode(dtypeA);
							DSSAttribute att = dc.createAttribute(attd.getName());
							att.setText((String)dp.getValue());
						} else {
							//FIXME : implement DSSData Creation without type reference
							throw new IllegalArgumentException();
						}
						
					} else {
						throw new IllegalArgumentException();
					}

					
				} else {
					throw new IllegalArgumentException();
				}
				
			}
			
		}
		
		
		return e;
	}

	
	
	
	private Artifact generateElementDecl(DSSElementDecl e) {
		assert(!(e instanceof DSSElement));
		Artifact a = generateElementInternal(e, null, null);
		return a;
	}
	private Artifact generateElement(DSSElement e, Artifact existing) {
		return generateElementInternal(e, e.getData(), existing);
	}
	private Artifact generateElementInternal(DSSElementDecl e, DSSData d, Artifact existing) {
		boolean instance = d != null;
		boolean update = existing != null;
		
		Artifact a;
		if (existing == null || !e.getName().toString().equals((String)existing.getPropertyValueOrNull("name"))) {
			a = ws.createArtifact(instance ? dssElementType : dssElementDeclType, instance ? pModel : pMetaModel);
			update = false;
			log.info("generating new element" + (instance?" ":"Decl ") + e.getName());
		} else {
			a = existing;
			log.info("updating existing element " + e.getName());
		}

		if (!instance) {
			dsElements.put(e.getName(), a);
		}
		
		if (d==null) d = e.getType().getTemplate();
		
		Set<String> expectedProperties = new HashSet<>();
		expectedProperties.add("name");
		expectedProperties.add("type");
		expectedProperties.add("@@hash");
		
		if (d instanceof DSSDataValue) {
			// text content
			
			if (!update || !a.hasProperty("text")) {
				Property dp = a.createProperty(ws, "text");
				if (instance) dp.setValue(ws, ((DSSDataValue) d).getText());
			} else {
				if (instance) a.setPropertyValue(ws, "text", ((DSSDataValue) d).getText());
			}
			expectedProperties.add("text");
			
		} else
		if (d instanceof DSSDataContainer) {
			// complex content
			
			for (DSSData dd : ((DSSDataContainer) d).getChildren()) {
				expectedProperties.add(dd.getName());
				Property dp;
				if (!update || !a.hasProperty(dd.getName())) {
					dp = a.createProperty(ws, dd.getName());
				} else {
					dp = a.getProperty(dd.getName());
				}
				if (dd instanceof DSSDataArray) {
					// list of values
					
					CollectionArtifact ca;
					if (!update || dp.getValue() == null) {
						ca = ws.createCollection(false, instance ? pModel : pMetaModel);
						dp.setValue(ws, ca.getId());
					} else {
						ca = ws.getCollectionArtifact((long)dp.getValue());
					}
					if (instance) {
						compareLists(ca, (DSSDataArray) dd);
					}
					
				} else
				if (dd instanceof DSSDataValue) {
					// single value
					
					if (instance) {
						DSSDataValue dv = (DSSDataValue) dd;
						if (dv.getValue() == null) {
							dp.setValue(ws, null);
						} else
						if (dv.getValue() instanceof DSSText) {
							dp.setValue(ws, dv.getText());
						} else
						if (dv.getValue() instanceof DSSElement) {
							Artifact eArt = null;
							if (update && dp.getValue() != null) {
								eArt = ws.getArtifact((long)dp.getValue());
								DSSElement eNode = (DSSElement) dv.getValue();
								if (!eNode.getName().toString().equals(eArt.getPropertyValueOrNull("name"))) {
									eArt = null;
								}
								eArt = generateNode(eNode, eArt);
							}
							if (eArt == null) {
								eArt = generateNode(dv.getValue());							
							}
							dp.setValue(ws, eArt.getId());
						} else {
							throw new IllegalArgumentException();
						}
					}
				}
				
				// store type of data node
				if (dd.getSubType() != null) {
					Artifact typeArt = dsSubTypes.get(dd.getSubType());
					if (typeArt == null) {
						log.info("could not find @type - creating: " + dd.getName());
						if (dd.getSubType() instanceof DSSParticle) {
							typeArt = generateParticle((DSSParticle) dd.getSubType());
						} else
						if (dd.getSubType() instanceof DSSAttributeDecl) {
							typeArt = generateNode((DSSNode) dd.getSubType());
						} else {
							assert(false);
						}
					}
					assert (typeArt != null);
					
					if (!update || !a.hasProperty("@" + dd.getName())) {
						a.createProperty(ws, "@" + dd.getName()).setValue(ws, typeArt.getId());
					} else {
						a.getProperty("@" + dd.getName()).setValue(ws, typeArt.getId());
					}
					expectedProperties.add("@" + dd.getName());
				}
			}
		}
		
		if (update) {
			// remove old/deleted properties
			for (Property dp : a.getAliveProperties()) {
				if (!expectedProperties.contains(dp.getName())) {
					dp.delete(ws);
				}
			}
		}
		
		return a;
	}
	

	/**
	 * Compares and Updates a list of DSS data nodes in DesignSpace
	 * @param ca source list in DesignSpace to update
	 * @param dd target list of data nodes
	 */
	private void compareLists(CollectionArtifact ca, DSSDataArray dd) {
		
		List<Integer> actual = new ArrayList<>();
		List<Integer> target = new ArrayList<>();
		
		for (Object o : ca.getElements()) {
			Artifact a = ws.getArtifact((long)o);
			Object hash = a.getPropertyValueOrNull("@@hash");
			assert(hash != null);
			actual.add((int)hash);
		}
		for (DSSNode n : dd.getValues()) {
			assert (n instanceof DSSElement);
			DSSElement e = (DSSElement) n;
			target.add(e.contentHash());
		}
		
		List<TransformOperation<Integer>> ops = ListDiff.compareLists(actual, target);
		
		for (TransformOperation<Integer> op : ops) {
			
			switch (op.op) {
			case Delete:
				ca.removeElementAt(ws, op.index);
				break;
			case Insert:
				Artifact a = generateNode(dd.getValues().get(op.valueIndex));
				ca.insertElementAt(ws, a.getId(), op.index);
				break;
			case Update:
				Artifact olda = ws.getArtifact((long)ca.getElementAt(op.index));
				Artifact newa = generateNode(dd.getValues().get(op.valueIndex), olda);
				if (olda.getId() != newa.getId()) {
					//FIXME: is there a way to update an existing element? (instead of delete+add)
					ca.removeElementAt(ws, op.index);
					ca.insertElementAt(ws, newa.getId(), op.index);
				}
				break;
			
			}
			
		}
		
	}
	
	
	
}
