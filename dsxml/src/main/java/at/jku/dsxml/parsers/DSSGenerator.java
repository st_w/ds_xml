package at.jku.dsxml.parsers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.schema.DSSAttribute;
import at.jku.dsxml.schema.DSSAttributeDecl;
import at.jku.dsxml.schema.DSSComplexType;
import at.jku.dsxml.schema.DSSComplexType.ContentType;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.dsxml.schema.DSSElement;
import at.jku.dsxml.schema.DSSSimpleType;
import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Serializer;

/**
 * Generates XML documents from DSS documents
 * @author stw
 */
public class DSSGenerator {
	
	Map<String, String> namespacePrefix = new HashMap<>();
	
	/**
	 * Creates a new XML generator for DSS models
	 */
	public DSSGenerator() {
		
		
	}
	
	
	/**
	 * Generates a XML document from a DSS document
	 * @param root DSS document (root)
	 * @return XML document
	 */
	public Document generateXML(DSSElement root) {
		
		Element rootNode = generateNode(root);
		for (Entry<String, String> e : namespacePrefix.entrySet()) {
			rootNode.addNamespaceDeclaration(e.getValue(), e.getKey());
		}
		
		Document d = new Document(rootNode);
		
		return d;
	}
	
	
	/**
	 * Generate XML element from DSS element
	 * @param element DSS element
	 * @return XML element
	 */
	private Element generateNode(DSSElement element) {
		
		XMLName name = element.getName();
		Element node = new Element(name.Name);
		if (!name.Namespace.isEmpty()) {
			getNSPrefixName(name);
			node.setNamespaceURI(name.Namespace);
			node.setNamespacePrefix(namespacePrefix.get(name.Namespace));
		}
		
		if (element.getType() instanceof DSSSimpleType) {
			if (element.getText() != null) {
				node.appendChild(element.getText());
			}
			
		} else
		if (element.getType() instanceof DSSComplexType) {
			
			for (DSSAttribute a : element.getAttributes()) {
				if (a.getText() != null) {
					Attribute att = generateAttribute(a);
					node.addAttribute(att);
				}
			}
			
			DSSComplexType t = (DSSComplexType) element.getType();
			
			if (t.getContentType() == ContentType.SIMPLE) {
				if (element.getText() != null) {
					node.appendChild(element.getText());
				}
				
			} else
			if (t.getContentType() == ContentType.COMPLEX) {
				for (DSSElement e : element.getElements()) {
					Element subNode = generateNode(e);
					node.appendChild(subNode);
				}
				
			}
		}
		return node;
	}
	
	
	/**
	 * Generate XML attribute from DSS attribute
	 * @param attribute DSS attribute
	 * @return XML attribute
	 */
	private Attribute generateAttribute(DSSAttribute attribute) {
		
		DSSAttributeDecl ad = (DSSAttributeDecl) attribute.getSubType();
		XMLName name = ad.getName();
		Attribute a = new Attribute(name.Name, attribute.getText());
		if (!name.Namespace.isEmpty()) {
			getNSPrefixName(name);
			a.setNamespace(namespacePrefix.get(name.Namespace), name.Namespace);
		}
		return a;
		
	}
	
	
	
	
	
	
	/**
	 * Generate a namespace prefix.
	 * In XML namespaces are not used directly, but via a prefix which refers to a certain namespace.
	 * @param name XML name with namespace
	 * @return prefix for the namespace of the XML name
	 */
	private String getNSPrefixName(XMLName name) {
		String prefix = namespacePrefix.get(name.Namespace);
		if (prefix == null) {
			// TODO: better name generation; allow more than 26 namespaces
			for (char p = 'a'; p <= 'z'; p++) {
				prefix = "" + p;
				if (!namespacePrefix.containsValue(prefix)) {
					namespacePrefix.put(name.Namespace, prefix);
					break;
				}
			}
			prefix = "";
		}
		return namespacePrefix.get(name.Namespace) + ":" + name.Name;
	}
	
	/**
	 * Converts a DSS document to a XML document and writes it into the given output stream
	 * @param doc DSS document
	 * @param o output stream to write to
	 */
	public void writeXML(DSSDocument doc, OutputStream o) {
		writeXML(doc.getRoot(), o);
	}
	
	/**
	 * Converts a DSS document to a XML document and writes it into the given output stream
	 * @param root DSS document (root node)
	 * @param o output stream to write to
	 */
	public void writeXML(DSSElement root, OutputStream o) {
		writeXML(generateXML(root), o);
	}
	
	/**
	 * Serializes and Writes a XML document to the given output stream
	 * @param d XML document
	 * @param o output stream to write to
	 */
	public void writeXML(Document d, OutputStream o) {
		Serializer s = new Serializer(o);
		s.setIndent(4);
		try {
			s.write(d);
			s.flush();
		} catch (IOException e) {
			//FIXME: error handling
			throw new RuntimeException(e);
		}
	}
}
