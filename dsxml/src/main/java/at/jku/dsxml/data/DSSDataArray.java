package at.jku.dsxml.data;

import java.util.ArrayList;
import java.util.List;

import at.jku.dsxml.schema.DSSNode;
import at.jku.dsxml.schema.DSSType;
import at.jku.dsxml.schema.compare.ListCompareHelper;

/**
 * Ordered (instantiable, comparable) list of element nodes
 * e.g. contains DSSElements, DSSText
 * @author stw
 */
public class DSSDataArray extends DSSData {

	private List<DSSNode> values;
	
	/**
	 * Creates a list of element nodes
	 * @param name Unique name for accessing the list
	 * Used in DS representation only, not used in XML.
	 * @param type Type of the contained element nodes; may be null
	 * @param subtype Defining element node
	 */
	public DSSDataArray(String name, DSSType type, IDataType subtype) {
		super(name, type, subtype);
		values = new ArrayList<>();
	}
	

	public List<DSSNode> getValues() {
		return new ArrayList<>(values);
	}


	public void addValue(DSSNode value) {
		this.values.add(value);
	}
	
	/**
	 * Instantiates the list for use in an element instance
	 */
	public DSSDataArray instantiate() {
		DSSDataArray i = new DSSDataArray(this.getName(), this.getType(), this.getSubType());
		return i;
	}


	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((values == null) ? 0 : ListCompareHelper.contentHash(values));
		return result;
	}


	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSDataArray other = (DSSDataArray) obj;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!ListCompareHelper.contentEqual(values, other.values))
			return false;
		return true;
	}


}
