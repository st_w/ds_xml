package at.jku.dsxml.data;

import at.jku.dsxml.schema.compare.IComparable;

/**
 * Interface for (element) node definitions to create data node containers
 * @author stw
 */
public interface IDataType extends IComparable {

	public DSSData getTemplate();
	public DSSData getTemplate(DSSDataContainer d);
	public DSSData getTemplate(DSSDataContainer d, String name);
	
}
