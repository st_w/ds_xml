package at.jku.dsxml.data;

import at.jku.dsxml.schema.DSSElement;
import at.jku.dsxml.schema.DSSNode;
import at.jku.dsxml.schema.DSSText;
import at.jku.dsxml.schema.DSSType;

/**
 * Data node for a single (instantiable, comparable) element/text node
 * @author stw
 */
public class DSSDataValue extends DSSData {

	
	private DSSNode value;

	/**
	 * Create a data node for a single element/text node
	 * @param name Name of the data node
	 * @param type Type of the contained element/text node
	 * @param subtype Defining element node
	 */
	public DSSDataValue(String name, DSSType type, IDataType subtype) {
		super(name, type, subtype);
	}



	public DSSNode getValue() {
		return value;
	}


	public void setValue(DSSNode value) {
		this.value = value;
	}
	
	
	/**
	 * Sets the text of this text node
	 * @param text Text
	 * @throws UnsupportedOperationException if the data node does not contain a text (element) node
	 */
	public void setText(String text) {
		if (text == null) {
			this.value = null;
			return;
		}
		if (this.value == null) {
			this.setValue(new DSSText(text));
		}
		if (this.value instanceof DSSText) {
			((DSSText) this.value).setText(text);
		} else
		if (this.value instanceof DSSElement) {
			((DSSElement) this.value).setText(text);
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
	/**
	 * Gets the text of this text node
	 * @return Text
	 * @throws UnsupportedOperationException if the data node does not contain a text (element) node
	 */
	public String getText() {
		if (this.value == null) {
			return null;
		} else
		if (this.value instanceof DSSText) {
			return ((DSSText) this.value).getText();
		} else
		if (this.value instanceof DSSElement) {
			return ((DSSElement) this.value).getText();
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
	
	@Override
	public DSSDataValue instantiate() {
		DSSDataValue i = new DSSDataValue(this.getName(), this.getType(), this.getSubType());
		return i;
	}



	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((value == null) ? 0 : value.contentHash());
		return result;
	}


	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSDataValue other = (DSSDataValue) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.contentEquals(other.value))
			return false;
		return true;
	}


	
	
	
}
