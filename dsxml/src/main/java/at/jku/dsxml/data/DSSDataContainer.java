package at.jku.dsxml.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.jku.dsxml.XMLName;
import at.jku.dsxml.schema.DSSAttribute;
import at.jku.dsxml.schema.DSSAttributeDecl;
import at.jku.dsxml.schema.DSSElementRef;
import at.jku.dsxml.schema.DSSType;
import at.jku.dsxml.schema.compare.ListCompareHelper;

/**
 * Ordered (instantiable, comparable) list of data nodes.
 * Used for grouping and nesting data nodes.
 * @author stw
 */
public class DSSDataContainer extends DSSData {

	private List<DSSData> children;
	
	/**
	 * Creates a container for data nodes
	 * @param type Represented (complex) data type
	 * @param subtype Defining element node
	 */
	public DSSDataContainer(DSSType type, IDataType subtype) {
		super("", type, subtype);
		this.children = new ArrayList<>();
	}


	public List<DSSData> getChildren() {
		return new ArrayList<DSSData>(children);
	}


	public void addChild(DSSData child) {
		Map<String, DSSData> childMap = buildChildMap();
		if (childMap.containsKey(child.getName())) {
			children.remove(childMap.get(child.getName()));
		}
		this.children.add(child);
	}
	
	public DSSData getChild(String name) {
		for (DSSData d : children) {
			if (d.getName().equals(name)) return d;
		}
		return null;
	}
	
	/**
	 * Finds attributes in this container
	 * @param name Attribute name to look for
	 * @return Returns the first attribute with the given name or null
	 */
	public DSSAttribute getAttribute(XMLName name) {
		for (DSSData d : children) {
			if (d.getSubType() instanceof DSSAttributeDecl) {
				DSSAttributeDecl a = (DSSAttributeDecl) d.getSubType();
				if (a.getName().equals(name)) return (DSSAttribute)d;
			}
		}
		return null;
	}
	
	/**
	 * Creates a attribute and adds it to the container
	 * @param name name of the new attribute
	 * @return the created attribute
	 * @throws UnsupportedOperationException if the attribute already exists
	 */
	public DSSAttribute createAttribute(XMLName name) {
		DSSAttribute a = getAttribute(name);
		if (a != null) {
			throw new UnsupportedOperationException("Attribute already exists");
		}
		a = new DSSAttribute(this.findName(name.Name), null, new DSSAttributeDecl(name));
		this.addChild(a);
		return a;
	}	
	
	/**
	 * Finds an element Node
	 * @param name Element name to look for
	 * @return Returns the first element node with the given name or null
	 */
	public DSSData getElement(String name) {
		for (DSSData d : children) {
			if (d.getSubType() instanceof DSSElementRef) {
				DSSElementRef e = (DSSElementRef) d.getSubType(); 
				if (e.getRef().getName().equals(name)) return d;
			}
		}
		return null;
	}
	
	
	/**
	 * Compiles a list of all attributes in this container
	 * @return Returns a list of all attributes in this container
	 */
	public List<DSSAttribute> getAttributes() {
		List<DSSAttribute> att = new ArrayList<>();
		for (DSSData d : children) {
			if (d.getSubType() instanceof DSSAttributeDecl) {
				att.add((DSSAttribute)d);
			}
		}
		return att;
	}
	
	/**
	 * Compiles a list of all non-attribute nodes in this container
	 * @return Returns a list of all element, group and wildcard nodes in this container
	 */
	public List<DSSData> getElements() {
		List<DSSData> el = new ArrayList<>();
		for (DSSData d : children) {
			if (!(d.getSubType() instanceof DSSAttributeDecl)) {
				el.add(d);
			}
		}
		return el;
	}
	
	
	/**
	 * Gets the text of a single contained dataValue node
	 * @return text of the contained dataValue node
	 * @throws UnsupportedOperationException other nodes than a single DSSDataValue node are in the container
	 */
	public String getText() {
		List<DSSData> elems = getElements();
		if (elems.size() == 1)
		{
			if (elems.get(0) instanceof DSSDataValue) {
				DSSDataValue textNodeData = (DSSDataValue) elems.get(0);
				return textNodeData.getText();
			}
		}
		throw new UnsupportedOperationException();
	}
	
	
	/**
	 * Sets the text of a single contained dataValue node
	 * @param txt text for the contained dataValue node
	 * @throws UnsupportedOperationException other nodes than a single DSSDataValue node are in the container
	 */
	public void setText(String txt) {
		List<DSSData> elems = getElements();
		if (elems.size() == 1)
		{
			if (elems.get(0) instanceof DSSDataValue) {
				DSSDataValue textNodeData = (DSSDataValue) elems.get(0);
				textNodeData.setText(txt);
				return;
			}
		}
		throw new UnsupportedOperationException();
	}
	
	
	private Map<String, DSSData> buildChildMap() {
		Map<String, DSSData> c = new HashMap<>();
		for (DSSData d : children) {
			c.put(d.getName(), d);
		}
		return c;
	}
	

	/**
	 * Finds a unique name in this container
	 * @return unique name
	 */
	public String findName() {
		return findName(null);
	}
	
	/**
	 * Finds a unique name in this container
	 * @param name preferred name
	 * @return unique name
	 */
	public String findName(String name) {
		if (name == null || name.isEmpty()) {
			name = "data";
		}
		String tmpName = name;
		if (name.equals("name") || name.equals("type")) {
			// reserved names
			tmpName = name + "1";
		}
		Map<String, DSSData> childMap = buildChildMap();
		int i = 2;
		while (childMap.containsKey(tmpName)) {
			tmpName = name + i;
			i++;
		}
		return tmpName;		
	}
	
	
	/**
	 * Gets/Creates a child list for any element nodes.
	 * May be used if no defining type is available or unexpected elements are encountered.
	 * @return DSSDataArray for any element nodes
	 */
	public DSSDataArray getGenericElementsContainer() {
		DSSDataArray ec = null;
		List<DSSData> elem = this.getElements();
		if (elem.size() > 0) {
			DSSData d = elem.get(elem.size()-1);
			if (d instanceof DSSDataArray && d.getType() == null && d.getSubType() == null) {
				ec = (DSSDataArray) d;
			}
		}
		
		if (ec == null) {
			String name = this.findName("rest");
			ec = new DSSDataArray(name, null, null);
			this.addChild(ec);
		}
		return ec;
	}
	


	@Override
	public DSSData instantiate() {
		DSSDataContainer i = new DSSDataContainer(this.getType(), this.getSubType());
		for (DSSData child : children) {
			i.addChild(child.instantiate());
		}
		return i;
	}


	@Override
	public int contentHash() {
		final int prime = 31;
		int result = super.contentHash();
		result = prime * result + ((children == null) ? 0 : ListCompareHelper.contentHash(children));
		return result;
	}


	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (!super.contentEquals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSDataContainer other = (DSSDataContainer) obj;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!ListCompareHelper.contentEqual(children, other.children))
			return false;
		return true;
	}
	
	
	
}
