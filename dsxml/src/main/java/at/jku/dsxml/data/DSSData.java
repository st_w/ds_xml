package at.jku.dsxml.data;

import at.jku.dsxml.schema.DSSType;
import at.jku.dsxml.schema.compare.IComparable;

/**
 * Abstract base class for an instantiable, comparable data template
 * @author stw
 */
public abstract class DSSData implements IComparable {

	private String name;
	private DSSType type;
	private IDataType subtype;
	
	public DSSData(String name, DSSType type, IDataType subtype) {
		this.setName(name);
		this.setType(type);
		this.setSubType(subtype);
	}
	
	abstract public DSSData instantiate();


	public IDataType getSubType() {
		return subtype;
	}


	public void setSubType(IDataType subtype) {
		this.subtype = subtype;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public DSSType getType() {
		return type;
	}


	public void setType(DSSType type) {
		this.type = type;
	}

	
	@Override
	public int contentHash() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((subtype == null) ? 0 : subtype.contentHash());
		result = prime * result + ((type == null) ? 0 : type.contentHash());
		return result;
	}

	@Override
	public boolean contentEquals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DSSData other = (DSSData) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (subtype == null) {
			if (other.subtype != null)
				return false;
		} else if (!subtype.contentEquals(other.subtype))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.contentEquals(other.type))
			return false;
		return true;
	}
	
	
	
}
