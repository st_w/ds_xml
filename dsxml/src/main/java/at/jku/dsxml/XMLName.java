package at.jku.dsxml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.xml.xsom.XSDeclaration;
import nu.xom.Attribute;
import nu.xom.Element;

/**
 * Represents a XMLName, which consists of a name and its namespace
 * The default string representation is: "{namespace}name"
 * @author stw
 */
public class XMLName {

	public final String Namespace;
	public final String Name;
	
	/**
	 * Creates a XMLName with empty namespace
	 * @param name XML name
	 */
	public XMLName(String name) {
		this("", name);
	}	
	
	/**
	 * Creates a XMLName in a namespace
	 * @param namespace XML namespace
	 * @param name XML name
	 */
	public XMLName(String namespace, String name) {
		if (name == null || name.isEmpty()) throw new IllegalArgumentException("name must not be null or empty");
		if (namespace == null) throw new IllegalArgumentException("namespace must not be null");
		this.Namespace = namespace;
		this.Name = name;
	}
	
	@Override
	public String toString() {
		return "{" + Namespace + "}" + Name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		return this.toString().equals(obj.toString());
	}
	
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	
	public static XMLName fromElement(Element e) {
		return new XMLName(e.getNamespaceURI(), e.getLocalName());
	}
	
	public static XMLName fromAttribute(Attribute a) {
		return new XMLName(a.getNamespaceURI(), a.getLocalName());
	}
	
	public static XMLName fromDeclaration(XSDeclaration d) {
		if (d.getName() == null) return null;
		return new XMLName(d.getTargetNamespace(), d.getName());
	}
	
	public static XMLName fromString(String s) {
		if (s == null) return null;
		Pattern pattern = Pattern.compile("^(\\{([^\\}]*)\\})?([^\\{\\}]+)$");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			String ns = matcher.group(2);
			if (ns == null) ns = "";
			String name = matcher.group(3);
			return new XMLName(ns, name);
		}
		throw new IllegalArgumentException("could not parse String as XMLName");
	}
	
}
