package app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import at.jku.dsxml.parsers.DS;
import at.jku.dsxml.parsers.DSSGenerator;
import at.jku.dsxml.parsers.DSSParser;
import at.jku.dsxml.schema.DSSDocument;
import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Workspace;
import at.jku.sea.cloud.rest.client.RestCloud;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Implements commands and callbacks for the DSS model manager
 * @author stw
 */
public class MainController {
	
	@FXML private TextField txtXML;
	@FXML private TextField txtXSD;
	@FXML private TextField txtXMLOut;
	@FXML private CheckBox chkXSDAutoResolve;
	@FXML private CheckBox chkOverwrite;
	@FXML private Button btnUpload;
	@FXML private Button btnDownload;
	
	@FXML private ComboBox<Workspace> cbWorkspace;
	@FXML private ListView<Artifact> lstDS;
	@FXML private Pane rootNode;
	@FXML private Pane busyNode;
	@FXML private Pane busyListNode;
	

	private RestCloud cloud;


	public RestCloud getCloud() {
		if (cloud == null) {
			cloud = RestCloud.getInstance();
		}
		return cloud;
	}
	
	
	
	@FXML
	private void handleBrowseXMLAction(ActionEvent event) {
		
		Stage stage = (Stage) rootNode.getScene().getWindow();
		FileChooser chooser = new FileChooser();
		File file = getXMLFile();
		if (file != null) {
			chooser.setInitialFileName(file.getName());
			chooser.setInitialDirectory(file.getParentFile());
		}
		File f = chooser.showOpenDialog(stage);
		if (f != null) {
			txtXML.setText(f.getAbsolutePath());
		}
	}
	
	@FXML
	private void handleBrowseXSDAction(ActionEvent event) {
		
		Stage stage = (Stage) rootNode.getScene().getWindow();
		FileChooser chooser = new FileChooser();
		File file = getXSDFile();
		if (file != null) {
			chooser.setInitialFileName(file.getName());
			chooser.setInitialDirectory(file.getParentFile());
		}
		File f = chooser.showOpenDialog(stage);
		if (f != null) {
			txtXSD.setText(f.getAbsolutePath());
		}
	}
	
	
	@FXML
	private void handleBrowseXMLOutAction(ActionEvent event) {
		
		Stage stage = (Stage) rootNode.getScene().getWindow();
		FileChooser chooser = new FileChooser();
		File file = getXMLOutFile();
		if (file != null) {
			chooser.setInitialFileName(file.getName());
			chooser.setInitialDirectory(file.getParentFile());
		}
		File f = chooser.showSaveDialog(stage);
		if (f != null) {
			txtXMLOut.setText(f.getAbsolutePath());
		}
	}
	

	@FXML
	private void handleUploadAction() {
		
		final Workspace ws = cbWorkspace.getSelectionModel().getSelectedItem();
		final boolean autoResolve = chkXSDAutoResolve.isSelected();
		final boolean overwrite = chkOverwrite.isSelected();
		final File xmlFile = getXMLFile();
		final File xsdFile = getXSDFile();
		
		String error = null;
		if (xmlFile == null || !xmlFile.exists() || !xmlFile.isFile()) {
			error = "no document specified";
		}
		if (ws == null) {
			error = "no workspace selected";
		}
		if (error != null) {
			Alert a = new Alert(AlertType.ERROR);
			a.setTitle("Error");
			a.setHeaderText("Error while uploading document");
			a.setContentText(error);
			a.show();
			return;
		}
		
		busyNode.setVisible(true);
		
		Thread t = new Thread(new Runnable() {
		    public void run() {
				
		    	Artifact tmpArtifact = null;
		    	Exception tmpException = null;
		    	try {
					// 1. XML parsing
					DSSParser parser = new DSSParser();
					FileInputStream xsdIn = null;
					FileInputStream xmlIn = null;
					if (xsdFile != null && xsdFile.exists() && xsdFile.isFile()) {
						InputSource input = new InputSource();
						input.setSystemId(xsdFile.getAbsolutePath());
						xsdIn = new FileInputStream(xsdFile);
						input.setByteStream(xsdIn);
						parser.addSchema(input);
						parser.setEntityResolver(new AskingEntityResolver(autoResolve));
					}
					xmlIn = new FileInputStream(xmlFile);
					DSSDocument d = parser.parseDocument(xmlIn);
					if (xsdIn != null) xsdIn.close();
					if (xmlIn != null) xmlIn.close();
					
					// 2. DS generation
			        DS ds = new DS(ws);
			        ds.setOverwriteEnabled(overwrite);
			        tmpArtifact = ds.storeDocument(d);
			        ws.commitAll("DSXML Upload: " + xmlFile.getName());
		    	} catch (Exception e) {
		    		tmpArtifact = null;
		    		tmpException = e;
		    	}
		    	final Artifact docArtifact = tmpArtifact;
		    	final Exception exception = tmpException;

		        // refresh UI
		        Platform.runLater(new Runnable() {
		            public void run() {
		            	
		            	busyNode.setVisible(false);
		            	
		            	if (docArtifact != null) {
		            		if (!lstDS.getItems().contains(docArtifact)) {
			            		lstDS.getItems().add(docArtifact);
		            		}
		            		lstDS.getSelectionModel().select(docArtifact);
		            	} else {
		        			Alert a = new Alert(AlertType.ERROR);
		        			a.setTitle("Error");
		        			a.setHeaderText("Error while uploading document");
		        			if (exception != null) {
		        				a.setContentText(exception.toString());
		        			}
		        			a.show();
		            	}
		                
		            }
		        });
				
			}
			
		});
		t.start();

	}
	
	
	@FXML
	private void handleDownloadAction() {

		final Workspace ws = cbWorkspace.getSelectionModel().getSelectedItem();
		final Artifact docArtifact = lstDS.getSelectionModel().getSelectedItem();
		final File xmlOutFile = getXMLOutFile();
		
		String error = null;
		if (xmlOutFile == null) {
			error = "no output file specified";
		}
		if (docArtifact == null) {
			error = "no document selected";
		}
		if (ws == null) {
			error = "no workspace selected";
		}
		if (error != null) {
			Alert a = new Alert(AlertType.ERROR);
			a.setTitle("Error");
			a.setHeaderText("Error while downloading document");
			a.setContentText(error);
			a.show();
			return;
		}
		
		busyNode.setVisible(true);
		
		Thread t = new Thread(new Runnable() {
		    public void run() {
				
		    	boolean tmpSuccess = false;
		    	try {
		    		
			        // 3. DS parsing
			        DS ds = new DS(ws);
			        DSSDocument d_out = ds.parseDocument(docArtifact);
			        
			        // 4. XML generation
					FileOutputStream xmlOut = new FileOutputStream(xmlOutFile);
			        DSSGenerator gen = new DSSGenerator();
			        gen.writeXML(d_out, xmlOut);
			        xmlOut.close();
			        
			        tmpSuccess = true;
		    	} catch (Exception e) {
		    		tmpSuccess = false;
		    	}
		    	final boolean success = tmpSuccess;				

		        // refresh UI
		        Platform.runLater(new Runnable() {
		            public void run() {
		            	
		            	busyNode.setVisible(false);
		            	
		            	if (success) {
		            	} else {
		        			Alert a = new Alert(AlertType.ERROR);
		        			a.setTitle("Error");
		        			a.setHeaderText("Error while downloading document");
		        			a.show();
		            	}
		            }
		        });
				
			}
			
		});
		t.start();

	}
	
	
	private void refreshWorkspaces() throws Exception {
		
        // refresh workspaces
		List<Workspace> tmpWSS = null;
		Exception ex = null;
		try {
			tmpWSS = getCloud().getWorkspaces();
		} catch (Exception e) {
			ex = e;
		}
        final List<Workspace> wss = tmpWSS;
				        
        // refresh UI
        Platform.runLater(new Runnable() {
            public void run() {
            	
                // refresh workspaces
                ObservableList<Workspace> WSList = cbWorkspace.getItems();
                List<Workspace> WSToDelete = new ArrayList<Workspace>(WSList);
                if (wss != null) {
                    for (Workspace ws : wss) {
                    	if (WSToDelete.remove(ws)) continue;
                    	WSList.add(ws);        	
                    }
                }
                WSList.removeAll(WSToDelete);
                
            }
        });
        
        if (ex != null) {
        	throw ex;
        }
        
	}
	
	
	private void refreshDocuments() throws Exception {
		
		busyListNode.setVisible(true);
		btnDownload.setDisable(true);
		btnUpload.setDisable(true);
		
        Workspace ws = cbWorkspace.getSelectionModel().getSelectedItem();

        List<Artifact> tmpDSS = null;
        Exception ex = null;
        try {
	        // refresh documents (if workspace selected)
	        if (ws != null) {
		        DS ds = new DS(ws);
		        tmpDSS = ds.getDocuments();
	        }
        } catch (Exception e) {
        	ex = e;
		}
        final List<Artifact> dss = tmpDSS;
        
        // refresh UI
        Platform.runLater(new Runnable() {
            public void run() {
            	
                // refresh documents
                //Workspace ws = cbWorkspace.getSelectionModel().getSelectedItem();
                ObservableList<Artifact> DSList = lstDS.getItems();
                List<Artifact> DSToDelete = new ArrayList<Artifact>(DSList);
                if (dss != null) {
	                for (Artifact a : dss) {
	                	if (DSToDelete.remove(a)) continue;
	                	DSList.add(a);        	
	                }
                }
                DSList.removeAll(DSToDelete);
                
                busyListNode.setVisible(false);
        		btnDownload.setDisable(false);
        		btnUpload.setDisable(false);

            }
        });
        
        if (ex != null) {
        	throw ex;
        }
		
	}
	
	
	@FXML
	private void handleRefreshAction() {
		
		Thread t = new Thread(new Runnable() {
		    public void run() {
				
		    	try {
					refreshWorkspaces();
				} catch (Throwable e) {
			        Platform.runLater(new Runnable() {
			            public void run() {
			            	
			                Alert msg = new Alert(AlertType.ERROR);
			                msg.setHeaderText("Error while fetching workspaces");
			                msg.show();
			                
			            }
			        });
			        return;
				}
		    	
		    	try {
					refreshDocuments();
				} catch (Throwable e) {
	        		
			        Platform.runLater(new Runnable() {
			            public void run() {
			            	
			                Alert msg = new Alert(AlertType.ERROR);
			                msg.setHeaderText("Error while fetching documents");
			                msg.show();
			                
			            }
			        });
				}
				
			}
			
		});
		t.start();
       
	}
	
	
	
	@FXML
	private void handleCreateWorkspaceAction() {
		
		TextInputDialog dialog = new TextInputDialog("DSXML Tool");
		dialog.setTitle("Create Workspace");
		dialog.setHeaderText("Create Workspace");
		dialog.setContentText("Please enter workspace name:");
		
		Optional<String> result = dialog.showAndWait();
		if (!result.isPresent()) return;
		final String name = result.get();
		
		Thread t = new Thread(new Runnable() {
		    public void run() {
				
		    	Workspace tmp = null;
		    	try {
			        RestCloud cloud = RestCloud.getInstance();
			        tmp = cloud.createWorkspace(cloud.createOwner(), cloud.createTool(), name);
		    	} catch (Exception e) {
		    	}
		    	final Workspace ws = tmp;

		        // refresh UI
		        Platform.runLater(new Runnable() {
		            public void run() {
		            	
		            	if (ws != null) {
			                cbWorkspace.getItems().add(ws);
			                cbWorkspace.getSelectionModel().select(ws);
		            	} else {
		        			Alert a = new Alert(AlertType.ERROR);
		        			a.setTitle("Error");
		        			a.setHeaderText("Error while creating workspace");
		        			a.show();
		            	}
		                
		            }
		        });
				
			}
			
		});
		t.start();
	}
	
	
	
	@FXML
	private void handleWorkspaceSelected() {
		
		Thread t = new Thread(new Runnable() {
		    public void run() {
		    	
		    	try {
					refreshDocuments();
				} catch (Throwable e) {
				}
		    	
			}
			
		});
		t.start();
		
	}
	
	
	
	
	
	public ComboBox<Workspace> getWorkspaceComboBox() {
		return cbWorkspace;
	}
	
	public ListView<Artifact> getDSDocumentList() {
		return lstDS;
	}
	
	
	public File getXMLFile() {
		File f = null;
		try {
			f = new File(txtXML.getText());
			if (!f.exists() || !f.isFile()) return null;
		} catch (Exception e) {
		}
		return f;
	}
	
	public File getXSDFile() {
		File f = null;
		try {
			f = new File(txtXSD.getText());
			if (!f.exists() || !f.isFile()) return null;
		} catch (Exception e) {
		}
		return f;
	}
	
	public File getXMLOutFile() {
		File f = null;
		try {
			f = new File(txtXMLOut.getText());
		} catch (Exception e) {
		}
		return f;
	}
	
	
	/**
	 * Asking entity resolver is a XSD entity resolver that asks the user (displays a popup) 
	 * to specify a XML schema document if it is unable to resolve it.
	 * @author stw
	 */
	class AskingEntityResolver implements EntityResolver {

		private boolean autoResolve = true;
		
		public AskingEntityResolver() {
		}
		
		/**
		 * Creates a AskingEntityResolver
		 * @param tryAutomaticResolve use the AutomaticEntityResolver first
		 */
		public AskingEntityResolver(boolean tryAutomaticResolve) {
			this.autoResolve = tryAutomaticResolve;
		}
		
		
		@Override
		public InputSource resolveEntity(final String publicId, final String systemId) throws SAXException, IOException {
			
			if (autoResolve) {
				AutomaticEntityResolver er = new AutomaticEntityResolver();
				InputSource is = er.resolveEntity(publicId, systemId);
				if (is != null) System.out.println("auto success");
				if (is != null) return is;
			}
			
			final FutureTask<File> query = new FutureTask<File>(new Callable<File>() {
			    @Override
			    public File call() throws Exception {
			        
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Referenced schema not found");
					alert.setHeaderText("Referenced schema not found");
					alert.setContentText("Please select the schema file which is referenced with\nSystem ID: " + systemId + "\nPublic ID: " + publicId);
					alert.showAndWait();
					
					Stage stage = (Stage) rootNode.getScene().getWindow();
					FileChooser chooser = new FileChooser();
					File file = getXSDFile();
					if (file != null) {
						chooser.setInitialDirectory(file.getParentFile());
					}
					File f = chooser.showOpenDialog(stage);
			    	return f;
			    	
			    }
			});
			Platform.runLater(query);
			File f = null;
			try {
				f = query.get();
			} catch (ExecutionException | InterruptedException e) {
				throw new IOException(e);
			}
			if (f != null) {
				return new InputSource(new FileInputStream(f));
			} else {
				return null;
			}
			
		}
		
		
	}
	
	
	/**
	 * Automatic entity resolver is an XSD entity resolver that tries to fetch the XML schema document automatically
	 * @author stw
	 */
	class AutomaticEntityResolver implements EntityResolver {
		
		@Override
		public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {			
			
			if (systemId != null) {
				InputStream is = tryFetchUri(systemId);
				if (is != null) return new InputSource(is);
			}
			
			return null;			
		}
		
		
		private InputStream tryFetchUri(String sourceUri) {
			
			try {
				URL url = new URL(sourceUri);
				URLConnection con = url.openConnection();
				con.setRequestProperty("User-Agent", "DSXML Tool");
				return con.getInputStream();
			} catch (Exception e) {
			}
			return null;
		}
		
	}
	
	
	
}
