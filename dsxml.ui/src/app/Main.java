package app;

import java.util.Date;

import at.jku.sea.cloud.Artifact;
import at.jku.sea.cloud.Workspace;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;

/**
 * UI that allows to view, upload and download DSS models in/to/from DesignSpace.
 * @author stw
 */
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Main.fxml"));
			Pane root = (Pane)loader.load();
			Scene scene = new Scene(root,600,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setMinHeight(400);
			primaryStage.setMinWidth(400);
			
			MainController c = loader.getController();
			
			ListView<Artifact> lstDS = c.getDSDocumentList();
			ObservableList<Artifact> DSItems = FXCollections.observableArrayList();
			lstDS.setItems(DSItems);
			lstDS.setCellFactory(new Callback<ListView<Artifact>, ListCell<Artifact>>() {
				public ListCell<Artifact> call(ListView<Artifact> param) {
					return new DocumentArtifactCell();
				}
			});
			
			ComboBox<Workspace> cbWS = c.getWorkspaceComboBox();
			ObservableList<Workspace> WSItems = FXCollections.observableArrayList();
			cbWS.setItems(WSItems);
			
			Callback<ListView<Workspace>, ListCell<Workspace>> cbCellFactory = 
				new Callback<ListView<Workspace>, ListCell<Workspace>>() {
				public ListCell<Workspace> call(ListView<Workspace> param) {
					return new WorkspaceCell();
				}
			};
			cbWS.setCellFactory(cbCellFactory);
			cbWS.setButtonCell(cbCellFactory.call(null));
			
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
	/**
	 * Custom list cell for DSS document artifacts
	 * Displays the document name and additional metadata
	 * @author stw
	 */
    static class DocumentArtifactCell extends ListCell<Artifact> {
    	
        @Override
        public void updateItem(Artifact item, boolean empty) {
            super.updateItem(item, empty);
            if (item == null || empty) {
            	setGraphic(null);
            } else {
            	VBox vbox = new VBox();
            	String name = (String) item.getPropertyValueOrNull("name");
            	if (name == null) name = "<Unnamed Document>";
            	Date date = null;
            	if (item.hasProperty("date")) {
            		date = new Date((Long)item.getPropertyValue("date"));
            	}
            	
            	Label lbl = new Label(name);
            	lbl.setUnderline(true);
            	vbox.getChildren().add(lbl);
            	if (date != null) {
            		vbox.getChildren().add(new Label("Created on " + date.toString()));
            	}
            	if (item.hasProperty("edition")) {
                	vbox.getChildren().add(new Label("Edition: " + item.getPropertyValue("edition")));
            	}
            	            	
                setGraphic(vbox);
            }
        }
        
    }
    
    
    /**
     * Custom ComboBox list cell for DesignSpace workspaces
     * Displays the workspace name and its identifier.
     * @author stw
     */
    static class WorkspaceCell extends ComboBoxListCell<Workspace> {
    	
    	@Override
    	public void updateItem(Workspace item, boolean empty) {
    		super.updateItem(item, empty);
    		if (item == null || empty) {
    			setText("<Create Workspace>");
    			//Label l = new Label("< Create Workspace >");
    			//setGraphic(l);
    		} else {
    			setText("Workspace " + item.getIdentifier() + " : " + item.getId());
    			//Label l = new Label("Workspace " + item.getIdentifier() + " : " + item.getId());
    			//setGraphic(l);
    		}
    	}
    	
    }
	
	
}
